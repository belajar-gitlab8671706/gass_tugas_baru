import { useQuery } from "@tanstack/react-query";
import axios from "@/libs/axios";

export function useProfile() {
  return useQuery({
    queryKey: ["auth", "user", "profile"],
    queryFn: () =>
      axios.post("", { act: "user_get_profile" }).then((res) => res.data),
  });
}
