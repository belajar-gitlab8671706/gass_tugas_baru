import { useQuery } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { extractRouteParams } from "@/libs/utils";

export function useUser() {
  return useQuery({
    queryKey: ["auth", "user"],
    queryFn: () =>
      axios.post("", { act: "user_check_token" }).then((res) => {
        const params = extractRouteParams(window.location.search);
        if (params.from) {
          window.location.href = `${params.from}${params.callbackUrl || ""}`;
          return Promise.reject();
        }

        return res.data;
      }),
    staleTime: Infinity,
  });
}
