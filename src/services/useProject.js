import { useQuery } from "@tanstack/react-query";
import axios from "@/libs/axios";

export function useProject(options = {}) {
  return useQuery({
    queryKey: ["projects"],
    queryFn: () =>
      axios
        .post("", { act: "project_get" })
        .then((res) => res.data.result.data),
    ...options,
  });
}
