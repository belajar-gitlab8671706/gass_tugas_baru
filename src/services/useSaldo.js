import { useQuery } from "@tanstack/react-query";
import axios from "@/libs/axios";

export function useSaldo() {
  return useQuery({
    queryKey: ["auth", "user", "saldo"],
    queryFn: () =>
      axios.post("", { act: "saldo_get" }).then((res) => res.data.result),
    staleTime: Infinity,
  });
}
