import { useEffect } from "react";
import {
  createBrowserRouter,
  Outlet,
  useNavigate,
  useMatch,
  useLocation,
} from "react-router-dom";

import { ToastContainer } from "react-toastify";
import { SkeletonTheme } from "react-loading-skeleton";

import { useUser } from "./services";
import NotFound from "./routes/404";
import Error from "./routes/Error";

function App() {
  const navigate = useNavigate();
  const location = useLocation();
  const authMatch = useMatch("auth/*");

  const { data: user = {}, status } = useUser();

  useEffect(() => {
    if (status === "success" && authMatch) navigate("/");
    else if (status === "error" && !authMatch) {
      localStorage.setItem("auth-redirect", location.pathname);
      navigate("/auth/login");
    }
  }, [status, location]);

  if (status === "loading") {
    return (
      <div
        className="position-fixed bg-white justify-content-center align-items-center"
        style={{
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 1100,
          transition: "all 0.2s ease",
          display: "flex",
        }}
      >
        <div className="nk-loading">
          <div className="spinner-border text-blue" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      </div>
    );
  }

  return (
    <SkeletonTheme
      highlightColor={
        document.body.classList.contains("dark-mode") ? "#222c3a" : "#e4ecf3"
      }
      baseColor={
        document.body.classList.contains("dark-mode") ? "#1c2736" : "#e2e6eb"
      }
    >
      <Outlet />
      <ToastContainer
        theme={document.body.classList.contains("dark-mode") ? "dark" : "light"}
        pauseOnHover={false}
        pauseOnFocusLoss={false}
        newestOnTop
      />
    </SkeletonTheme>
  );
}

const authRoutes = {
  path: "auth",
  lazy: () => import("./routes/auth/root"),
  children: [
    {
      index: true,
      async lazy() {
        const { Redirect } = await import("./routes/auth/root");
        return { Component: Redirect };
      },
    },
    {
      path: "login",
      lazy: () => import("./routes/auth/login"),
    },
    {
      path: "register",
      lazy: () => import("./routes/auth/register"),
    },
    {
      path: "verify-otp",
      lazy: () => import("./routes/auth/verify-otp"),
    },
    {
      path: "forgot-password",
      children: [
        {
          index: true,
          async lazy() {
            const { Redirect } = await import(
              "./routes/auth/forgot-password/index"
            );
            return { Component: Redirect };
          },
        },
        {
          path: "phone",
          lazy: () => import("./routes/auth/forgot-password/phone"),
        },
        {
          path: "otp",
          lazy: () => import("./routes/auth/forgot-password/otp"),
        },
        {
          path: "change",
          lazy: () => import("./routes/auth/forgot-password/change"),
        },
      ],
    },
  ],
};

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <Error />,
    children: [
      authRoutes,
      {
        path: "/",
        lazy: () => import("./routes/dashboard/root"),
        children: [
          {
            path: "/",
            lazy: () => import("./routes/dashboard/homeDashboard/index"),
          },
          {
            path: "/pesanan",
            lazy: () => import("./routes/dashboard/kelolaPesanan/pesanan"),
          },
          {
            path: "/pesanan-distributor",
            lazy: () => import("./routes/dashboard/kelolaPesanan/pesananDistributor"),
          },
          {
            path: "/customer",
            lazy: () => import("./routes/dashboard/customer/customer"),
          },
          {
            path: "/verifikasi-pembayaran",
            lazy: () => import("./routes/dashboard/admin/verifikasiPembayaranAdmin"),
          },
          {
            path: "/verifikasi-pesanan",
            lazy: () => import("./routes/dashboard/admin/verifikasiPesananAdmin"),
          },
          {
            path: "/verifikasi-bulking",
            lazy: () => import("./routes/dashboard/admin/verifikasiBulkin"),
          },
          {
            path: "/shiping",
            lazy: () => import("./routes/dashboard/admin/shiping"),
          },
          {
            path: "/product",
            lazy: () => import("./routes/dashboard/kelolaProduct/product"),
          },
          {
            path: "/pengguna",
            lazy: () => import("./routes/dashboard/pengaturan/pengguna"),
          },
          {
            path: "/partner",
            lazy: () => import("./routes/dashboard/pengaturan/partner"),
          },
          {
            path: "/pengaturan-umum",
            lazy: () => import("./routes/dashboard/pengaturan/pengaturanUmum"),
          },
          {
            path: "/profile",
            lazy: () => import("./routes/dashboard/profile/index"),
          },
          {
            path: "/:project_key?",
            lazy: () => import("./routes/dashboard/index"),
          },
          {
            path: "/:project_key",
            children: [
              {
                path: "billing",
                lazy: () => import("./routes/dashboard/billing/index"),
              },
            ],
          },
        ],
      },
    ],
  },
  {
    path: "*",
    element: <NotFound />,
  },
]);

export default router;
