import { forwardRef } from "react";
import { memo } from "react";
import { createPortal } from "react-dom";

const ModalWrapper = memo(
  forwardRef(function ModalWrapper(
    { children, size = "md", as = "div", scrollable = false, ...props },
    ref
  ) {
    const Tag = as;

    return createPortal(
      <Tag className="modal fade" {...props} ref={ref}>
        <div
          className={`modal-dialog modal-${size} ${
            scrollable ? "modal-dialog-scrollable" : ""
          }`}
          role="document"
        >
          <div className="modal-content">{children}</div>
        </div>
      </Tag>,
      document.getElementById("modal-root")
    );
  })
);

export { ModalWrapper };
