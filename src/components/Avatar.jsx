import { memo, forwardRef, useMemo } from "react";
import { avatarName } from "@/libs/utils";

const Avatar = memo(
  forwardRef(function Avatar(
    { children, className, name, src, type, status, icon, bg, size, ...props },
    ref
  ) {
    const classes = useMemo(() => {
      let classes = "user-avatar";

      switch (type) {
        case "square":
          classes += " sq";
          break;

        default:
          break;
      }

      if (bg) classes += ` bg-${bg}`;

      if (size) classes += ` ${size}`;

      if (className) classes += ` ${className}`;

      return classes;
    }, [type]);

    const dotClasses = useMemo(() => {
      let classes = "dot dot-lg status";

      switch (status) {
        case "primary":
          classes += " dot-primary";
          break;

        case "success":
          classes += " dot-success";
          break;

        case "warning":
          classes += " dot-warning";
          break;

        case "danger":
          classes += " dot-danger";
          break;

        case "info":
          classes += " dot-info";
          break;

        default:
          break;
      }

      return classes;
    }, [status, size, bg]);

    return (
      <div className={classes} ref={ref} {...props}>
        {/* {name ? avatarName(name) : src ? <img src={src} /> : children} */}
        {src ? <img src={src} /> : name ? avatarName(name) : children}
        {status && (
          <span
            className={dotClasses}
            {...(icon
              ? {
                  style: {
                    width: "24px",
                    height: "24px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    transform: "translate(2px, 2px)",
                  },
                }
              : {})}
          >
            {!!icon && <em className={`icon ni ni-${icon} fs-6`}></em>}
          </span>
        )}
      </div>
    );
  })
);

export { Avatar };
