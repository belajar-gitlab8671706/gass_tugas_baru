import {
  memo,
  forwardRef,
  useMemo,
  Children,
  createContext,
  useContext,
} from "react";

const AccordionContext = createContext({});

const Accordion = memo(
  forwardRef(function Accordion({ children, id, className, ...props }, ref) {
    const classes = useMemo(() => {
      let classes = "accordion";

      if (className) classes = `${classes} ${className}`;

      return classes;
    }, [className]);

    const idAccordion = useMemo(() => {
      if (id) return id;

      return `accordion-${Math.random().toString(36).substr(2, 9)}`;
    }, [id]);

    return (
      <AccordionContext.Provider value={{ idAccordion }}>
        <div id={idAccordion} className={classes} ref={ref} {...props}>
          {Children.map(children, (child) => {
            if (child.type.displayName === "Accordion.Item") {
              return (
                <child.type {...child.props} parent={idAccordion}>
                  {child.props.children}
                </child.type>
              );
            }

            return child;
          })}
        </div>
      </AccordionContext.Provider>
    );
  })
);

Accordion.Item = memo(
  forwardRef(function AccordionItem(
    { children, parent, className, expand = false, ...props },
    ref
  ) {
    const id = useMemo(() => {
      return `${parent}-item-${Math.random().toString(36).substr(2, 9)}`;
    }, [parent]);

    const classes = useMemo(() => {
      let classes = "accordion-item";

      if (className) classes = `${classes} ${className}`;

      return classes;
    }, [className]);

    return (
      <div className={classes} ref={ref} {...props}>
        {Children.map(children, (child) => {
          if (child.type.displayName === "Accordion.Head") {
            return (
              <child.type
                {...child.props}
                data-bs-toggle="collapse"
                data-bs-target={`#${child.props.id || id}`}
                className={expand === true ? "" : "collapsed"}
              >
                {child.props.children}
              </child.type>
            );
          }

          if (child.type.displayName === "Accordion.Body") {
            return (
              <child.type
                {...child.props}
                id={child.props.id || id}
                className={expand === true ? "show" : ""}
              >
                {child.props.children}
              </child.type>
            );
          }

          return child;
        })}
      </div>
    );
  })
);
Accordion.Item.displayName = "Accordion.Item";

Accordion.Head = memo(
  forwardRef(function AccordionHead({ children, className, ...props }, ref) {
    const classes = useMemo(() => {
      let classes = "accordion-head";

      if (className) classes = `${classes} ${className}`;

      return classes;
    }, [className]);

    return (
      <a href="#" className={classes} ref={ref} {...props}>
        <h6 className="title">{children}</h6>
        <span className="accordion-icon"></span>
      </a>
    );
  })
);
Accordion.Head.displayName = "Accordion.Head";

Accordion.Body = memo(
  forwardRef(function AccordionBody({ children, className, ...props }, ref) {
    const classes = useMemo(() => {
      let classes = "accordion-body collapse";

      if (className) classes = `${classes} ${className}`;

      return classes;
    }, [className]);

    const { idAccordion } = useContext(AccordionContext);

    return (
      <div
        className={classes}
        ref={ref}
        {...props}
        data-bs-parent={`#${idAccordion}`}
      >
        <div className="accordion-inner py-1">{children}</div>
      </div>
    );
  })
);
Accordion.Body.displayName = "Accordion.Body";

export { Accordion };
