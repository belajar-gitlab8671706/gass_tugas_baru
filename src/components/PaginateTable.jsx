import {
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
  useMemo,
  memo,
} from "react";
import {
  useReactTable,
  flexRender,
  getCoreRowModel,
} from "@tanstack/react-table";
import { For, If } from "react-haiku";
import { useQuery } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { toast } from "react-toastify";
import { useRef } from "react";

const PaginateTable = memo(
  forwardRef(function PaginateTable(
    {
      columns,
      id,
      payload,
      target,
      className,
      wrapperClassName,
      paginate = true,
      querykey,
      scrollable = false,
      onSuccess = () => ({}),
      onError = () => ({}),
      type = "default", // "default" | "arrow"
      withPaging = true,
      defaultData = null,
      sorting = false,
      maxHeight = null,
      onSort = () => ({}),
      perPage = 10,
      loading,
      ...props
    },
    ref
  ) {
    const [page, setPage] = useState(type === "default" ? 0 : 1);
    const [totalData, setTotalData] = useState(0);
    const totalPage = useMemo(
      () => Math.ceil(totalData / perPage),
      [totalData]
    );

    const [sortBy, setSortBy] = useState(null);
    const [sortType, setSortType] = useState(null);

    const { data, isFetching, refetch } = useQuery({
      queryKey: querykey || ["paginate-table", payload?.act],
      queryFn: () =>
        axios
          .post("", {
            ...(paginate ? { page, start: page } : {}),
            ...(sortBy ? { sort_by: sortBy, sort_type: sortType } : {}),
            ...payload,
          })
          .then((res) => {
            if (typeof res.data === "string")
              return { data: [], total_page: 0 };

            if (type === "arrow") setTotalData(res.data?.result?.total);

            if (res.data.hasOwnProperty("result")) {
              if (res.data?.result?.hasOwnProperty("total_page"))
                return res.data?.result;
              else if (!res.data?.result?.hasOwnProperty("total_page"))
                return res.data?.result?.data;
            }

            if (typeof res.data === "object") {
              if (typeof res.data.data === "object") return res.data.data;
              else return res.data;
            }
            return { data: [], total_page: 0 };
          })
          .catch(() => ({ data: [], total_page: 0 })),
      placeholderData: { data: [] },
      onSuccess: onSuccess,
      onError: (err) => {
        toast.error(err?.message);
        onError(err);
      },
      enabled: !defaultData,
    });

    function handleSort(header) {
      if (sortBy === header.column.id) {
        if (sortType === "asc") {
          setSortType("desc");
        } else {
          setSortType("asc");
        }
      } else {
        setSortBy(header.column.id);
        setSortType("asc");
      }

      if (paginate) {
        if (type === "default") {
          setPage(0);
        } else {
          setPage(1);
        }
        refetch();
      } else if (!!defaultData) {
        onSort({
          sort_by: header.column.id,
          sort_type: sortType === "asc" ? "desc" : "asc",
        });
      }
    }

    useImperativeHandle(ref, () => ({
      refetch,
      setPage,
    }));

    const table = useReactTable({
      data: defaultData
        ? defaultData
        : paginate && type === "default"
        ? data.data || []
        : data || [],
      columns,
      getCoreRowModel: getCoreRowModel(),
    });

    const pagination = useMemo(() => {
      let limit = data.total_page || totalPage <= page + 1 ? 5 : 2;
      return Array.from(
        { length: data.total_page || totalPage },
        (_, i) => i + 1
      ).filter(
        (i) =>
          i >= (page < 3 ? 3 : page) - limit &&
          i <= (page < 3 ? 3 : page) + limit
      );
    }, [data, page, totalPage]);

    const prevPayload = useRef([page, payload]);
    useEffect(() => {
      if (
        JSON.stringify(prevPayload.current) !== JSON.stringify([page, payload])
      ) {
        prevPayload.current = payload;
        refetch();
      }
    }, [page, payload]);

    return (
      <div
        id={id}
        className={`position-relative d-flex flex-column ${
          wrapperClassName ?? ""
        }`}
        {...props}
      >
        <div
          className={`datatable-wrap ${className ?? ""}`}
          style={{
            maxHeight: maxHeight ? maxHeight : "auto",
          }}
          {...(scrollable && { "data-simplebar": true })}
        >
          <table className="nk-tb-list nk-tb-ulist">
            <thead>
              {table.getHeaderGroups().map((headerGroup) => (
                <tr className="nk-tb-item nk-tb-head" key={headerGroup.id}>
                  {headerGroup.headers.map((header) => (
                    <th
                      className={`nk-tb-col ${
                        header.column.columnDef.className || ""
                      }`}
                      key={header.id}
                      style={{ ...header.column.columnDef.style }}
                      {...(sorting && header.column.columnDef.canSort !== false
                        ? {
                            onClick: () => handleSort(header),
                          }
                        : {})}
                    >
                      <span
                        className={`sub-text ${
                          sorting ? "d-flex align-items-center" : ""
                        }`}
                        {...(sorting
                          ? {
                              style: { cursor: "pointer", lineHeight: 1.5 },
                            }
                          : { style: { lineHeight: 1.5 } })}
                      >
                        {header.isPlaceholder
                          ? null
                          : flexRender(
                              header.column.columnDef.header,
                              header.getContext()
                            )}
                        {/* {JSON.stringify(header.column.columnDef)} */}
                        {sorting &&
                          header.column.columnDef.canSort !== false && (
                            <span className="ml-2">
                              {sortBy === header.column.id ? (
                                sortType === "asc" ? (
                                  <em className="icon ni ni-sort-down-fill fs-5"></em>
                                ) : (
                                  <em className="icon ni ni-sort-up-fill fs-5"></em>
                                )
                              ) : (
                                <em className="icon ni ni-sort fs-5"></em>
                              )}
                            </span>
                          )}
                      </span>
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody>
              <If
                isTrue={
                  (!paginate && !!table.getRowModel().rows.length) ||
                  (paginate && !!table.getRowModel().rows.length) ||
                  (paginate &&
                    type === "arrow" &&
                    !!table.getRowModel().rows.length)
                }
              >
                {table.getRowModel().rows.map((row) => (
                  <tr
                    className="nk-tb-item"
                    key={
                      target
                        ? `row.${row.original[target]}.${row.index}`
                        : `row.${row.index}`
                    }
                  >
                    {row.getVisibleCells().map((cell, index) => (
                      <td
                        className={`nk-tb-col ${
                          cell.column.columnDef.className || ""
                        }`}
                        key={
                          target
                            ? `cell.${cell.id}.${cell.row.original[target]}`
                            : `cell.${cell.id}.${index}`
                        }
                        style={{ ...cell.column.columnDef.style }}
                      >
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )}
                      </td>
                    ))}
                  </tr>
                ))}
              </If>
              {type === "default" && (
                <If
                  isTrue={
                    (!paginate && !table.getRowModel().rows.length) ||
                    (paginate && !table.getRowModel().rows.length)
                  }
                >
                  <tr className="nk-tb-item">
                    <td
                      colSpan={columns.length}
                      className="nk-tb-col text-center"
                    >
                      Data tidak ditemukan
                    </td>
                  </tr>
                </If>
              )}
              {type === "arrow" && (
                <If isTrue={!data.length}>
                  <tr className="nk-tb-item">
                    <td
                      colSpan={columns.length}
                      className="nk-tb-col text-center"
                    >
                      Data tidak ditemukan
                    </td>
                  </tr>
                </If>
              )}
            </tbody>
          </table>
        </div>
        {paginate && type === "default" && (
          <nav className="p-2 mt-auto">
            <ul className="pagination justify-content-end">
              <li className={`page-item previous ${page === 0 && "disabled"}`}>
                <span
                  className={`page-link`}
                  {...(page === 0 ? {} : { onClick: () => setPage(page - 1) })}
                  style={{ cursor: "pointer" }}
                >
                  Prev
                </span>
              </li>
              {withPaging && (
                <For
                  each={pagination}
                  render={(item) => (
                    <li
                      onClick={() => setPage(item - 1)}
                      className={`page-item ${item === page + 1 && "active"}`}
                      key={item}
                    >
                      <span
                        className={`page-link ${
                          item === page + 1 && "text-white"
                        }`}
                        style={{ cursor: "pointer" }}
                      >
                        {item}
                      </span>
                    </li>
                  )}
                />
              )}
              <li
                className={`page-item next ${
                  (page === data.total_page - 1 || data.total_page === 0) &&
                  "disabled"
                }`}
              >
                <span
                  className={`page-link`}
                  style={{ cursor: "pointer" }}
                  {...(page === data.total_page - 1 || data.total_page === 0
                    ? {}
                    : { onClick: () => setPage(page + 1) })}
                >
                  Next
                </span>
              </li>
            </ul>
          </nav>
        )}
        {paginate && type === "arrow" && (
          <nav className="p-2 mt-auto">
            <ul className="pagination justify-content-end">
              <li
                className={`page-item previous ${
                  page === 1 && "disabled bg-gray-100"
                }`}
              >
                <span
                  className={`page-link`}
                  {...(page === 1 ? {} : { onClick: () => setPage(page - 1) })}
                  style={{ cursor: "pointer" }}
                >
                  <em className="icon ni ni-chevrons-left"></em>
                </span>
              </li>
              {withPaging && (
                <For
                  each={pagination}
                  render={(item) => (
                    <li
                      onClick={() => setPage(item)}
                      className={`page-item ${
                        item === page && "active text-white"
                      }`}
                      key={item}
                    >
                      <span
                        className={`page-link ${item === page && "text-white"}`}
                        style={{ cursor: "pointer" }}
                      >
                        {item}
                      </span>
                    </li>
                  )}
                />
              )}
              <li
                className={`page-item next ${
                  (page === totalPage || totalPage === 0 || !totalPage) &&
                  (!isNaN(totalPage) || data.length < perPage) &&
                  "disabled bg-gray-100"
                }`}
              >
                <span
                  className={`page-link`}
                  style={{ cursor: "pointer" }}
                  {...((page === totalPage || totalPage === 0 || !totalPage) &&
                  (!isNaN(totalPage) || data.length < perPage)
                    ? {}
                    : { onClick: () => setPage(page + 1) })}
                >
                  <em className="icon ni ni-chevrons-right"></em>
                </span>
              </li>
            </ul>
          </nav>
        )}
        {(isFetching || loading) && (
          <div
            className="position-absolute justify-content-center align-items-center d-flex"
            style={{
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              backgroundColor: "rgba(10, 10, 15, 0.1)",
              transition: "all 0.2s ease",
              opacity: 1,
            }}
          >
            <div className="nk-loading">
              <div className="spinner-border text-blue" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  })
);

export { PaginateTable };
