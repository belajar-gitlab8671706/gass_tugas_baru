import { memo, useRef, forwardRef } from "react";

const ToolsToggle = memo(
  forwardRef(function ToolsToggle({ children, ...props }, ref) {
    return (
      <div className="toggle-wrap nk-block-tools-toggle" {...props} ref={ref}>
        {children}
      </div>
    );
  })
);

ToolsToggle.Button = memo(function Button({ children, className, ...props }) {
  const toggleExpandRef = useRef();
  function handleToggleExpand(ev) {
    ev.preventDefault();
    NioApp.Toggle.trigger($(toggleExpandRef.current).data("target"), {
      toggle: true,
    });
  }

  return (
    <a
      href="#"
      className={`btn toggle-expand ${className}`}
      data-target="pageMenuLabel"
      onClick={handleToggleExpand}
      ref={toggleExpandRef}
      {...props}
    >
      {children}
    </a>
  );
});

ToolsToggle.Content = memo(function Content({ children, className, ...props }) {
  return (
    <div
      className={`toggle-expand-content ${className}`}
      data-content="pageMenuLabel"
      {...props}
    >
      {children}
    </div>
  );
});

export { ToolsToggle };
