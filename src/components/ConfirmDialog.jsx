import { memo } from "react";
import { ModalWrapper, Button } from ".";

const ConfirmDialog = memo(
  ({
    title,
    children,
    onConfirm = () => {},
    onCancel = () => {},
    cancelText = "Cancel",
    confirmText = "Delete",
    cancelBg = "outline-light",
    confirmBg = "danger",
    loading = false,
    disabled = false,
    ...props
  }) => {
    return (
      <ModalWrapper {...props}>
        <div className="modal-header">
          <h5 className="modal-title">{title}</h5>
          <a
            href="#"
            className="close"
            data-bs-dismiss="modal"
            aria-label="Close"
          >
            <em className="icon ni ni-cross"></em>
          </a>
        </div>
        <div className="modal-body">{children}</div>
        <div className="modal-footer">
          <Button
            bg={cancelBg}
            size="lg"
            data-bs-dismiss="modal"
            onClick={onCancel}
            loading={loading}
            disabled={disabled}
          >
            {cancelText}
          </Button>
          <Button
            bg={confirmBg}
            size="lg"
            onClick={onConfirm}
            loading={loading}
            disabled={disabled}
          >
            {confirmText}
          </Button>
        </div>
      </ModalWrapper>
    );
  }
);

export { ConfirmDialog };
