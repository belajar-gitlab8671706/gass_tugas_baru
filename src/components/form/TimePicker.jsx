import { memo, forwardRef } from "react";

import { Select } from "..";

function renderSelectTime(full) {
  // In component
  const options = [];
  for (let i = 0; i <= 23; i++) {
    options.push(
      <option key={i} value={!full ? i : i < 10 ? `0${i}:00:00` : `${i}:00:00`}>
        {i < 10 ? `0${i}:00:00` : `${i}:00:00`}
      </option>
    );

    if (full) {
      options.push(
        <option key={`${i}:30`} value={i < 10 ? `0${i}:30:00` : `${i}:30:00`}>
          {i < 10 ? `0${i}:30:00` : `${i}:30:00`}
        </option>
      );
    }
  }
  return options;
}

const TimePicker = memo(
  forwardRef(function TimePicker({ full = false, ...props }, ref) {
    return (
      <Select
        {...props}
        ref={ref}
        options={{ minimumResultsForSearch: -1 }}
        readOnly
      >
        {renderSelectTime(full)}
      </Select>
    );
  })
);

export { TimePicker };
