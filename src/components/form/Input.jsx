import { useRef, forwardRef, memo, useMemo } from "react";
import CurrencyInput from "react-currency-input-field";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/bootstrap.css";

const Input = memo(
  forwardRef(function Input(
    { children, type, size, className, ...props },
    ref
  ) {
    const idInputFile = useRef(`input-file-${Math.random()}`);

    const togglePassRef = useRef(null);
    function togglePass(ev) {
      ev.preventDefault();
      const input = document.getElementById(
        togglePassRef.current.dataset.target
      );

      if (input.type === "password") {
        input.type = "text";

        togglePassRef.current.classList.add("is-shown");
        togglePassRef.current.classList.remove("is-hidden");
      } else {
        input.type = "password";

        togglePassRef.current.classList.remove("is-shown");
        togglePassRef.current.classList.add("is-hidden");
      }
    }

    const classes = useMemo(() => {
      let classes = "form-control";

      switch (size) {
        case "sm":
          classes += " form-control-sm";
          break;

        case "lg":
          classes += " form-control-lg";
          break;

        case "xl":
          classes += " form-control-xl";

        default:
          break;
      }

      if (className) classes += ` ${className}`;

      return classes;
    }, [size]);

    switch (type) {
      case "textarea":
        return (
          <textarea className={classes} {...props} ref={ref}>
            {children}
          </textarea>
        );

      case "password":
        return (
          <>
            <a
              href="#"
              className={`form-icon form-icon-right passcode-switch ${size}`}
              data-target={props.id}
              ref={togglePassRef}
              onClick={togglePass}
            >
              <em className="passcode-icon icon-show icon ni ni-eye"></em>
              <em className="passcode-icon icon-hide icon ni ni-eye-off"></em>
            </a>
            <input className={classes} type={type} {...props} ref={ref} />
          </>
        );
      case "file":
        return (
          <div className="form-file">
            <input
              type="file"
              className="form-file-input"
              id={idInputFile.current}
            />
            <label
              className="form-file-label"
              htmlFor={idInputFile.current}
              style={{ cursor: "pointer" }}
            >
              Choose file
            </label>
          </div>
        );

      case "currency":
        return (
          <CurrencyInput
            mask="999.999"
            placeholder="000.000"
            className={classes}
            groupSeparator="."
            decimalSeparator=","
            autoComplete="off"
            {...props}
            ref={ref}
          />
        );

      case "tel":
        return (
          <PhoneInput
            country="id"
            inputClass={classes}
            inputProps={{ ...props }}
            ref={ref}
          />
        );

      default:
        return <input className={classes} type={type} {...props} ref={ref} />;
    }
  })
);

export { Input };
