import { memo, forwardRef, useMemo } from "react";

const Radio = memo(
  forwardRef(function Radio(
    { children, className, id, size = "sm", ...props },
    ref
  ) {
    const idRadio = useMemo(() => {
      if (id) return id;

      return `switch-${Math.random().toString(36).substr(2, 9)}`;
    }, [id]);

    return (
      <div
        className={`custom-control custom-radio ${`custom-control-${size}`} ${className}`}
      >
        <input
          type="radio"
          className="custom-control-input"
          id={idRadio}
          {...props}
          ref={ref}
        />
        <label className="custom-control-label" htmlFor={idRadio}>
          {children}
        </label>
      </div>
    );
  })
);

export { Radio };
