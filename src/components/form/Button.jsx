import { memo, forwardRef, useMemo } from "react";

const Button = memo(
  forwardRef(function Button(
    {
      children,
      className,
      loading = false,
      disabled = false,
      size = "md",
      bg,
      variant = "solid",
      icon = false,
      round = false,
      as = "button",
      ...props
    },
    ref
  ) {
    const classes = useMemo(() => {
      let classes = "btn";

      if (size) classes += ` btn-${size}`;

      if (bg) classes += ` btn-${bg}`;

      switch (variant) {
        case "subtle":
          classes += " btn-dim";
          break;

        case "outline":
          classes += " btn-outline";
          break;

        case "link":
          classes += " btn-link";
          break;

        default:
          break;
      }

      if (icon) classes += " btn-icon";
      if (round) classes += " btn-round";

      if (className) classes += ` ${className}`;

      return classes;
    }, [className, size, bg, variant, icon, round]);

    const Tag = useMemo(() => {
      if (as) return as;

      return "button";
    }, [as]);

    return (
      <Tag className={classes} disabled={disabled} {...props} ref={ref}>
        {loading ? (
          <div
            className="spinner-border text-white"
            role="status"
            style={{ width: "16px", height: "16px" }}
          >
            <span className="visually-hidden">Loading...</span>
          </div>
        ) : (
          children
        )}
      </Tag>
    );
  })
);

export { Button };
