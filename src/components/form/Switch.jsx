import { memo, forwardRef, useMemo } from "react";

const Switch = memo(
  forwardRef(function Switch(
    { children, className, id, name, as = "checkbox", ...props },
    ref
  ) {
    const idSwitch = useMemo(() => {
      if (id) return id;

      return `switch-${Math.random().toString(36).substr(2, 9)}`;
    }, [id]);

    return (
      <div className={`custom-control custom-switch ${className ?? ""}`}>
        <input
          type={as}
          className="custom-control-input"
          id={idSwitch}
          name={name}
          {...props}
          ref={ref}
        />
        <label className="custom-control-label" htmlFor={idSwitch}>
          {children}
        </label>
      </div>
    );
  })
);

export { Switch };
