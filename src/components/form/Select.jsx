import React, { memo, useMemo, forwardRef, useEffect, useRef } from "react";

const Select = memo(
  forwardRef(function Select(
    {
      children,
      id,
      placeholder = "Select",
      options,
      search = false,
      value,
      defaultValue,
      ...props
    },
    ref
  ) {
    const selectReference = useRef();
    const idSelect = useMemo(() => {
      if (id) return id;

      return `select-${Math.random().toString(36).substr(2, 9)}`;
    }, [id]);

    useEffect(() => {
      $(`#${idSelect}`)
        .val(value || defaultValue)
        .select2({
          placeholder,
          ...(!search && { minimumResultsForSearch: -1 }),
          ...options,
        })
        .on("change", function (ev) {
          props.onChange && props.onChange(ev);
        });

      selectReference.current = $(`#${idSelect}`).next(".select2-container");

      return () => {
        $(selectReference.current).remove();
      };
    }, []);

    useEffect(() => {
      if (JSON.stringify($(`#${idSelect}`).val()) !== JSON.stringify(value)) {
        $(`#${idSelect}`).val(value).trigger("change");
      }
    }, [value]);

    return (
      <select
        className={`form-select form-control ${props.className}`}
        id={idSelect}
        {...props}
        ref={ref}
      >
        {children}
      </select>
    );
  })
);

// class Select extends React.Component {
//   constructor(props) {
//     super(props);
//     this.selectRef = React.createRef();

//     this.state = {
//       idSelect: props.id
//         ? props.id
//         : `select-${Math.random().toString(36).substr(2, 9)}`,
//     };
//   }

//   componentDidMount() {
//     const { placeholder, options, onChange } = this.props;

//     $(`#${this.state.idSelect}`)
//       .val(this.props.value)
//       .select2({
//         placeholder,
//         ...options,
//       })
//       .on("change", function (ev) {
//         onChange && onChange(ev);
//       });
//   }

//   componentDidUpdate(prevProps) {
//     if (prevProps.value !== this.props.value) {
//       $(`#${this.state.idSelect}`).val(this.props.value).trigger("change");
//     }
//   }

//   componentWillUnmount() {
//     $(`#${this.state.idSelect}`).select2("destroy");
//   }

//   render() {
//     const { children, id, className, ...props } = this.props;

//     return (
//       <select
//         className={`form-select ${className}`}
//         id={this.state.idSelect}
//         {...props}
//         ref={this.selectRef}
//       >
//         {children}
//       </select>
//     );
//   }
// }

export { Select };
