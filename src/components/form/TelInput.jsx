import "react-phone-input-2/lib/bootstrap.css";
import PhoneInput from "react-phone-input-2";
import { Controller } from "react-hook-form";
import { memo } from "react";
import { forwardRef } from "react";

const TelInput = memo(
  forwardRef(function TelInput(
    { control, trigger, name, required, schema, ...elemProps },
    ref
  ) {
    let validPhoneNumber = false;
    const validatePhoneNumber = (
      inputNumber,
      country,
      isDirty,
      phoneLength
    ) => {
      if (isDirty) {
        if (
          inputNumber &&
          inputNumber?.replace(country.dialCode, "")?.trim() === ""
        ) {
          validPhoneNumber = false;
          return false;
        } else if (inputNumber.length < phoneLength) {
          validPhoneNumber = false;
          return false;
        }
        validPhoneNumber = true;
        return true;
      }
      validPhoneNumber = false;
      return false;
    };
    return (
      <Controller
        name={name}
        control={control}
        render={(props) => {
          return (
            <PhoneInput
              onChange={(e) => {
                trigger && trigger();
                props.field.onChange(e);
              }}
              inputProps={{
                id: name,
                name,
                required,
                autoComplete: "none",
              }}
              country={"fr"}
              value={props.field.value}
              isValid={(inputNumber, country, countries) => {
                const phoneLength = Math.ceil(
                  countries.filter(
                    (val) => val.dialCode === country.dialCode
                  )[0]?.format.length / 2
                );
                return validatePhoneNumber(
                  inputNumber,
                  country,
                  props.formState.isDirty,
                  phoneLength
                );
              }}
              specialLabel=""
              {...elemProps}
            />
          );
        }}
        rules={{
          required,
          validate: () => validPhoneNumber || schema?.errorMessage?.validate,
        }}
      />
    );
  })
);

export { TelInput };
