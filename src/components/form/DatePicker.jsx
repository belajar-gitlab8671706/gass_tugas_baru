import moment from "moment";
import { memo, useEffect, forwardRef, useMemo } from "react";

const DatePicker = memo(
  forwardRef(function DatePicker(
    {
      className,
      size,
      id,
      onChange,
      options = {},
      minDate,
      inline = false,
      range = false,
      hideInput = false,
      onCancel = () => {},
      ...props
    },
    ref
  ) {
    const idPicker = useMemo(() => {
      if (id) return id;

      return `date-picker-${Math.random().toString(36).substr(2, 9)}`;
    }, [id]);

    const classes = useMemo(() => {
      if (inline) return className;
      let classes = "form-control date-picker";

      switch (size) {
        case "sm":
          classes += " form-control-sm";
          break;

        case "lg":
          classes += " form-control-lg";
          break;

        case "xl":
          classes += " form-control-xl";

        default:
          break;
      }

      if (className) classes += ` ${className}`;

      return classes;
    }, [size]);

    useEffect(() => {
      if (!range) {
        $(`#${idPicker}`)
          .datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            ...options,
          })
          .on("changeDate", function (ev) {
            if (!inline) onChange(ev);
            else onChange($(this).datepicker("getFormattedDate"));
          });

        return () => {
          $(`#${idPicker}`).datepicker("destroy");
        };
      } else {
        const picker = $(`#${idPicker}`).daterangepicker({
          format: "yyyy-mm-dd",
          autoclose: true,
          ranges: {
            Today: [moment(), moment()],
            Yesterday: [
              moment().subtract(1, "days"),
              moment().subtract(1, "days"),
            ],
            "Last 7 Days": [moment().subtract(6, "days"), moment()],
            "Last 30 Days": [moment().subtract(29, "days"), moment()],
            "This Month": [moment().startOf("month"), moment().endOf("month")],
            "Last Month": [
              moment().subtract(1, "month").startOf("month"),
              moment().subtract(1, "month").endOf("month"),
            ],
          },
          alwaysShowCalendars: true,
          parentEl: $(`#${idPicker}`).parent(),
          ...options,
        });

        picker
          .data("daterangepicker")
          .setStartDate(moment(props.value.start).format("MM/DD/YYYY"));
        picker
          .data("daterangepicker")
          .setEndDate(moment(props.value.end).format("MM/DD/YYYY"));

        picker.data("daterangepicker").show();
        picker.data("daterangepicker").hide = function () {};
        picker.on("apply.daterangepicker", function (ev, data) {
          onChange(ev, data);
          picker
            .data("daterangepicker")
            .setStartDate(moment(data.startDate).format("MM/DD/YYYY"));
          picker
            .data("daterangepicker")
            .setEndDate(moment(data.endDate).format("MM/DD/YYYY"));
        });
        picker.on("cancel.daterangepicker", onCancel);

        // return () => {
        //   $(`#${idPicker}`).datepicker("destroy");
        // };
      }
    }, [props.value]);

    useEffect(() => {
      if (minDate) $(`#${idPicker}`).datepicker("setStartDate", minDate);
    }, [minDate]);

    if (inline) {
      return (
        <div
          className={`${classes} ${hideInput ? "d-none" : ""}`}
          id={idPicker}
          data-date={props.value}
          style={{ cursor: "pointer" }}
          {...props}
          ref={ref}
        />
      );
    }

    return (
      <input
        type="text"
        className={`${classes} ${hideInput ? "d-none" : ""}`}
        id={idPicker}
        autoComplete="off"
        style={{ cursor: "pointer" }}
        {...props}
        ref={ref}
      />
    );
  })
);

export { DatePicker };
