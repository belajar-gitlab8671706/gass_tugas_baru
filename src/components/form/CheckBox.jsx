import { memo, forwardRef, useMemo } from "react";

const CheckBox = memo(
  forwardRef(function CheckBox(
    { children, className, id, size = "sm", ...props },
    ref
  ) {
    const idCheckBox = useMemo(() => {
      if (id) return id;

      return `switch-${Math.random().toString(36).substr(2, 9)}`;
    }, [id]);

    return (
      <div
        className={`custom-control custom-checkbox ${`custom-control-${size}`} ${className}`}
      >
        <input
          type="checkbox"
          className="custom-control-input"
          id={idCheckBox}
          {...props}
          ref={ref}
        />
        <label className="custom-control-label" htmlFor={idCheckBox}>
          {children}
        </label>
      </div>
    );
  })
);

export { CheckBox };
