import { useRef, Children, memo, useEffect } from "react";
import { Link } from "react-router-dom";

const SidebarMenu = memo(function SidebarMenu({ children }) {
  return Children.map(children, (child) => {
    switch (child.type) {
      case SidebarMenu.Item:
        return SidebarMenu.Item(child.props);
      case SidebarMenu.Toggle:
        return SidebarMenu.Toggle(child.props);
      case SidebarMenu.Dropdown:
        return SidebarMenu.Dropdown(child.props);
      default:
        return child;
    }
  });
});

SidebarMenu.Dropdown = memo(function SidebarMenuDropdown({
  children,
  className = "",
  active = false,
  ...restProps
}) {
  return (
    <li
      className={`nk-menu-item has-sub ${className} ${
        active ? "active current-page" : ""
      }`}
      {...restProps}
    >
      {children}
    </li>
  );
});

SidebarMenu.Item = memo(function SidebarMenuItem({
  children,
  className = "",
  active = false,
  ...restProps
}) {
  return (
    <li
      className={`nk-menu-item ${className} ${
        active ? "active current-page" : ""
      }`}
      {...restProps}
    >
      {children}
    </li>
  );
});

SidebarMenu.Toggle = memo(function SidebarMenuToggle({
  children,
  className = "",
  to = "#",
  ...restProps
}) {
  const optionRef = useRef({
    active: "active",
    self: "nk-menu-toggle",
    child: "nk-menu-sub",
  });
  const toggleRef = useRef();

  function toggleMenu(ev) {
    if (
      NioApp.Win.width < 992 ||
      $(toggleRef.current).parents().hasClass("nk-sidebar")
    ) {
      NioApp.Toggle.dropMenu($(toggleRef.current), optionRef.current);
    }
    if (to === "#") ev.preventDefault();
  }

  return (
    <Link
      to={to}
      className={`nk-menu-link nk-menu-toggle ${className}`}
      ref={toggleRef}
      onClick={toggleMenu}
      {...restProps}
    >
      {children}
    </Link>
  );
});

SidebarMenu.Menu = memo(function SidebarMenuMenu({
  children,
  className = "",
  ...restProps
}) {
  const menuRef = useRef();

  useEffect(() => {
    if ($(menuRef.current).parents().hasClass("active"))
      $(menuRef.current).css("display", "block");
    else $(menuRef.current).css("display", "none");
  }, [$(menuRef.current).parents().hasClass("active")]);

  return (
    <ul
      className="nk-menu-sub"
      style={{ display: "none" }}
      ref={menuRef}
      {...restProps}
    >
      {children}
    </ul>
  );
});

export { SidebarMenu };
