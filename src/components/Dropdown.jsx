import { memo, forwardRef, useMemo, Children } from "react";
import { Button } from ".";
import { useEventListener } from "react-haiku";
import { useRef } from "react";

const Dropdown = memo(
  forwardRef(function Dropdown(
    {
      children,
      className,
      top = false,
      right = false,
      bottom = false,
      left = false,
      backdrop = false,
      ...props
    },
    ref
  ) {
    const classes = useMemo(() => {
      let classes = "dropdown";

      if (top) classes = "dropup";
      if (right) classes = "dropright";
      if (left) classes = "dropleft";

      if (className) classes += ` ${className}`;

      return classes;
    }, [className]);

    const dropdownRef = ref || useRef();

    useEventListener(
      "shown.bs.dropdown",
      () => {
        if (backdrop) {
          document.querySelectorAll(".app-backdrop")[0].style.display = "block";
          document.querySelectorAll(".app-backdrop")[1].style.display = "block";
        }
      },
      dropdownRef
    );

    useEventListener(
      "hidden.bs.dropdown",
      () => {
        if (backdrop) {
          document.querySelectorAll(".app-backdrop")[0].style.display = "none";
          document.querySelectorAll(".app-backdrop")[1].style.display = "none";
        }
      },
      dropdownRef
    );

    return (
      <>
        <div className={classes} {...props} ref={dropdownRef}>
          {children}
        </div>
        {backdrop && <div className="app-backdrop" style={{ right: "-1px" }} />}
      </>
    );
  })
);

Dropdown.Button = memo(
  forwardRef(function DropdownButton(
    { children, className, indicator, as, ...props },
    ref
  ) {
    const classes = useMemo(() => {
      let classes = "dropdown-toggle";

      if (indicator) classes += " dropdown-indicator";
      if (className) classes += ` ${className}`;

      return classes;
    }, [className]);

    const Comp = as || Button;

    return (
      <Comp
        className={classes}
        data-bs-toggle="dropdown"
        aria-expanded="false"
        ref={ref}
        {...props}
      >
        {children}
      </Comp>
    );
  })
);

Dropdown.Menu = memo(
  forwardRef(function DropdownButton(
    { children, className, end = false, placement, ...props },
    ref
  ) {
    const classes = useMemo(() => {
      let classes = "dropdown-menu";

      if (end) classes += " dropdown-menu-end";

      if (className) classes += ` ${className}`;

      return classes;
    }, [className]);

    return (
      <div
        className={classes}
        data-popper-placement={placement}
        ref={ref}
        {...props}
      >
        {children}
      </div>
    );
  })
);

export { Dropdown };
