import { memo, forwardRef, useMemo, useRef } from "react";

import { Button } from ".";

const ChatTools = memo(
  forwardRef(function ChatTools(
    { className, icon = "ni-plus-circle-fill", children, ...props },
    ref
  ) {
    const toggleRef = useRef();
    const optionRef = useRef();
    function handleToggleOption(ev) {
      NioApp.Toggle.trigger($(toggleRef.current).data("target"), {
        toggle: true,
      });
    }

    return (
      <div
        className={`nk-chat-editor-upload ms-n1 ${className}`}
        {...props}
        ref={ref}
      >
        <Button
          icon
          size="sm"
          type="button"
          id="chat-tools"
          className="btn-trigger text-primary toggle-opt"
          data-target="chat-upload"
          ref={toggleRef}
          onClick={handleToggleOption}
        >
          <em className={`icon ni ${icon}`}></em>
        </Button>
        <div
          className="chat-upload-option bg-transparent"
          data-content="chat-upload"
          ref={optionRef}
        >
          <ul className="flex-column" style={{ rowGap: "0.625rem" }}>
            {children}
          </ul>
        </div>
      </div>
    );
  })
);

ChatTools.Item = memo(
  forwardRef(function ChatToolsItem({ children, ...props }, ref) {
    return (
      <li {...props} ref={ref}>
        {children}
      </li>
    );
  })
);

export { ChatTools };
