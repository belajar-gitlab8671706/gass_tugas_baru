import { memo } from "react";
import { toLink } from "@/libs/utils";

import Zoom from "react-medium-image-zoom";
import "react-medium-image-zoom/dist/styles.css";
import ReactAudioPlayer from "react-audio-player";
import ReactPlayer from "react-player";
import parse from "html-react-parser";

const ChatMessage = memo(function ChatMessage({
  message = {},
  mode,
  isMe = false,
  reply = false,
}) {
  if (mode === "recent") {
    if (!message) return null;

    if (
      message.type == "jpeg" ||
      message.type == "png" ||
      message.type == "webp"
    ) {
      return (
        <>
          <em className="icon ni ni-img"></em>
          <span>Image</span>
        </>
      );
    } else if (message.type === "ogg") {
      return (
        <>
          <em className="icon ni ni-mic"></em>
          <span>Audio</span>
        </>
      );
    } else if (message.type === "mp4") {
      return (
        <>
          <em className="icon ni ni-video"></em>
          <span>Video</span>
        </>
      );
    } else if (message.type === "vcard") {
      return (
        <>
          <em className="icon ni ni-user-alt-fill"></em>
          <span>{message.name}</span>
        </>
      );
    } else if (message.type === "text") {
      return message.caption;
    } else {
      return (
        <>
          <em className="icon ni ni-clip-h"></em>
          <span>{message.filename}</span>
        </>
      );
    }
  } else {
    if (!message) return null;

    let caption = message.caption?.replace(
      /<(\s*\/?(?!(a|\/a|br)\b)[^>]*)>/gi,
      "&lt;$1&gt;"
    );
    caption = caption?.replace(new RegExp("\r?\n", "g"), "<br />");

    caption = toLink(caption || "", isMe);

    if (["jpeg", "png", "webp", "jpg"].includes(message.type)) {
      return (
        <div
          className="gallery card"
          style={
            reply ? { display: "grid", gridTemplateColumns: "70px 1fr" } : {}
          }
        >
          <Zoom>
            <img
              className={`w-100 h-100 ${
                reply ? "rounded-start" : "rounded-top"
              }`}
              alt=""
              src={message.url}
              style={reply ? { aspectRatio: 1, objectFit: "cover" } : {}}
            />
          </Zoom>
          <div className="gallery-body card-inner align-center justify-between flex-wrap g-2 p-2">
            <div className="user-card">
              <div className="user-info">
                <span
                  className={`d-block ${reply ? "line-clamp-2" : ""}`}
                  dangerouslySetInnerHTML={{ __html: caption }}
                ></span>
              </div>
            </div>
          </div>
        </div>
      );
    } else if (message.type === "text") {
      return (
        <span className={`${reply ? "line-clamp-3" : ""}`}>
          {parse(caption)}
        </span>
      );
    } else if (message.type === "vcard") {
      return (
        <div className="card">
          <div className="card-header bg-light px-2 py-2">
            <strong className="fs-6">{message.name}</strong>
          </div>
          <div className="card-body px-2 py-1">{message.phone}</div>
        </div>
      );
    } else if (message.type === "mp4") {
      return (
        <>
          <div
            className="gallery card"
            style={
              reply ? { display: "grid", gridTemplateColumns: "120px 1fr" } : {}
            }
          >
            <a className="download" href={message.url} target="_blank">
              {/* <iframe src={message.url} allowfullscreen></iframe> */}
              <ReactPlayer
                url={message.url}
                controls
                width="100%"
                height="100%"
                style={reply ? { aspectRatio: "16/9", objectFit: "cover" } : {}}
              />
            </a>
            <div className="gallery-body card-inner align-center justify-between flex-wrap g-2 p-2 pt-1">
              <div className="user-card">
                <div className="user-info">
                  <span className={`d-block ${reply ? "line-clamp-2" : ""}`}>
                    {parse(caption)}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </>
      );
    } else if (message.type === "ogg") {
      return (
        <>
          <div className="card">
            <div className="kanban-item p-0">
              <div className="kanban-item-meta">
                <ul className="kanban-item-meta-list px-2">
                  <li>
                    <ReactAudioPlayer
                      src={message.url}
                      controls
                      style={{ maxWidth: "100%" }}
                    />
                  </li>
                </ul>
              </div>
            </div>
            {!!caption && (
              <div className="gallery-body card-inner align-center justify-between flex-wrap g-2 p-2 pt-1">
                <div className="user-card">
                  <div className="user-info">
                    <span className={`d-block ${reply ? "line-clamp-2" : ""}`}>
                      {parse(caption)}
                    </span>
                  </div>
                </div>
              </div>
            )}
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="card">
            <div className="kanban-item p-0">
              <div className="kanban-item-meta p-0">
                <ul className="kanban-item-meta-list p-3">
                  <li>
                    <a
                      className="download d-flex align-items-center"
                      href={message.url}
                      target="_blank"
                    >
                      <em className="icon ni ni-clip-h"></em>
                      <span
                        className="d-inline-block text-truncate"
                        style={{ maxWidth: "250px" }}
                      >
                        {message.filename}
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            {!!caption && (
              <div className="gallery-body card-inner align-center justify-between flex-wrap g-2 p-2 pt-1">
                <div className="user-card">
                  <div className="user-info">
                    <span className={`d-block ${reply ? "line-clamp-2" : ""}`}>
                      {parse(caption)}
                    </span>
                  </div>
                </div>
              </div>
            )}
          </div>
        </>
      );
    }
  }
});

export { ChatMessage };
