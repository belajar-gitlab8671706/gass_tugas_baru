import { memo, forwardRef, useMemo } from "react";

const Alert = memo(
  forwardRef(function Alert(
    { children, visible = false, state, variant, label, onClose, ...props },
    ref
  ) {
    if (!visible) return null;

    const classes = useMemo(() => {
      let classes = "alert";

      switch (state) {
        case "primary":
          classes += " alert-primary";
          break;

        case "secondary":
          classes += " alert-secondary";
          break;

        case "success":
          classes += " alert-success";
          break;

        case "info":
          classes += " alert-info";
          break;

        case "warning":
          classes += " alert-warning";
          break;

        case "danger":
          classes += " alert-danger";
          break;

        case "gray":
          classes += " alert-gray";
          break;

        case "light":
          classes += " alert-light";
          break;

        default:
          break;
      }

      switch (variant) {
        case "fill":
          classes += " alert-fill";
          break;

        case "pro":
          classes += " alert-pro";
          break;

        default:
          break;
      }

      return classes;
    }, [state, variant]);

    return (
      <div className={classes} role="alert" {...props} ref={ref}>
        <button
          type="button"
          className="close"
          data-dismiss="alert"
          aria-label="Close"
          onClick={onClose}
        >
          <span aria-hidden="true" className="ni">
            ×
          </span>
        </button>
        <strong>{label}</strong>
        <div>{children}</div>
      </div>
    );
  })
);

export { Alert };
