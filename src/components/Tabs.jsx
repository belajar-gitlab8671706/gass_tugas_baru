import {
  memo,
  forwardRef,
  createContext,
  useState,
  useContext,
  useReducer,
  useEffect,
} from "react";

const TabContext = createContext();

const Tabs = memo(
  forwardRef(function Tabs({ children, active, ...props }, ref) {
    const [state, setState] = useState(active);
    useEffect(() => {
      setState(active);
    }, [active]);

    return (
      <TabContext.Provider value={state}>
        <div {...props} ref={ref}>
          {children}
        </div>
      </TabContext.Provider>
    );
  })
);

Tabs.Nav = memo(
  forwardRef(function TabsNav({ children, ...props }, ref) {
    return (
      <ul className="nav" role="tablist" {...props} ref={ref}>
        {children}
      </ul>
    );
  })
);

Tabs.Tab = memo(
  forwardRef(function TabsTab(
    { children, target = "", className, as = "a", toggle = true, ...props },
    ref
  ) {
    const state = useContext(TabContext);

    const Tag = as;

    return (
      <li className="nav-item p-0" ref={ref}>
        <Tag
          className={`nav-link ${className || ""} ${
            state === target ? "active" : ""
          }`}
          {...(toggle
            ? {
                "data-bs-toggle": "tab",
                href: `#${target}`,
                "aria-selected": "true",
                role: "tab",
              }
            : {})}
          {...props}
        >
          {children}
        </Tag>
      </li>
    );
  })
);

Tabs.Content = memo(
  forwardRef(function TabsNav({ children, className, ...props }, ref) {
    return (
      <div className={`tab-content ${className}`} {...props} ref={ref}>
        {children}
      </div>
    );
  })
);

Tabs.Panel = memo(
  forwardRef(function TabsNav({ children, className, id, ...props }, ref) {
    const state = useContext(TabContext);

    return (
      <div
        className={`tab-pane ${id === state ? "active show" : ""} ${
          className || ""
        }`}
        id={id}
        {...props}
        ref={ref}
      >
        {children}
      </div>
    );
  })
);

export { Tabs };
