import { Button } from "@/components";

export default function Error() {
  return (
    <div
      className="d-flex align-items-center justify-content-center flex-column gap-4"
      style={{ height: "100vh" }}
    >
      <img
        src="/images/error.svg"
        alt="Error"
        style={{ width: "75%", maxWidth: "560px" }}
      />
      {/* <h6 className="text-center">
        Maaf ya, sepertinya website kita lagi bermasalah nih 😅. Tenang aja, tim
        kita udah pada ngoprek-ngoprek buat benerin.
      </h6> */}
      <Button type="button" bg="danger" onClick={() => window.location.reload()}>
        Refresh
      </Button>
    </div>
  );
}
