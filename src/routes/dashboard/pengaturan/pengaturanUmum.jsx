import './style-pengaturan.css'
import { Link } from "react-router-dom"
import { AiOutlineCheckCircle, AiOutlineMore, AiOutlineCreditCard, AiOutlineSetting } from "react-icons/ai";
import { MdOutlineHome } from "react-icons/md";
import { TbPointFilled } from "react-icons/tb";
import { FaShippingFast } from "react-icons/fa";
import BankPembayaran from './componen/bankPembayaran';
import ComponentCod from './componen/cod';
import Setting from './componen/setting';
import GudangPengiriman from './componen/gudangPengiriman';
import { useState } from 'react';

export function Component() {
    const [activeComponent, setActiveComponent] = useState(null);

    const handleLinkClick = (component) => {
        setActiveComponent(component)
    }

    const renderActiveComponent = () => {
        switch (activeComponent) {
            case 'bankPembayaran':
                return <BankPembayaran />;
            case 'gudangPengiriman':
                return <GudangPengiriman />;
            case 'cod':
                return <ComponentCod />;
            case 'setting':
                return <Setting />;
            default:
                return <BankPembayaran />;
        }
    };

    return (
        <div>
            <p><Link to='/' className="h6 text-primary">Dashboard </Link> / Verifikasi Pesanan Distributor</p>
            <div className="my-3 bg-white position-relative py-4 border rounded">
                <AiOutlineMore className='simbolTitikTiga' />
                <span className="d-flex ms-4"><AiOutlineCheckCircle className="me-1 iconHome" /> <p className="fw-bold h5 text dark">PT. Ripit</p></span>
                <span className="d-flex ms-4"><MdOutlineHome className="iconHome me-1" /> <p className="textProdusen">Produsen</p></span>
                <div className='d-flex justify-content-end'>
                    <p className='me-4'>YONO <TbPointFilled /> 02/06/2021</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-3'>
                    <div className='bg-white border rounded my-3 position-relative px-3 py-4'>
                        <AiOutlineMore className='simbolTitikTiga' />
                        <p className='text-decoration-underline'>Kontak</p>
                        <ul>
                            <li><TbPointFilled /> 08676767676</li>
                        </ul>
                    </div>
                    <div className='bg-white border rounded my-3 position-relative py-3 px-3'>
                        <AiOutlineMore className='simbolTitikTiga' />
                        <p className='text-decoration-underline'>Owner</p>
                        <ul>
                            <li className='fw-bold'><TbPointFilled /> Yono Cahyono</li>
                            <li><TbPointFilled /> 08676767676</li>
                        </ul>

                    </div>
                    <div className='bg-white border rounded my-3 position-relative py-3 px-3'>
                        <AiOutlineMore className='simbolTitikTiga' />
                        <p className='text-decoration-underline'>Alamat</p>
                        <p>Jl. Sabar Raya <br />
                            Kec. Cipayung <TbPointFilled /> Kota. Jakarta Timur
                            DKI Jakarta <TbPointFilled />12312</p>
                    </div>
                </div>
                <div className='col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 my-3'>
                    <div className='bg-white border rounded'>
                        <div className='border-bottom pb-3 px-3 py-4'>
                            <Link className='' onClick={() => handleLinkClick('bankPembayaran')}><AiOutlineCreditCard className='h6' /> Bank Pembayaran</Link>
                            <Link className='mx-md-4 mx-2' onClick={() => handleLinkClick('gudangPengiriman')}><FaShippingFast className='h6' /> Gudang Pengiriman</Link>
                            <Link onClick={() => handleLinkClick('cod')}><AiOutlineCreditCard className='h6' /> COD</Link>
                            <Link className='ms-md-4 ms-2' onClick={() => handleLinkClick('setting')}><AiOutlineSetting className='h6' /> Setting</Link>
                        </div>
                        <div>
                            {renderActiveComponent()}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}