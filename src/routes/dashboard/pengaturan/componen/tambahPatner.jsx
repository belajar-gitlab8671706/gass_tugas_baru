import '../style-pengaturan.css'
import { Link } from "react-router-dom"
import { IoMdClose } from "react-icons/io";
import { useCounterStore } from '@/store';
import { Input } from "@/components";
import { AiOutlineInfoCircle } from "react-icons/ai";

const TambahPatner = () => {
    const { toggleBackgroundTambahPatner } = useCounterStore()

    return (
        <div className="bg-white contenSetelProduct position-relative">
            <div className='border px-3 py-3'>
                <Link to='/partner' onClick={toggleBackgroundTambahPatner} className='position-absolute end-0 me-3'>
                    <IoMdClose className='h4' />
                </Link>
                <p className='text-center fw-bold mb-0'>Tambah Partner</p>
            </div>
            <div className="contenFormInputSetelProduct">
                <div className="row mx-5 py-4 border-bottom">
                    <div className="col-6 d-flex align-items-center">
                        <div>
                            <p className="fw-bold">Nama atau Label Distributor</p>
                            <p>Nama atau label Distributor 5 <br /> karakter</p>
                        </div>
                    </div>
                    <div className="col-6 d-flex align-items-center">
                        <Input placeholder='Contoh: PT. Ripit Cuy' className='w-100' />
                    </div>
                </div>
                <div className="row mx-5 py-4 border-bottom">
                    <div className="col-6 d-flex align-items-center">
                        <div>
                            <p className="fw-bold">Informasi Perjanjian</p>
                            <p>Informasi Perjanjian dengan<br /> nomor surat dan batas akhir<br /> pembayaran</p>
                        </div>
                    </div>
                    <div className="col-6">
                        <Input placeholder='Nomor Suara Perjanjian' />
                        <div className="d-flex align-items-center w-100 mt-3 py-1 justify-content-center rounded contenInputPembayaranAkhir">
                            <input placeholder='0' className="inputBatasAkhirPembayaran form-control text-center" />
                            <p className='textBatasAKhirPembayaran ms-1'><AiOutlineInfoCircle /> Batas akhir pembayaran (dalam hari)</p>
                        </div>
                    </div>
                </div>
                <div className="row mx-5 py-4 border-bottom">
                    <div className="col-6 d-flex align-items-center">
                        <div>
                            <p className="fw-bold">Admin</p>
                            <p>Tambahkan Admin sebagai<br /> pengelola Dashboard.</p>
                        </div>
                    </div>
                    <div className="col-6 d-flex align-items-center">
                        <div className="w-100">
                            <input placeholder='Nama Admin' className='w-100 form-control' />
                            <input placeholder='Alamat Email' className='w-100 my-2 form-control' />
                            <input placeholder='Whatsapp: 08506XXX' className='w-100 form-control' />
                        </div>
                    </div>
                </div>
                <div className="row mx-5 py-4 border-bottom">
                    <div className="col-6 d-flex align-items-center">
                        <div>
                            <p className="fw-bold">Owner</p>
                            <p>Sertakan informasi Owner,<br /> Kontak telephone dan email.</p>
                        </div>
                    </div>
                    <div className="col-6 d-flex align-items-center">
                        <div className="w-100">
                            <input placeholder='Nama Owner' className='w-100 form-control' />
                            <input placeholder='Phone' className='w-100 my-2 form-control' />
                            <input placeholder='Email' className='w-100 form-control' />
                        </div>
                    </div>
                </div>
                <div className="row mx-5 py-4 border-bottom">
                    <div className="col-6 d-flex align-items-center">
                        <div>
                            <p className="fw-bold">Kontak</p>
                            <p>Lengkapi informasi kontak<br /> Partner.</p>
                        </div>
                    </div>
                    <div className="col-6 d-flex align-items-center">
                        <div className="w-100">
                            <input placeholder='Phone' className='w-100 form-control' />
                            <input placeholder='Alamat Email' className='w-100 my-2 form-control' />
                        </div>
                    </div>
                </div>
                <div className="row mx-5 py-4">
                    <div className="col-6 d-flex align-items-center">
                        <div className="w-100">
                            <p className="fw-bold">Alamat Lengkap</p>
                            <p>Alamat lengkap partner</p>
                        </div>
                    </div>
                    <div className="col-6">
                        <textarea className="form-control" rows="7"></textarea>
                        <input className='w-100 my-3 form-control' placeholder='Cari Kota atau Kecamatan' />
                        <Input placeholder='Kode Pos' />
                    </div>
                </div>
            </div>
            <div className='py-3 px-5 border-bottom bg-light d-flex justify-content-end'>
                <button className='bg-primary text-white border-0 py-1 px-3 rounded fw-bold'>SIMPAN</button>
            </div>
        </div>
    )
}

export default TambahPatner