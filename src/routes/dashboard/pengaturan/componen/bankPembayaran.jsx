import '../style-pengaturan.css'
import { Input } from "@/components"
import { AiOutlineEllipsis } from "react-icons/ai";
import { MdAddCard } from "react-icons/md";

const BankPembayaran = () => {
    return (
        <div className="px-3 py-4">
            <div className="d-flex">
                <Input placeholder='Cari Bank' />
                <button className="bg-primary py-1 px-1 border-0 rounded btnFile text-white ms-2">
                    <MdAddCard className='iconFile top-0' />
                </button>
            </div>
            <div className="border rounded px-3 py-4 bg-light mt-4 position-relative">
                <AiOutlineEllipsis className='iconBankBayar text-dark' />
                <p className="fw-bold h6 my-0 text-dark">BANK MANDIRI</p>
                <p className='text-dark'>PRODUSEN - 321321</p>
            </div>
        </div>
    )
}

export default BankPembayaran