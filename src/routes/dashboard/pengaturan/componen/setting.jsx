import { Form } from 'react-bootstrap'

const Setting = () => {
    return (
        <div className="px-3 py-4">
            <div className="border rounded px-3 py-4 bg-light position-relative">
                <Form.Check
                    type="switch"
                    id="custom-switch"
                    className='CheckBookSetting form-switch-lg'
                />
                <p className='fw-bold text-dark h6 my-0'>Pengaturan Angka Unik</p>
                <p>Penggunaan 3 digit angka unik pada proses pembayaran</p>
                <div className='w-50'>
                    <p>Contoh:</p>
                    <div className='d-flex justify-content-between border-bottom border-1 border-dark'>
                        <p className='my-0'>Sub Total</p>
                        <p className='my-0'>Rp1000</p>
                    </div>
                    <div className='d-flex justify-content-between'>
                        <p className='my-0 fw-bold text-dark'>Total Bayar</p>
                        <p className='my-0 fw-bold text-dark'>Rp1000</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Setting