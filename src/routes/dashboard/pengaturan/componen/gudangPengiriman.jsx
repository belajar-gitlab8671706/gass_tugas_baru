import { FaShippingFast } from "react-icons/fa";
import { AiOutlineEllipsis } from "react-icons/ai";
import { Input } from "@/components";

const GudangPengiriman = () => {
    return (
        <div className="px-3 py-4">
            <div className="d-flex">
                <Input placeholder='Cari Gudang' />
                <button className="bg-primary py-1 px-1 border-0 rounded btnFile text-white ms-2">
                    <FaShippingFast className='iconFile top-0' />
                </button>
            </div>
            <div className="border rounded px-3 py-4 bg-light mt-4 position-relative">
                <AiOutlineEllipsis className='simbolTitikTiga me-3 text-dark' />
                <p className="fw-bold h6 text-dark">Gudang Ripit</p>
                <p className="w-50 text-dark">
                    jl. Ripit (08787876667)
                    Kec. Cipayung - Kota. Jakarta Timur
                    DKI Jakarta 123123
                </p>
            </div>
        </div>
    )
}

export default GudangPengiriman