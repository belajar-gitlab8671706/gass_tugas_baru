import { AiOutlineClose, AiOutlineUser } from "react-icons/ai";
import { Link } from "react-router-dom";
import { useCounterStore } from '@/store'
import { Input } from "@/components";
import { Form } from 'react-bootstrap';

const TambahPengguna = () => {
    const { toggleBackgroundTambahPenguna } = useCounterStore()

    return (
        <div className="bg-white contenerTambahPengguna">
            <div className="d-flex justify-content-between border-bottom py-3 px-3">
                <span><AiOutlineUser className="h5 text-dark" /> <p className="text-dark fw-bold h5 d-inline">Tambah Pengguna</p></span>
                <Link to='/pengguna' onClick={toggleBackgroundTambahPenguna}><AiOutlineClose className="fw-bold h5" /></Link>
            </div>
            <div className="py-3 px-3 border-bottom">
                <label className="w-100">
                    NAMA
                    <Input />
                </label>
                <div className="d-flex justify-content-between py-3">
                    <button className="border-0 rounded w-100 py-1 me-1">Laki - Laki</button>
                    <button className="border-0 rounded w-100 py-1 ms-1">Perempuan</button>
                </div>
                <label className="w-100">
                    EMAIL
                    <Input />
                </label>
                <label className="w-100 py-3">
                    WHATSAPP
                    <Input />
                </label>
                <label className="w-100 ">
                    LEVEL
                    <Form.Select aria-label="Default select example">
                        <option disabled selected hidden>All</option>
                        <option value="1">Supervisor</option>
                        <option value="2">Customer Service</option>
                        <option value="3">Admin Gudang</option>
                        <option value="3">Admin Payment</option>
                        <option value="3">Admin Shiping</option>
                        <option value="3">Admin Data</option>
                        <option value="3">CS CRM</option>
                    </Form.Select>
                </label>
            </div>
            <div className="px-3 py-3">
                <button className="py-1 bg-primary w-100 text-center text-white border-0 rounded">SIMPAN</button>
            </div>
        </div>
    )
}

export default TambahPengguna