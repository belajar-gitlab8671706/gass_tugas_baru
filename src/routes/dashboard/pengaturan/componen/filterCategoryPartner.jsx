import { Form } from 'react-bootstrap';

const FilterCategoryPartner = () => {
    return (
        <div className="bg-white conten">
            <p className='mx-4 mt-2 mb-0'>Filter Kategori</p>
            <hr className='text-primary' />
            <div className="mx-3">
                <label className="w-100">
                    LEVEL
                    <Form.Select aria-label="Default select example">
                        <option disabled selected hidden>All</option>
                        <option value="1">Supervisor</option>
                        <option value="2">Customer Service</option>
                        <option value="3">Admin Gudang</option>
                        <option value="3">Admin Payment</option>
                        <option value="3">Admin Shiping</option>
                        <option value="3">Admin Data</option>
                        <option value="3">CS CRM</option>
                    </Form.Select>
                </label>
                <label className="w-100 my-3">
                    STATUS
                    <Form.Select aria-label="Default select example">
                        <option disabled selected hidden>All</option>
                        <option value="1">Aktif</option>
                        <option value="2">Menunggu Verifikasi</option>
                        <option value="3">Blokir</option>
                    </Form.Select>
                </label>
                <label>
                    DATE
                    <div className='d-flex'>
                        <input className="form-control" />
                        <input className="form-control" />
                    </div>
                </label>
                <button className="my-3 border-0 bg-primary rounded text-white text-center w-100 py-1">SIMPAN</button>
            </div>
        </div>
    )
}

export default FilterCategoryPartner