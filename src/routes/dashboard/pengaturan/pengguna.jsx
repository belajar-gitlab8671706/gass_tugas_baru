import { Button, Dropdown } from "@/components";
import { Form, InputGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
    AiFillFilter,
    AiTwotoneFileZip,
    AiOutlineIdcard,
    AiOutlineCreditCard,
    AiOutlineMail,
    AiOutlineSetting,
    AiOutlineUser,
    AiOutlineArrowLeft,
    AiOutlineArrowRight,
    AiOutlineMobile,
    AiOutlineCloseCircle,
    AiOutlineMinusCircle,
    AiOutlineUserAdd
} from "react-icons/ai";
import { IoSearchOutline } from "react-icons/io5";
import { FaList } from "react-icons/fa";
import { useState } from 'react';

// state
import { useCounterStore } from '@/store'
import FilterCategoryPesanan from "./componen/filterCategoryPengguna";

export function Component() {
    const { toggleBackgroundActive, toggleBackgroundTambahPenguna } = useCounterStore()
    const [toggleFilterCategory, setToggleFilterCategory] = useState(false)
    const [categoryStyle, setCategoryStyle] = useState({})

    const btnToggleFilterCategory = () => {
        if (!toggleFilterCategory) {
            setCategoryStyle({ zIndex: 99999991 });
        } else {

            setCategoryStyle({});
        }
        toggleBackgroundActive()
        setToggleFilterCategory(!toggleFilterCategory)
    };

    return (
        <div>
            <p><Link to='/' className="h6 text-primary">Dashboard </Link> / Verifikasi Pesanan Distributor</p>

            {/* Button Group */}
            <div className="contenInputButton">
                <div className='d-flex'>
                    <div class="dropdown">
                        <button
                            class="btn dropdown-toggle btn-primary btnFile me-3"
                            type="button"
                            data-bs-toggle="dropdown"
                            aria-expanded="false"
                            style={categoryStyle}
                            onClick={btnToggleFilterCategory}
                        >
                            {
                                toggleFilterCategory ? < AiOutlineCloseCircle className='iconFile' /> : <AiFillFilter className='iconFile' />
                            }
                        </button>
                        {
                            toggleFilterCategory && (
                                <Dropdown style={categoryStyle} className="py-2 contenFilterCategory">
                                    <div className='d-flex w-100 justify-content-between dropdownList'>
                                        <FilterCategoryPesanan />
                                    </div>
                                </Dropdown>
                            )
                        }

                    </div>

                    <InputGroup className="mb-3 inputSearch">
                        <Form.Control
                            placeholder="Recipient's username"
                            aria-label="Recipient's username"
                            aria-describedby="basic-addon2"
                        />
                        <Button variant="outline-secondary" className='btn-primary btnFile' id="button-addon2">
                            <IoSearchOutline className='iconFile' />
                        </Button>
                    </InputGroup>
                    <Button onClick={toggleBackgroundTambahPenguna} className="btn-primary btnFile ms-3">
                        <AiOutlineUserAdd className='iconFile' />
                    </Button>
                </div>
            </div>

            {/* Selesai button Group */}

            {/* Penjualan List */}
            <div className='row mt-3 rounded'>
                <div className='col-5 bg-white py-2 border-end'>
                    <div className='d-flex justify-content-between'>
                        <Link className='d-flex'><AiTwotoneFileZip className='me-1' /> <p>RICING</p></Link>
                        <Link><FaList /></Link>
                    </div>
                </div>
                <div className='col-3 bg-white py-2 border-end'>
                    <p className='text-primary'><AiOutlineIdcard className='h6 me-1 d-none d-lg-inline' />WHATSAPP</p>
                </div>
                <div className='col-3 bg-white py-2 border-end'>
                    <p className='text-primary'><AiOutlineIdcard className='h6 me-1 d-none d-lg-inline' />EMAIL</p>
                </div>
                <div className='col-1 bg-white py-2'>
                    <AiOutlineSetting className='h5 d-block mx-auto' />
                </div>
            </div>
            <div className='row border mt-3 rounded'>
                <div className='col-12'>
                    <div className='d-flex justify-content-between py-3 align-items-center'>
                        <div className='d-flex justify-content-center align-items-center'>
                            <p className='bg-danger text-white px-2 textPending me-2'>PENDING</p>
                        </div>
                        <div className='d-flex align-items-center'>
                            <span><p className='me-3'><AiOutlineUser className='me-1' /> Baus</p></span>
                            <p><AiOutlineCreditCard className='me-1' /> PAYMENT</p>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-5 bg-white py-2 border-end d-flex align-items-center'>
                            <ul >
                                <li>- x1 bajukoko</li>
                                <li>- x1 Chiki</li>
                            </ul>
                        </div>
                        <div className='col-3 bg-white py-2 border-end d-flex align-items-center'>
                            <p>
                                <AiOutlineMobile className='my-0 me-1' />
                                08781233223
                            </p>
                        </div>
                        <div className='col-3 bg-white py-2 border-end d-flex align-items-center'>
                            <p>
                                <AiOutlineMail className='my-0 me-1' />
                                mambaus.jobs@gmail.com
                            </p>
                        </div>
                        <div className='col-1 bg-white py-2 d-flex align-items-center'>
                            <div className='mx-auto'>
                                <Button className='btn-secondary btnPrin d-block'><AiOutlineMinusCircle className='iconFile top-0 text-white' /></Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='d-flex justify-content-between align-items-center mt-4'>
                <span><p>Total 4 - Hal 1 dari 1</p></span>
                <div>
                    <Button className='btn-light btnFile border'><AiOutlineArrowLeft className='iconFile top-0' /></Button>
                    <Button className='btn-light ms-2 btnFile border'><AiOutlineArrowRight className='iconFile top-0' /></Button>
                </div>
            </div>
        </div>
    )
}