import './style-customer.css'
import { Button, Dropdown } from "@/components";
import { Form, InputGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
    AiOutlineUserAdd,
    AiFillFilter,
    AiTwotoneFileZip,
    AiOutlineIdcard,
    AiOutlineSync,
    AiOutlineSetting,
    AiOutlineUser,
    AiOutlineMore,
    AiOutlineArrowLeft,
    AiOutlineArrowRight,
    AiOutlineMobile,
    AiFillMail,
    AiFillNotification,
    AiOutlineCloseCircle
} from "react-icons/ai";
import { IoSearchOutline, IoMale } from "react-icons/io5";
import { FaList, FaBookmark, FaCalendar, FaFileDownload } from "react-icons/fa";
import { MdOutlineDateRange } from "react-icons/md";
import { useState } from 'react';

import FilterCustomer from './filterCustomer';
import { useCounterStore } from '@/store'

export function Component() {
    const { toggleBackgroundActive, toggleBackgroundTambahCustomer } = useCounterStore()
    const [toggleFilterCategory, setToggleFilterCategory] = useState(false)
    const [categoryStyle, setCategoryStyle] = useState({})

    const hendlebtnCategory = () => {
        if (!toggleFilterCategory) {
            setCategoryStyle({ zIndex: 99999991 })
        } else {
            setCategoryStyle({})
        }
        setToggleFilterCategory(!toggleFilterCategory)
        toggleBackgroundActive()
    }

    return (
        <div>
            <p><Link to='/' className="h6 text-primary">Dashboard </Link> / Customers</p>

            {/* Button Group */}
            <div className="contenInputButton">
                <div className='d-flex'>
                    <div class="dropdown">
                        <button
                            className="btn dropdown-toggle btn-primary btnFile me-3"
                            type="button"
                            data-bs-toggle="dropdown"
                            aria-expanded="false"
                            onClick={hendlebtnCategory}
                            style={categoryStyle}
                        >
                            {
                                toggleFilterCategory ? < AiOutlineCloseCircle className='iconFile' /> : <AiFillFilter className='iconFile' />
                            }
                        </button>
                        {
                            toggleFilterCategory && (
                                <Dropdown style={categoryStyle} className="py-2 contenFilterCategory">
                                    <div className='d-flex w-100 justify-content-between dropdownList'>
                                        <FilterCustomer />
                                    </div>
                                </Dropdown>
                            )
                        }
                    </div>

                    <InputGroup className="mb-3 inputSearch">
                        <Form.Control
                            placeholder="Recipient's username"
                            aria-label="Recipient's username"
                            aria-describedby="basic-addon2"
                        />
                        <Button variant="outline-secondary" className='btn-primary btnFile' id="button-addon2">
                            <IoSearchOutline className='iconFile' />
                        </Button>
                    </InputGroup>
                </div>
                <div className='d-flex contenButton'>
                    <Button className="btn-primary btnFile ms-md-3">  <FaFileDownload className='iconFile' /></Button>
                    <Button onClick={toggleBackgroundTambahCustomer} className="btn-primary btnFile ms-3"><AiOutlineUserAdd className='iconFile' /></Button>
                </div>
            </div>

            {/* Selesai button Group */}

            {/* Penjualan List */}
            <div className='row mt-3 rounded'>
                <div className='col-4 bg-white py-2 border-end'>
                    <div className='d-flex justify-content-between'>
                        <Link className='d-flex'><AiTwotoneFileZip className='me-1' /> <p>RICING</p></Link>
                        <Link><FaList /></Link>
                    </div>
                </div>
                <div className='col-3 bg-white py-2 border-end'>
                    <p className='text-primary'><AiOutlineIdcard className='h6 me-1 d-none d-lg-inline' /> KONTAK </p>
                </div>
                <div className='col-4 bg-white py-2 border-end'>
                    <p className='text-primary'><AiOutlineSync className='h6 me-1 d-none d-sm-inline' />PENGIRIMAN</p>
                </div>
                <div className='col-1 bg-white py-2'>
                    <AiOutlineSetting className='h5 d-block mx-auto' />
                </div>
            </div>
            <div className='row border mt-3 rounded'>
                <div className='col-12'>
                    <div className='d-flex justify-content-between py-3 align-items-center'>
                        <div className='d-flex justify-content-center align-items-center'>
                            <FaBookmark className='h5 pt-1 text-success me-1' />
                            <p className='text-success my-0'>CLOSING</p>
                            <p className='bg-danger text-white px-2 textPayment mx-2 '>CBT</p>
                        </div>
                        <div className='d-flex align-items-center'>
                            <span><p className='me-3'><AiOutlineUser className='me-1' />Baus</p></span>
                            <p><MdOutlineDateRange className='me-1' />15/11/2021</p>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-4 bg-white py-2 border-end d-flex align-items-center'>
                            <span className='fw-bold'><IoMale className='my-0' /> A. Mambaus Sholihin</span><br />
                        </div>
                        <div className='col-3 bg-white py-2 border-end d-flex align-items-center'>
                            <p>
                                <AiFillMail className='my-0' />yono@g.com<br />
                                <AiOutlineMobile className='my-0' />
                                08781233223
                            </p>
                        </div>
                        <div className='col-4 bg-white py-2 border-end'>
                            <p>
                                < AiFillNotification className='me-1' /> Facebook
                                <br />
                                <FaCalendar className='me-1' />2x Closing
                                <br />
                                From: <span className='fw-bold'>PT. Ripit</span>
                            </p>
                        </div>
                        <div className='col-1 bg-white py-2 d-flex align-items-center'>
                            <div className='mx-auto'>
                                <Button className='btn-light mt-2 btnPrin d-block border'><AiOutlineMore className='iconTitikHVertikal' /></Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='d-flex justify-content-between align-items-center mt-4'>
                <span><p>Total 4 - Hal 1 dari 1</p></span>
                <div>
                    <Button className='btn-light btnFile border'><AiOutlineArrowLeft className='iconFile top-0' /></Button>
                    <Button className='btn-light ms-2 btnFile border'><AiOutlineArrowRight className='iconFile top-0' /></Button>
                </div>
            </div>
        </div>
    )
}