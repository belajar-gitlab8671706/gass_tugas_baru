import './style-customer.css'
import { Form } from 'react-bootstrap'
import { AiOutlineClose, AiOutlineUserAdd } from "react-icons/ai";

import { useCounterStore } from '../../../store'
import { Link } from 'react-router-dom';
import { Input } from '@/components';

const TambahCustomer = () => {
    const { toggleBackgroundTambahCustomer } = useCounterStore()

    return (
        <div className="containerTambahCustomer bg-white rounded pt-2">
            <div className="d-flex justify-content-between border-bottom pt-2 pb-3 px-3">
                <h6><AiOutlineUserAdd /> Tambah Customers</h6>
                <Link to='/customer' onClick={toggleBackgroundTambahCustomer}><AiOutlineClose className='h4' /></Link>
            </div>
            <div className='px-3 pt-3'>
                <label className='w-100'>
                    NAMA
                    <Input className='w-100' />
                </label>
                <label className='w-100 pt-3'>
                    <button className='bg-light border-0 px-3 py-1 me-3'>Laki laki</button>
                    <button className='bg-light border-0 px-3 py-1'>Perempuan</button>
                </label>
                <label className='w-100 pt-3'>
                    WHATSAPP
                    <Input className='w-100' placeholder='08X XXXX XXXX' />
                </label>
                <label className='w-100 pt-3'>
                    EMAIL
                    <Input className='w-100' />
                </label>
                <label className='w-100 pt-3'>
                    SOURCE
                    <Form.Select aria-label="Default select example">
                        <option>Source</option>
                        <option value="1">Facebook</option>
                        <option value="2">Google</option>
                        <option value="3">Tik-Tok</option>
                        <option>Instagram</option>
                        <option>Teman</option>
                        <option>Lainya</option>
                    </Form.Select>
                </label>
                <button className='w-100 text-center text-white bg-primary border-0 rounded my-3 py-1'>SIMPAN</button>
            </div>
        </div>
    )
}

export default TambahCustomer
