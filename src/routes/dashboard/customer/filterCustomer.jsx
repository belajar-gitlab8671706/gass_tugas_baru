import { Button, Input } from '@/components'
import './style-customer.css'
import { Form } from 'react-bootstrap'

const FilterCustomer = () => {
    return (
        <div className='bg-white containerCustomer'>
            <div className="border-bottom py-3 px-3">
                <p>Filter Kategori</p>
            </div>
            <div className='py-3 px-2'>
                <label className='w-100'>
                    PROGRESS
                    <Form.Select aria-label="Default select example w-100">
                        <option >All...</option>
                        <option value="1">Baru</option>
                        <option value="2">Prospek</option>
                        <option value="3">Nego</option>
                        <option value="3">Closing</option>
                    </Form.Select>
                </label>
                <label className='w-100 mt-4'>
                    <p className='my-0'>TAGING</p>
                    <Button className='btn-light'>HP</Button>
                    <Button className='btn-light mx-1'>CBO</Button>
                    <Button className='btn-light'>CBT</Button>
                    <Button className='btn-light ms-1'>CST</Button>
                </label>
                <label className='w-100 mt-4'>
                    SOURCE
                    <Form.Select aria-label="Default select example w-100">
                        <option >All...</option>
                        <option value="1">Facebook</option>
                        <option value="2">Google</option>
                        <option value="3">Tik-Tok</option>
                        <option value="3">Instragram</option>
                        <option value="3">Teman</option>
                        <option value="3">Lainya</option>
                    </Form.Select>
                </label>
                <label className='w-100 mt-4'>
                    TANGGAL
                    <div className='d-flex'>
                        <Input />
                        <Input />
                    </div>
                </label>
                <button className='text-center rounder border-0 text-white btn-primary my-4 py-1 w-100'>SIMPAN</button>
            </div>
        </div>
    )
}

export default FilterCustomer