import { Form } from 'react-bootstrap'

const FilterProduct = () => {
    return (
        <div className="bg-white conten">
            <p className='mx-4 mt-2 mb-0'>Filter Kategori</p>
            <hr className='text-primary' />
            <div className="mx-3">
                <label className="w-100 ">
                    TYPE
                    <Form.Select aria-label="Default select example">
                        <option value="1">Maklon</option>
                    </Form.Select>
                </label>
                <label className='w-100 my-3'>
                    PILIH MAKLON
                    < Form.Select aria-label="Default select example">
                        <option>PT. Ripit </option>
                    </Form.Select>
                </label>
                <label className='w-100'>
                    STATUS
                    <Form.Select aria-label="Default select example">
                        <option disabled selected hidden>All</option>
                        <option value="1">Aktif</option>
                        <option value="2">Pending Verivikasi</option>
                    </Form.Select>
                </label>
                <button className='text-center text-white border-0 rounded bg-primary my-3 py-1 w-100'>SIMPAN</button>
            </div>
        </div >
    )
}

export default FilterProduct