import './style-kelolaProduct..css'
import { Link } from "react-router-dom"
import { IoMdClose } from "react-icons/io";
import { Form } from 'react-bootstrap';
import { useCounterStore } from '../../../store';
import { Input } from "@/components";

const SetelanProduct = () => {
    const { toggleBackgroundSetelanProduct } = useCounterStore()

    return (
        <div className="bg-white contenSetelProduct position-relative">
            <div className='border px-3 py-3'>
                <Link to='/product' onClick={toggleBackgroundSetelanProduct} className='position-absolute end-0 me-3'>
                    <IoMdClose className='h4' />
                </Link>
                <p className='text-center fw-bold mb-0'>Produk</p>
            </div>
            <div className="contenFormInputSetelProduct">
                <div className="row mx-5 py-4 border-bottom">
                    <div className="col-6 d-flex align-items-center">
                        <div>
                            <p className="fw-bold">Nama Produk</p>
                            <p>Nama Produk sekurangnya 3 <br /> karakter</p>
                        </div>
                    </div>
                    <div className="col-6 d-flex align-items-center">
                        <Input className='w-100' />
                    </div>
                </div>
                <div className="row mx-5 py-4 border-bottom">
                    <div className="col-6 d-flex align-items-center">
                        <div>
                            <p className="fw-bold">Informasi Produk</p>
                            <p>Isikan informasi produk seperti <br /> gambar produk, berat produk</p>
                        </div>
                    </div>
                    <div className="col-6 d-flex align-items-center">
                        <div className="w-100">
                            <input className="border w-100 text-center rounded" type="image" src='https://t4.ftcdn.net/jpg/01/07/57/91/360_F_107579101_QVlTG43Fwg9Q6ggwF436MPIBTVpaKKtb.jpg' alt="Submit" height="150" />
                            <div className="d-flex w-100 align-items-center mt-2">
                                <div className="me-2">
                                    <input className='form-control w-100' />
                                </div>
                                Berat Produk
                            </div>
                            <Form.Select className="my-2" aria-label="Default select example">
                                <option disabled selected hidden>Ukuran Berat</option>
                                <option value="1">Gram</option>
                                <option value="2">Kg</option>
                            </Form.Select>
                        </div>
                    </div>
                </div>
                <div className="row mx-5 py-4 border-bottom">
                    <div className="col-6 d-flex align-items-center">
                        <div>
                            <p className="fw-bold">Harga Produk</p>
                            <p>Tentukan harga produk sesuai <br /> dengan ketentuan yaitu, harga <br />ecer, harga distributor dan harga <br /> reseller</p>
                        </div>
                    </div>
                    <div className="col-6 d-flex align-items-center">
                        <div className="w-100">
                            <input className='w-100 form-control' placeholder='Harga' />
                            <input className='w-100 my-2 form-control' placeholder='Harga Distributor' />
                            <input className='w-100 form-control' placeholder='Harga Reseller' />
                        </div>
                    </div>
                </div>
                <div className="row mx-5 py-4">
                    <div className="col-6 d-flex align-items-center">
                        <div className="w-100">
                            <p className="fw-bold">Keterangan Produk</p>
                            <p>Isikan keterangan produk untuk <br /> informasi lebih detail</p>
                        </div>
                    </div>
                    <div className="col-6 d-flex align-items-center">
                        <textarea className="form-control" rows="7"></textarea>
                    </div>
                </div>
            </div>
            <div className='py-3 px-5 border-bottom bg-light d-flex justify-content-end'>
                <button className='bg-primary text-white border-0 py-1 px-3 rounded fw-bold'>SIMPAN</button>
            </div>
        </div>
    )
}

export default SetelanProduct