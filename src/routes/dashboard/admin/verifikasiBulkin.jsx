import { Button, Dropdown } from "@/components";
import { Form, InputGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
    AiFillFilter,
    AiTwotoneFileZip,
    AiOutlineCreditCard,
    AiOutlineSetting,
    AiOutlineCloseCircle
} from "react-icons/ai";
import { IoSearchOutline } from "react-icons/io5";
import { FaList, FaFileDownload } from "react-icons/fa";
import { CiWarning } from "react-icons/ci";
import { useState } from 'react';
import { useCounterStore } from '@/store'
import FilterCategoryBulkin from "./componen/filterBukingCategory";

export function Component() {
    const { toggleBackgroundActive } = useCounterStore()
    const [toggleFilterCategory, setToggleFilterCategory] = useState(false)
    const [categoryStyle, setCategoryStyle] = useState({})

    const btnToggleFilterCategory = () => {
        if (!toggleFilterCategory) {
            setCategoryStyle({ zIndex: 99999991 });
        } else {

            setCategoryStyle({});
        }
        toggleBackgroundActive()
        setToggleFilterCategory(!toggleFilterCategory)
    };

    return (
        <div>
            <p><Link to='/' className="h6 text-primary">Dashboard </Link> / Verifikasi Pembayaran Bulking</p>

            {/* Button Group */}
            <div className="contenInputButton">
                <div className='d-flex'>
                    <div class="dropdown">
                        <button
                            class="btn dropdown-toggle btn-primary btnFile me-3"
                            type="button"
                            data-bs-toggle="dropdown"
                            aria-expanded="false"
                            style={categoryStyle}
                            onClick={btnToggleFilterCategory}
                        >
                            {
                                toggleFilterCategory ? < AiOutlineCloseCircle className='iconFile' /> : <AiFillFilter className='iconFile' />
                            }
                        </button>
                        {
                            toggleFilterCategory && (
                                <Dropdown style={categoryStyle} className="py-2 contenFilterCategory">
                                    <div className='d-flex w-100 justify-content-between dropdownList'>
                                        <FilterCategoryBulkin />
                                    </div>
                                </Dropdown>
                            )
                        }

                    </div>

                    <InputGroup className="mb-3 inputSearch">
                        <Form.Control
                            placeholder="Recipient's username"
                            aria-label="Recipient's username"
                            aria-describedby="basic-addon2"
                        />
                        <Button variant="outline-secondary" className='btn-primary btnFile' id="button-addon2">
                            <IoSearchOutline className='iconFile' />
                        </Button>
                    </InputGroup>
                </div>
                <div className='d-flex contenButton'>
                    <Button className="btn-primary btnFile ms-md-3">
                        <FaFileDownload className='iconAdmin' />
                    </Button>
                </div>
            </div>

            {/* Selesai button Group */}

            {/* Penjualan List */}
            <div className='row mt-3 rounded'>
                <div className='col-4 bg-white py-2 border-end'>
                    <div className='d-flex justify-content-between'>
                        <Link className='d-flex'><AiTwotoneFileZip className='me-1' /> <p>RICING</p></Link>
                        <Link><FaList /></Link>
                    </div>
                </div>
                <div className='col-3 bg-white py-2 border-end'>
                    <p className='text-primary'><AiOutlineCreditCard className='h6 me-1 d-none d-lg-inline' />TOTAL BAYAR</p>
                </div>
                <div className='col-4 bg-white py-2 border-end'>
                    <p className='text-primary'><AiOutlineCreditCard className='h6 me-1 d-none d-lg-inline' />PEMBAYARAN</p>
                </div>
                <div className='col-1 bg-white py-2'>
                    <AiOutlineSetting className='h5 d-block mx-auto' />
                </div>
            </div>
            <div className="d-flex justify-content-center">
                <div className="mt-5">
                    <CiWarning className="h1 text-danger d-block mx-auto mb-0" />
                    <p className="text-danger">Data tidak ditemukan</p>
                </div>
            </div>
        </div>
    )
}