import { Input, Select } from "@/components"
import { Form } from 'react-bootstrap';

const FilterCategoryPesanan = () => {
    return (
        <div className="bg-white conten">
            <p className='mx-4 mt-2 mb-0'>Filter Kategori</p>
            <hr className='text-primary' />
            <div className="mx-3">
                <label className="w-100">
                    CUSTOMER
                    <Select
                        placeholder="All.."
                    >
                    </Select>
                </label>
                <label className="w-100 my-3">
                    STATUS
                    <Form.Select aria-label="Default select example">
                        <option disabled selected hidden>All</option>
                        <option value="1">Lunas</option>
                        <option value="2">Verify</option>
                        <option value="3">Cancel</option>
                    </Form.Select>
                </label>
                <label className="w-100">
                    TANGGAL
                    <div className="d-flex">
                        <Input />
                        <Input />
                    </div>
                </label>
                <button className="my-3 border-0 bg-primary rounded text-white text-center w-100 py-1">SIMPAN</button>
            </div>
        </div>
    )
}

export default FilterCategoryPesanan