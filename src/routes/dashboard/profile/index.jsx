import { useEffect, useRef, useState } from "react";
import { formatDate } from "@/libs/utils";
import { useProfile, useProject } from "@/services";
import { useEventListener } from "react-haiku";
import { useForm } from "react-hook-form";
import axios from "@/libs/axios";
import { useMutation, useQueryClient } from "@tanstack/react-query";

import {
  Avatar,
  Button,
  Dropdown,
  Input,
  ModalWrapper,
  Select,
} from "@/components";
import { toast } from "react-toastify";

const $contentD = $("[data-content]"),
  toggleBreak = true;
const toggleNavAttr = {
  active: "active",
  content: "content-active",
  break: toggleBreak,
};

export function Component() {
  const queryClient = useQueryClient();
  const { data: user = {}, refetch } = useProfile();
  const { data: projects = [] } = useProject();
  const { register, handleSubmit } = useForm({
    values: {
      data: user,
    },
  });

  const { mutate: updateProfile, isLoading } = useMutation(
    (data) => axios.post("", { act: "user_set_profile", ...data }),
    {
      onSuccess: (data, payload) => {
        queryClient.setQueryData(["auth", "user"], (old) => ({
          ...old,
          name: payload.name,
        }));
        refetch();
        toast.success("Profile berhasil diperbarui");
        $("#modal-change-profile").modal("hide");
      },
    }
  );

  const toggler = useRef();
  const [togglerState, setTogglerState] = useState(false);
  function handleToggler(ev) {
    NioApp.Toggle.trigger($(toggler.current).data("target"), toggleNavAttr);
    setTogglerState(toggler.current);
  }
  useEventListener("mouseup", (e) => {
    if (togglerState) {
      var $toggleCurrent = $(togglerState),
        currentTarget = $(togglerState).data("target"),
        $contentCurrent = $(`[data-content="${currentTarget}"]`),
        $dpd = $(".datepicker-dropdown"),
        $tpc = $(".ui-timepicker-container");
      if (
        !$toggleCurrent.is(e.target) &&
        $toggleCurrent.has(e.target).length === 0 &&
        !$contentCurrent.is(e.target) &&
        $contentCurrent.has(e.target).length === 0 &&
        $(e.target).closest(".select2-container").length === 0 &&
        !$dpd.is(e.target) &&
        $dpd.has(e.target).length === 0 &&
        !$tpc.is(e.target) &&
        $tpc.has(e.target).length === 0
      ) {
        NioApp.Toggle.removed($toggleCurrent.data("target"), toggleNavAttr);
        setTogglerState(false);
      }
    }
  });

  return (
    <section className="card">
      <div className="card-aside-wrap">
        <div className="card-inner card-inner-lg">
          <div className="tab-content">
            <div className="tab-pane active" id="personal">
              <div className="nk-block-head nk-block-head-lg">
                <div className="nk-block-between">
                  <div className="nk-block-head-content">
                    <h4 className="nk-block-title">Profile</h4>
                    <div className="nk-block-des">
                      <p>Kamu bisa mengatur informasi profile di sini</p>
                    </div>
                  </div>
                  <div className="nk-block-head-content d-none d-lg-block">
                    <Button
                      bg="light"
                      data-bs-toggle="modal"
                      data-bs-target="#modal-change-profile"
                    >
                      Update Profile
                    </Button>
                  </div>
                  <div className="nk-block-head-content align-self-start d-lg-none">
                    <a
                      href="#"
                      className="toggle btn btn-icon btn-trigger mt-n1"
                      data-target="userAside"
                      ref={toggler}
                      onClick={handleToggler}
                    >
                      <em className="icon ni ni-menu-alt-r"></em>
                    </a>
                  </div>
                </div>
              </div>
              <div className="nk-block">
                <div className="nk-data data-list">
                  <div className="data-head">
                    <h6 className="overline-title">Informasi</h6>
                  </div>
                  <div className="data-item">
                    <div className="data-col">
                      <span className="data-label">Nama</span>
                      <span className="data-value">{user.name}</span>
                    </div>
                    <div
                      className="data-col data-col-end"
                      data-bs-toggle="modal"
                      data-bs-target="#modal-change-profile"
                    >
                      <span className="data-more">
                        <em className="icon ni ni-forward-ios"></em>
                      </span>
                    </div>
                  </div>
                  <div className="data-item">
                    <div className="data-col">
                      <span className="data-label">Email</span>
                      <span className="data-value">{user.email}</span>
                    </div>
                    <div className="data-col data-col-end"></div>
                  </div>
                  <div className="data-item">
                    <div className="data-col">
                      <span className="data-label">No. Telepon</span>
                      <span className="data-value text-soft">{user.phone}</span>
                    </div>
                    <div className="data-col data-col-end"></div>
                  </div>
                  <div className="data-item">
                    <div className="data-col">
                      <span className="data-label">Country</span>
                      <span className="data-value">{user?.country?.name}</span>
                    </div>
                    <div className="data-col data-col-end"></div>
                  </div>
                  <div className="data-item">
                    <div className="data-col">
                      <span className="data-label">City</span>
                      <span className="data-value">{user?.city?.name}</span>
                    </div>
                    <div className="data-col data-col-end"></div>
                  </div>
                  <div className="data-item">
                    <div className="data-col">
                      <span className="data-label">Facebok</span>
                      <span className="data-value text-soft">
                        {user.facebook}
                      </span>
                    </div>
                    <div className="data-col data-col-end"></div>
                  </div>
                  <div className="data-item">
                    <div className="data-col">
                      <span className="data-label">Instagram</span>
                      <span className="data-value text-soft">
                        {user.instagram}
                      </span>
                    </div>
                    <div className="data-col data-col-end"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg"
          data-content="userAside"
          data-toggle-screen="lg"
          data-toggle-overlay="true"
        >
          <div className="card-inner-group" data-simplebar>
            <div className="card-inner">
              <div className="user-card">
                <Avatar name={user.name} />
                <div className="user-info">
                  <span className="lead-text">{user.name}</span>
                  <span className="sub-text">{user.email}</span>
                </div>
                <div className="user-action">
                  <Dropdown>
                    <Dropdown.Button className="btn-trigger me-n2" icon>
                      <em className="icon ni ni-more-v"></em>
                    </Dropdown.Button>
                    <Dropdown.Menu end>
                      <ul className="link-list-opt no-bdr">
                        <li>
                          <a href="#">
                            <em className="icon ni ni-edit-fill"></em>
                            <span>Update Profile</span>
                          </a>
                        </li>
                      </ul>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </div>
            <div className="card-inner">
              <div className="user-account-info py-0">
                <h6 className="overline-title-alt">Nama</h6>
                <p>{user.name}</p>
                <hr />
                <h6 className="overline-title-alt">Registered at</h6>
                <p>{formatDate(user.registered)}</p>
                <hr />
                <h6 className="overline-title-alt">Join in</h6>
                <p>{projects.length} Project(s)</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <ModalWrapper
        as="form"
        onSubmit={handleSubmit(updateProfile)}
        id="modal-change-profile"
      >
        <div className="modal-header">
          <h5 className="modal-title">Update Profile</h5>
          <a
            href="#"
            className="close"
            data-bs-dismiss="modal"
            aria-label="Close"
          >
            <em className="icon ni ni-cross"></em>
          </a>
        </div>
        <div className="modal-body">
          <div className="form-group">
            <label className="form-label" htmlFor="name">
              Name
            </label>
            <div className="form-control-wrap">
              <Input
                type="text"
                id="name"
                placeholder="Enter your Name"
                name="name"
                {...register("data[name]", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="form-label" htmlFor="facebook">
              Facebook
            </label>
            <div className="form-control-wrap">
              <Input
                type="text"
                id="fb"
                placeholder="Enter your Facebook"
                name="facebook"
                {...register("data[facebook]")}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="form-label" htmlFor="instagram">
              Instagram
            </label>
            <div className="form-control-wrap">
              <Input
                type="text"
                id="name"
                placeholder="Enter your name"
                name="instagram"
                {...register("data[instagram]")}
              />
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <Button
            type="submit"
            size="lg"
            bg="primary"
            loading={isLoading}
            disabled={isLoading}
          >
            Update
          </Button>
        </div>
      </ModalWrapper>
    </section>
  );
}
