import { useParams } from "react-router-dom";
import {
  Button,
  ConfirmDialog,
  PaginateTable,
  ModalWrapper,
  Input,
} from "@/components";
import { createColumnHelper } from "@tanstack/react-table";
import { useEffect, useMemo, useCallback, useState } from "react";
import { useRef } from "react";
import { create } from "zustand";
import { useMutation, useQuery } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { toast } from "react-toastify";
import { useForm } from "react-hook-form";
import { copyText, currency, formatDate } from "@/libs/utils";
import { useEventListener } from "react-haiku";

const column = createColumnHelper();
const useStore = create((set) => ({
  bill: null,
  setBill: (bill) => set({ bill }),
}));

export function Component() {
  const { project_key } = useParams();
  const { bill, setBill } = useStore();

  const table = useRef();
  const payload = useMemo(
    () => ({ act: "saldo_get", project_key }),
    [project_key]
  );
  const columns = useMemo(
    () => [
      column.accessor("tanggal", {
        header: "#",
        cell: (cell) => (
          <div>
            {cell.row.original.status === 1 ? (
              <span className="badge bg-success mb-2 d-sm-none">Approved</span>
            ) : (
              <span className="badge bg-light mb-2 d-sm-none">Pending</span>
            )}
            <div>{formatDate(cell.getValue())}</div>
            <strong className="fs-6 d-md-none">
              {currency(cell.row.original.nominal)}
            </strong>
          </div>
        ),
      }),
      column.accessor("nominal", {
        header: "Nominal",
        className: "tb-col-md",
        cell: (cell) => <strong>{currency(cell.getValue())}</strong>,
      }),
      column.accessor("status", {
        header: "Status",
        className: "tb-col-sm",
        cell: (cell) =>
          cell.getValue() === 1 ? (
            <span className="badge bg-success">Lunas</span>
          ) : (
            <span className="badge bg-light">Pending</span>
          ),
      }),
      column.accessor("keterangan", {
        header: "Keterangan",
        // className: "tb-col-lg",
        cell: (cell) => (
          <div className="text-truncate" style={{ maxWidth: "200px" }}>
            {cell.getValue()}
          </div>
        ),
      }),
      column.accessor("data", {
        header: "",
        // className: "tb-col-lg",
        cell: (cell) => (
          <div className="d-flex align-items-center" style={{ gap: "1rem" }}>
            {cell.getValue().map((item) =>
              item.text ? (
                <Button
                  as="a"
                  href={item.link}
                  target="_blank"
                  className="text-primary"
                  icon
                >
                  {item.text}
                  <em
                    className="icon ni ni-external-alt"
                    style={{ fontSize: "0.75rem", width: "1rem" }}
                  ></em>
                </Button>
              ) : (
                <Button
                  bg="primary"
                  {...(item.bill_id && {
                    onClick: () => {
                      setBill({ ...item });
                      $("#saldo-modal").modal("show");
                    },
                  })}
                >
                  {item.button}
                  <em className="icon ni ni-wallet-out ms-1 fs-4"></em>
                </Button>
              )
            )}
          </div>
        ),
      }),
    ],
    []
  );

  const refresh = useCallback(() => table.current.refetch(), [table.current]);

  const { data: usage = {} } = useQuery({
    queryKey: [project_key, "bill-usage"],
    queryFn: () =>
      axios
        .post("", { act: "client_bill_get_usage", project_key })
        .then((res) => res.data.data),
  });

  return (
    <section>
      <div
        className="d-flex flex-wrap mb-4 justify-content-between"
        style={{ rowGap: "1rem" }}
      >
        <h3 className="mb-0">Billing</h3>
        <div className="d-flex" style={{ gap: "0.5rem" }}>
          <Button
            bg="outline-primary"
            size="lg"
            data-bs-toggle="modal"
            data-bs-target="#notif-modal"
          >
            <i className="icon ni ni-spark-fill fs-4 me-1"></i>
            Set Notif
          </Button>
          <Button
            bg="primary"
            size="lg"
            data-bs-toggle="modal"
            data-bs-target="#saldo-modal"
            onClick={() => setBill(null)}
          >
            <i className="icon ni ni-grid-plus fs-4 me-1"></i>
            Top Up
          </Button>
        </div>
      </div>

      <div className="card mb-4">
        <div
          className="card-header pt-5 bg-primary"
          style={{ paddingBottom: "5rem" }}
        >
          <h3 className="card-title fw-normal text-white">
            Total Usage:{" "}
            <span className="fw-bold">
              {currency(usage.total?.total, {
                notation: "compact",
                maximumFractionDigits: 3,
              }).replace(",00", "")}
            </span>
          </h3>
        </div>
        <div className="card-inner" style={{ marginTop: "-4rem" }}>
          <div className="row" style={{ rowGap: "1rem" }}>
            <div className="col-lg-3 col-sm-6">
              <div className="card bg-gray analytic-data-group analytic-ov-group">
                <div className="card-body analytic-data">
                  <div className="title text-white fs-5 fw-normal">
                    <em className="ni ni-globe me-1"></em>
                    Site
                  </div>
                  <div className="amount text-white fs-2 fw-bold">
                    <div className="mb-1">
                      {currency(usage.total?.total_bill_site, {
                        notation: "compact",
                        maximumFractionDigits: 3,
                      }).replace(",00", "")}
                    </div>
                    <div className="fw-normal fs-6">
                      {" "}
                      in {usage.total?.total_day_site} day(s)
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="card bg-teal analytic-data-group analytic-ov-group">
                <div className="card-body analytic-data">
                  <div className="title text-white fs-5 fw-normal">
                    <em className="ni ni-whatsapp me-1"></em>
                    CTWA
                  </div>
                  <div className="amount text-white fs-2 fw-bold">
                    <div className="mb-1">
                      {currency(usage.total?.total_bill_ctwa, {
                        notation: "compact",
                        maximumFractionDigits: 3,
                      }).replace(",00", "")}
                    </div>
                    <div className="fw-normal fs-6">
                      {" "}
                      in {usage.total?.total_day_ctwa} day(s)
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="card bg-info analytic-data-group analytic-ov-group">
                <div className="card-body analytic-data">
                  <div className="title text-white fs-5 fw-normal">
                    <em className="ni ni-account-setting me-1"></em>
                    Customer Service
                  </div>
                  <div className="amount text-white fs-2 fw-bold">
                    <div className="mb-1">
                      {currency(usage.total?.total_bill_cs, {
                        notation: "compact",
                        maximumFractionDigits: 3,
                      }).replace(",00", "")}
                    </div>
                    <div className="fw-normal fs-6">
                      {" "}
                      in {usage.total?.total_day_cs} day(s)
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="card bg-dark analytic-data-group analytic-ov-group">
                <div className="card-body analytic-data">
                  <div className="title text-white fs-5 fw-normal">
                    <em className="ni ni-spark me-1"></em>
                    CTA
                  </div>
                  <div className="amount text-white fs-2 fw-bold">
                    <div className="mb-1">
                      {currency(usage.total?.total_bill_cta, {
                        notation: "compact",
                        maximumFractionDigits: 3,
                      }).replace(",00", "")}
                    </div>
                    <div className="fw-normal fs-6">
                      {" "}
                      in {usage.total?.total_day_cta} day(s)
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="card">
        <div className="card-header bg-white">
          <strong>{usage.start}</strong> to <strong>{usage.end}</strong>
        </div>
        <PaginateTable
          scrollable
          ref={table}
          payload={payload}
          columns={columns}
          querykey={["auth", "user", "saldo"]}
          target="id"
        />
      </div>

      <FormTopup refresh={refresh} />
      <FormNotif refresh={refresh} />
    </section>
  );
}

export function FormTopup({ refresh }) {
  const { bill, setBill } = useStore();

  const { register, handleSubmit, reset } = useForm({
    values: {
      bill_id: bill?.bill_id,
      nominal: bill?.nominal?.toLocaleString("id-ID"),
    },
  });

  const { mutate, isLoading } = useMutation(
    (data) =>
      axios
        .post("", {
          act: "saldo_topup",
          ...data,
          nominal: parseFloat(
            data.nominal.replace(/\./g, "").replace(",", ".")
          ),
        })
        .then((res) => res.data),
    {
      onSuccess: (data) => {
        refresh && refresh();
        reset({});
        $("#saldo-modal #nominal").val("");
        $("#saldo-modal").modal("hide");
        toast.success("Top-Up saldo berhasil");
        setBill(null);
        window.open(data.link_url || data.result.link_url, "_blank");
      },
      onError: (err) => {
        toast.error(() => (
          <div
            dangerouslySetInnerHTML={{
              __html: err.message?.replace("<br>", ""),
            }}
          ></div>
        ));
      },
    }
  );

  const modal = useRef();
  useEventListener(
    "hidden.bs.modal",
    function () {
      setBill(null);
    },
    modal
  );

  return (
    <ModalWrapper id="saldo-modal" ref={modal}>
      <form id="form-saldo" onSubmit={handleSubmit(mutate)}>
        <div className="modal-header">
          <h5 className="modal-title">Top Up</h5>
          <a
            href="#"
            className="close"
            data-bs-dismiss="modal"
            aria-label="Close"
          >
            <em className="icon ni ni-cross"></em>
          </a>
        </div>
        <div className="modal-body">
          <div>
            <div className="form-group">
              <label className="form-label" htmlFor="nominal">
                Nominal
              </label>
              <div className="form-control-wrap">
                <Input
                  type="currency"
                  id="nominal"
                  placeholder="Masukkan nominal"
                  allowNegativeValue={false}
                  readOnly={Boolean(bill?.bill_id)}
                  {...register("nominal", { required: true })}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <Button
            type="submit"
            size="lg"
            bg="primary"
            loading={isLoading}
            disabled={isLoading}
          >
            Top Up
          </Button>
        </div>
      </form>
    </ModalWrapper>
  );
}

export function FormNotif({ refresh }) {
  const { register, handleSubmit, reset } = useForm();
  const { mutate, isLoading } = useMutation(
    (data) =>
      axios
        .post("", {
          act: "billing_set_alert",
          nominal: parseFloat(
            data.nominal.replace(/\./g, "").replace(",", ".")
          ),
        })
        .then((res) => res.data),
    {
      onSuccess: (data) => {
        $("#notif-modal #nominal").val("");
        refresh();
        reset({});
        $("#notif-modal").modal("hide");
        toast.success("Berhasil mengatur notifikasi");
      },
      onError: (err) => {
        toast.error(() => (
          <div
            dangerouslySetInnerHTML={{
              __html: err.message?.replace("<br>", ""),
            }}
          ></div>
        ));
      },
    }
  );

  return (
    <ModalWrapper id="notif-modal">
      <form id="form-notif" onSubmit={handleSubmit(mutate)}>
        <div className="modal-header">
          <h5 className="modal-title">Set Billing Notif</h5>
          <a
            href="#"
            className="close"
            data-bs-dismiss="modal"
            aria-label="Close"
          >
            <em className="icon ni ni-cross"></em>
          </a>
        </div>
        <div className="modal-body">
          <div>
            <div className="form-group">
              <label className="form-label" htmlFor="nominal">
                Nominal
              </label>
              <div className="form-control-wrap">
                <Input
                  type="currency"
                  id="nominal"
                  placeholder="Masukkan nominal"
                  allowNegativeValue={false}
                  min={250000}
                  max={5000000}
                  {...register("nominal", {
                    required: true,
                    min: 250000,
                    max: 5000000,
                  })}
                />
                <div className="text-muted mt-2">
                  Min: 250.000 ; Max: 5.000.000
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <Button
            type="submit"
            size="lg"
            bg="primary"
            loading={isLoading}
            disabled={isLoading}
          >
            Top Up
          </Button>
        </div>
      </form>
    </ModalWrapper>
  );
}
