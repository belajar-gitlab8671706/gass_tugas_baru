import { useRef, useEffect, useMemo, useState } from "react";
import '../dashboard/kelolaPesanan/style-pesanan.css'

import {
  Link,
  Outlet,
  useNavigation,
  useNavigate,
  useLocation,
  useParams,
  useMatch,
} from "react-router-dom";
import { For } from "react-haiku";
import {
  SidebarMenu,
  Avatar,
  Button,
  Switch,
  Dropdown,
  ModalWrapper,
  Input,
} from "@/components";

import { useQueryClient } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { useEventListener } from "react-haiku";
import { useProject, useSaldo, useUser } from "@/services";
import { toast } from "react-toastify";
import { currency } from "@/libs/utils";
import { FormTopup } from "./billing";

// state
import { useCounterStore } from '../../store'
import Product from "./kelolaPesanan/komponen/product";
import ExportFormatDefault from "./kelolaPesanan/komponen/exportFormatDefault";
import PesananDistributor from "./kelolaPesanan/komponen/componenpesananDistributor";
import TambahPengguna from "./pengaturan/componen/tambahPengguna";
import TambahCustomer from "./customer/tambahCustomer";
import ExportFormatDefaultAdmin from "./admin/componen/exportFormat";
import SetelanProduct from "./kelolaProduct/setelanProduct";
import TambahPatner from "./pengaturan/componen/tambahPatner";
import StatusOrder from "./kelolaPesanan/komponen/statusOrder";
import UpdatePesananDistributor from "./kelolaPesanan/komponen/updatePesananDistributor";
import CustomerPesanan from "./kelolaPesanan/komponen/customerPesanan";
import KeranjangPesanan from "./kelolaPesanan/komponen/keranjangPesanan";
import ChekoutPesanan from "./kelolaPesanan/komponen/checkout";
import Pembayaran from "./kelolaPesanan/komponen/pembayaran";

const $contentD = $("[data-content]"),
  toggleBreak = $contentD.hasClass("nk-header-menu") ? 992 : 1200,
  toggleOlay = "nk-sidebar-overlay",
  toggleClose = { profile: true, menu: false };
const toggleNavAttr = {
  active: "toggle-active",
  content: "nk-sidebar-active",
  body: "nav-shown",
  // overlay: toggleOlay,
  break: toggleBreak,
  close: toggleClose,
};

export function Component() {
  const sidebarRef = useRef(null);
  function compactSidebar(ev) {
    ev.preventDefault();
    ev.target.classList.toggle("compact-active");
    sidebarRef.current.classList.toggle("is-compact");
  }

  const {
    background,
    backgroundProduct,
    backgroundExportDefault,
    backgroundExportToImazi,
    backgroundPesananDistributor,
    backgroundTambahCustomer,
    backgroundExportToImaziAdmin,
    backgroundExportDefaultAdmin,
    backgroundSetelanProduct,
    backgroundTambahPengguna,
    backgroundTambahPatner,
    backgroundStatusOrder,
    backgroundUpdatePesananDistributor,
    pemesananCustomer,
    keranjangPesanan,
    checkoutPesanan,
    btnPembayaran
  } = useCounterStore()

  const toggleNavRef = useRef(null);
  const toggleBackNavRef = useRef(null);

  function handleToggleNav(ev) {
    ev.preventDefault();

    NioApp.Toggle.trigger(
      $(toggleNavRef.current).data("target"),
      toggleNavAttr
    );
  }
  function handleToggleBackNav(ev) {
    ev.preventDefault();

    NioApp.Toggle.trigger(
      $(toggleBackNavRef.current).data("target"),
      toggleNavAttr
    );
  }

  useEventListener(
    "mouseup",
    function (e) {
      if (
        !$(toggleNavRef.current).is(e.target) &&
        $(toggleNavRef.current).has(e.target).length === 0 &&
        !$(toggleBackNavRef.current).is(e.target) &&
        $(toggleBackNavRef.current).has(e.target).length === 0 &&
        !$(sidebarRef.current).is(e.target) &&
        $(sidebarRef.current).has(e.target).length === 0 &&
        NioApp.Win.width < toggleBreak
      ) {
        NioApp.Toggle.removed(
          $(toggleNavRef.current).data("target"),
          toggleNavAttr
        );
      }
    },
    document
  );

  useEventListener(
    "resize",
    function () {
      if (NioApp.Win.width < 1200 || NioApp.Win.width < toggleBreak) {
        NioApp.Toggle.removed(
          $(toggleNavRef.current).data("target"),
          toggleNavAttr
        );
      }
    },
    window
  );

  const { pathname } = useLocation();
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const { data: user = {} } = useUser();

  const {
    data: projects = [],
    isLoading: isLoadingProjects,
    refetch,
  } = useProject();
  const { project_key } = useParams();
  const key = useMemo(() => {
    if (
      !projects.find(
        (project) => project.project_key === localStorage.getItem("project_key")
      )
    ) {
      return projects[0]?.project_key;
    }

    return (
      (projects.find((project) => project.project_key === project_key)
        ?.project_key &&
        project_key) ||
      localStorage.getItem("project_key") ||
      projects[0]?.project_key
    );
  }, [project_key, localStorage.getItem("project_key"), projects]);

  const [firstVisit, setFirstVisit] = useState(false);
  useEffect(() => {
    if (key && !firstVisit && !project_key) {
      navigate(`/${key}/report/overview`);
      setFirstVisit(true);
    }
  }, [key]);

  const currentProject = useMemo(() => {
    return projects.find((project) => project.project_key === key);
  });

  const toastProject = useRef();
  useEffect(() => {
    if (toastProject.current) {
      toast.dismiss(toastProject.current);
    }

    setTimeout(() => {
      if (currentProject?.status == 0) {
        toastProject.current = toast.info(
          () => (
            <div>
              <h4>Project Creation in Progress</h4>
              <div className="mb-2">
                Hello there! We're currently setting up your project. Hang
                tight, we're working hard to get everything ready for you.
              </div>
              <div>Thanks for choosing Gass for your project!</div>
            </div>
          ),
          {
            position: "top-center",
            autoClose: 12000,
          }
        );
      }
    }, 200);
  }, [currentProject, pathname]);

  const [editProject, setEditProject] = useState(null);

  useEffect(() => {
    const interval = setInterval(() => {
      if (projects.some((project) => project.status == 0)) refetch();
    }, 5000);

    return () => clearInterval(interval);
  }, [projects]);

  useEventListener(
    "click",
    (ev) => {
      const elemPaths = ev.composedPath() || ev.path;
      if (
        elemPaths.some(
          (elem) =>
            (elem.getAttribute &&
              elem.getAttribute("class") === "nk-menu-link") ||
            elem.classList?.contains("logo-link")
        )
      ) {
        toggleBackNavRef.current.click();

        setTimeout(() => {
          document.body.classList.remove("nav-shown");
        }, 100);
      }
    },
    sidebarRef
  );

  useEffect(() => {
    if (!isLoadingProjects) {
      if (key) localStorage.setItem("project_key", key);
      else {
        localStorage.removeItem("project_key");
        navigate("/");
      }
    }
  }, [key, isLoadingProjects]);

  function handleLogout() {
    Swal.fire({
      title: "Apakah Anda yakin ingin Logout?",
      showCancelButton: true,
      confirmButtonText: "Ya, Logout",
      preConfirm: () =>
        axios.post("", { act: "user_logout" }).then((res) => res.data),
      allowOutsideClick: () => !Swal.isLoading(),
    }).then((result) => {
      if (result.isConfirmed) {
        localStorage.removeItem("auth-token");
        localStorage.removeItem("project_key");
        navigate("/auth/login");
        queryClient.clear();
        queryClient.invalidateQueries(["auth", "user"]);
      }
    });
  }

  useEffect(() => {
    $.timeField.init({ interval: 1 });
  }, []);

  const { data: saldo = {} } = useSaldo();

  return (
    <div className="row">
      <div className="position-relative">
        <div className="nk-app-root">
          <div className="nk-main">
            <div
              className="nk-sidebar nk-sidebar-fixed is-light"
              data-content="sidebarMenu"
              ref={sidebarRef}
              style={{ zIndex: 999999 }}
            >
              <div className="nk-sidebar-element nk-sidebar-head">
                <div className="nk-sidebar-brand">
                  <Link to="/" className="logo-link nk-sidebar-logo">
                    <img
                      className="logo-light logo-img mh-100"
                      src="/images/GASS LOGO.png"
                      alt="logo"
                    ></img>
                    <img
                      className="logo-dark logo-img mh-100"
                      src="/images/GASS LOGO.png"
                      alt="GASS LOGO"
                      height={50}
                    ></img>
                    <img
                      className="logo-small logo-img logo-img-small mh-100"
                      src="/images/GASS LOGO.png"
                      alt="GASS LOGO"
                    ></img>
                  </Link>
                </div>
                <div className="nk-menu-trigger me-n2">
                  <Link
                    to="#"
                    className="nk-nav-toggle nk-quick-nav-icon d-xl-none"
                    data-target="sidebarMenu"
                    onClick={handleToggleBackNav}
                    ref={toggleBackNavRef}
                  >
                    <em className="icon ni ni-arrow-left"></em>
                  </Link>
                  <Link
                    to="#"
                    className="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex"
                    data-target="sidebarMenu"
                    onClick={compactSidebar}
                  >
                    <em className="icon ni ni-menu"></em>
                  </Link>
                </div>
              </div>

              <div className="nk-sidebar-element">
                <div className="nk-sidebar-content">
                  <div className="nk-sidebar-menu pb-4" data-simplebar>
                    <ul className="nk-menu d-flex flex-column h-100">

                      {/* Projects Menu */}

                      <SidebarMenu.Item>
                        <Link
                          to=""
                          className="nk-menu-link"
                        >
                          <span className="nk-menu-icon">
                            <em className="icon ni ni-opt-dot-alt"></em>
                          </span>
                          <span className="nk-menu-text">Dashboard</span>
                        </Link>
                      </SidebarMenu.Item>

                      <SidebarMenu.Dropdown
                        active={location.pathname.includes("/report")}
                      >
                        <SidebarMenu.Toggle>
                          <span className="nk-menu-icon">
                            <em className="icon ni ni-file-docs"></em>
                          </span>
                          <span className="nk-menu-text">Kelola Pesanan</span>
                        </SidebarMenu.Toggle>
                        <SidebarMenu.Menu>
                          <SidebarMenu.Item
                            active={location.pathname.includes("/report/overview")}
                          >
                            <Link
                              to="pesanan"
                              className="nk-menu-link"
                            >
                              <span className="nk-menu-text">Pesanan</span>
                            </Link>
                          </SidebarMenu.Item>
                          <SidebarMenu.Item
                            active={location.pathname.startsWith("/report/ads")}
                          >
                            <Link
                              to='pesanan-distributor'
                              className="nk-menu-link"
                            >
                              <span className="nk-menu-text">Pesanan Distributor</span>
                            </Link>
                          </SidebarMenu.Item>
                        </SidebarMenu.Menu>
                      </SidebarMenu.Dropdown>

                      <SidebarMenu.Dropdown
                        active={location.pathname.includes("/report")}
                      >
                        <SidebarMenu.Toggle>
                          <span className="nk-menu-icon">
                            <em className="icon ni ni-file-docs"></em>
                          </span>
                          <span className="nk-menu-text">Kelola Custmers</span>
                        </SidebarMenu.Toggle>
                        <SidebarMenu.Menu>
                          <SidebarMenu.Item
                            active={location.pathname.includes("/report/overview")}
                          >
                            <Link
                              to='/customer'
                              className="nk-menu-link"
                            >
                              <span className="nk-menu-text">Customer</span>
                            </Link>
                          </SidebarMenu.Item>
                        </SidebarMenu.Menu>
                      </SidebarMenu.Dropdown>

                      <SidebarMenu.Dropdown
                        active={location.pathname.includes("/report")}
                      >
                        <SidebarMenu.Toggle>
                          <span className="nk-menu-icon">
                            <em className="icon ni ni-file-docs"></em>
                          </span>
                          <span className="nk-menu-text">Admin</span>
                        </SidebarMenu.Toggle>
                        <SidebarMenu.Menu>
                          <ul>
                            <li>
                              <SidebarMenu.Item
                                active={location.pathname.includes("/report/overview")}
                              >
                                <Link
                                  to='/verifikasi-pembayaran'
                                  className="nk-menu-link"
                                >
                                  <span className="nk-menu-text">Verifikasi Pembayaran</span>
                                </Link>
                              </SidebarMenu.Item>
                            </li>
                            <li>
                              <SidebarMenu.Item
                                active={location.pathname.startsWith("/report/ads")}
                              >
                                <Link
                                  to='/verifikasi-bulking'
                                  className="nk-menu-link"
                                >
                                  <span className="nk-menu-text">Verifikasi Bulking</span>
                                </Link>
                              </SidebarMenu.Item>
                            </li>
                            <li>
                              <SidebarMenu.Item
                                active={location.pathname.startsWith("/report/ads")}
                              >
                                <Link
                                  to='/verifikasi-pesanan'
                                  className="nk-menu-link"
                                >
                                  <span className="nk-menu-text">Verifikasi Pesanan Distributor</span>
                                </Link>
                              </SidebarMenu.Item>
                            </li>
                          </ul>
                          <SidebarMenu.Item
                            active={location.pathname.startsWith("/report/ads")}
                          >
                            <Link
                              to='/shiping'
                              className="nk-menu-link"
                            >
                              <span className="nk-menu-text">Shiping</span>
                            </Link>
                          </SidebarMenu.Item>
                        </SidebarMenu.Menu>
                      </SidebarMenu.Dropdown>

                      <SidebarMenu.Dropdown
                        active={location.pathname.includes("/report")}
                      >
                        <SidebarMenu.Toggle>
                          <span className="nk-menu-icon">
                            <em className="icon ni ni-file-docs"></em>
                          </span>
                          <span className="nk-menu-text">Kelola Produk</span>
                        </SidebarMenu.Toggle>
                        <SidebarMenu.Menu>
                          <SidebarMenu.Item
                            active={location.pathname.includes("/report/overview")}
                          >
                            <Link
                              to='/product'
                              className="nk-menu-link"
                            >
                              <span className="nk-menu-text">Produk</span>
                            </Link>
                          </SidebarMenu.Item>
                        </SidebarMenu.Menu>
                      </SidebarMenu.Dropdown>

                      <SidebarMenu.Dropdown
                        active={location.pathname.includes("/report")}
                      >
                        <SidebarMenu.Toggle>
                          <span className="nk-menu-icon">
                            <em className="icon ni ni-file-docs"></em>
                          </span>
                          <span className="nk-menu-text">Pengaturan</span>
                        </SidebarMenu.Toggle>
                        <SidebarMenu.Menu>
                          <SidebarMenu.Item
                            active={location.pathname.includes("/report/overview")}
                          >
                            <Link
                              to='/pengguna'
                              className="nk-menu-link"
                            >
                              <span className="nk-menu-text">Penggunaan</span>
                            </Link>
                          </SidebarMenu.Item>
                          <SidebarMenu.Item
                            active={location.pathname.startsWith("/report/ads")}
                          >
                            <Link
                              to='/partner'
                              className="nk-menu-link"
                            >
                              <span className="nk-menu-text">Partner</span>
                            </Link>
                          </SidebarMenu.Item>
                          <SidebarMenu.Item
                            active={location.pathname.startsWith("/report/ads")}
                          >
                            <Link
                              to='/pengaturan-umum'
                              className="nk-menu-link"
                            >
                              <span className="nk-menu-text">Pengaturan Umum</span>
                            </Link>
                          </SidebarMenu.Item>
                        </SidebarMenu.Menu>
                      </SidebarMenu.Dropdown>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="nk-wrap">
              <div className="nk-header nk-header-fixed is-light">
                <div className="container-fluid">
                  <div className="nk-header-wrap">
                    <div className="nk-menu-trigger d-xl-none ms-n1">
                      <Link
                        to="#"
                        className="nk-nav-toggle nk-quick-nav-icon"
                        data-target="sidebarMenu"
                        onClick={handleToggleNav}
                        ref={toggleNavRef}
                      >
                        <em className="icon ni ni-menu"></em>
                      </Link>
                    </div>
                    <div className="nk-header-brand d-none d-sm-block d-xl-none">
                      <Link to="/" className="logo-link">
                        <img
                          className="logo-light logo-img mh-100 mh-100"
                          src="/images/GASS LOGO.png"
                          alt="logo"
                        ></img>
                        <img
                          className="logo-dark logo-img mh-100"
                          src="/images/GASS LOGO.png"
                          alt="GASS LOGO"
                        ></img>
                      </Link>
                    </div>

                    <div className="nk-header-tools">
                      <ul className="nk-quick-nav">
                        <li className="dropdown notification-dropdown">
                          <Link
                            to="#"
                            className="dropdown-toggle nk-quick-nav-icon"
                            data-bs-toggle="dropdown"
                          >
                            <em className="icon ni ni-bell"></em>
                          </Link>
                          <div className="dropdown-menu dropdown-menu-xl dropdown-menu-end">
                            <div className="dropdown-head">
                              <span className="sub-title nk-dropdown-title">
                                Notifications
                              </span>
                            </div>
                            <div className="dropdown-body">
                              <div className="nk-notification">
                                {/* <For
                              each={orderedNotifs}
                              render={(notif) => (
                                <Link
                                  className="nk-notification-item dropdown-inner p-2"
                                  key={notif.id}
                                  {...(notif.link && {
                                    to: notif.link,
                                    target: notif.link.includes("http")
                                      ? "_blank"
                                      : "",
                                  })}
                                  onClick={() => readNotif(notif.id)}
                                >
                                  <div className="nk-notification-icon">
                                    <em className="icon icon-circle bg-info-dim ni ni-info"></em>
                                  </div>
                                  <div className="nk-notification-content">
                                    <div className="nk-notification-time">
                                      <span
                                        className={`nk-notification-time timefield t:${Date.parse(
                                          notif.date
                                        )}`}
                                      ></span>
                                    </div>
                                    <div className="nk-notification-text">
                                      <div>{notif.title}</div>
                                      <div className="text-muted text-truncate">
                                        {notif.description}
                                      </div>
                                    </div>
                                  </div>
                                </Link>
                              )}
                            /> */}
                              </div>
                            </div>
                            <div className="dropdown-foot center">
                              <Link to="/notification">View All</Link>
                            </div>
                          </div>
                        </li>
                        <li>
                          <Link
                            to="#"
                            className="dropdown-toggle nk-quick-nav-icon"
                            data-bs-toggle="dropdown"
                          >
                            <em className="icon ni ni-bell"></em>
                          </Link>
                        </li>
                        <li>
                          <Dropdown Dropdown className="dropdown user-dropdown">
                            <Dropdown.Button
                              as={Link}
                              className="dropdown-toggle me-n1"
                            >
                              <div className="user-toggle">
                                {user.profile_pic ? (
                                  <Avatar size="sm" src={user.profile_pic} />
                                ) : (
                                  <Avatar size="sm">
                                    <em className="icon ni ni-user-alt"></em>
                                  </Avatar>
                                )}
                                <div className="user-info d-none d-xl-block">
                                  <div className="user-name dropdown-indicator">
                                    {user?.name}
                                  </div>
                                </div>
                              </div>
                            </Dropdown.Button>
                            <Dropdown.Menu className="dropdown-menu-md w-100" end>
                              <div className="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                <div className="user-card">
                                  {user.profile_pic ? (
                                    <Avatar src={user.profile_pic} />
                                  ) : (
                                    <Avatar name={user?.name} />
                                  )}
                                  <div className="user-info">
                                    <span className="lead-text user-name">
                                      {user?.name}
                                    </span>
                                    <span className="sub-text"></span>
                                  </div>
                                </div>
                              </div>
                              <div className="dropdown-inner">
                                <ul className="link-list">
                                  <li>
                                    <Link to="profile">
                                      <em
                                        className="icon ni ni-user-circle"
                                        style={{ fontSize: "1.5rem" }}
                                      ></em>
                                      <span className="ms-2">My Profile</span>
                                    </Link>
                                  </li>
                                  <li>
                                    <Link to={`/${key}/billing`}>
                                      <em
                                        className="icon ni ni-cc-alt"
                                        style={{ fontSize: "1.5rem" }}
                                      ></em>
                                      <span className="ms-2">Billing</span>
                                    </Link>
                                  </li>
                                </ul>
                              </div>
                              <div className="dropdown-inner">
                                <ul className="link-list">
                                  <li>
                                    <Link onClick={handleLogout}>
                                      <em
                                        className="icon ni ni-signout"
                                        style={{
                                          transform: "rotateY(180deg)",
                                          fontSize: "1.5rem",
                                        }}
                                      ></em>
                                      <span className="ms-2">Log Out</span>
                                    </Link>
                                  </li>
                                </ul>
                              </div>
                            </Dropdown.Menu>
                          </Dropdown>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="nk-content pos-rel">
                <div className="container-fluid">
                  {currentProject?.status == 1 && !isSubdomainLoading && <Outlet />}
                  <Outlet />
                </div>
              </div>
              <div className="nk-footer">
                <div className="container-fluid">
                  <div className="nk-footer-wrap">
                    <div className="nk-footer-copyright">
                      {" "}
                      © 2023 {import.meta.env.VITE_TITLE}
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <LoadingLayer />
          {!pathname.includes("billing") && <FormTopup />}
        </div>
        {
          background && (
            <div className='background d-flex justify-content-center align-items-center'>

            </div>
          )
        }
        {
          backgroundProduct && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              <Product />
            </div>
          )
        }
        {
          backgroundExportDefault && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < ExportFormatDefault formatText="Format Default" />
            </div>
          )
        }
        {
          backgroundExportToImazi && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < ExportFormatDefault formatText="Format to Imazi" />
            </div>
          )
        }
        {
          backgroundExportToImaziAdmin && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < ExportFormatDefaultAdmin formatText="Format to Imazi" />
            </div>
          )
        }
        {
          backgroundExportDefaultAdmin && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < ExportFormatDefaultAdmin formatText="Format Default" />
            </div>
          )
        }
        {
          backgroundSetelanProduct && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < SetelanProduct />
            </div>
          )
        }
        {
          backgroundTambahPatner && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < TambahPatner />
            </div>
          )
        }
        {
          backgroundPesananDistributor && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < PesananDistributor />
            </div>
          )
        }
        {
          backgroundTambahPengguna && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < TambahPengguna />
            </div>
          )
        }
        {
          checkoutPesanan && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              <ChekoutPesanan />
            </div>
          )
        }
        {
          backgroundStatusOrder && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < StatusOrder />
            </div>
          )
        }
        {
          backgroundUpdatePesananDistributor && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < UpdatePesananDistributor />
            </div>
          )
        }
        {
          pemesananCustomer && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              <CustomerPesanan />
            </div>
          )
        }
        {
          keranjangPesanan && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              <KeranjangPesanan />
            </div>
          )
        }
        {
          btnPembayaran && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              <Pembayaran />
            </div>
          )
        }
        {
          backgroundTambahCustomer && (
            <div className='background d-flex justify-content-center align-items-center px-1'>
              < TambahCustomer />
            </div>
          )
        }
      </div>
    </div>
  );
}

function LoadingLayer({ loading }) {
  const navigation = useNavigation();
  const loadingRef = useRef(null);

  useEffect(() => {
    if (navigation.state === "loading" || loading === true) {
      loadingRef.current.style.display = "flex";
    } else {
      setTimeout(() => {
        if (loadingRef.current) loadingRef.current.style.display = "none";
      }, 200);
    }
  }, [navigation, loadingRef]);

  return (
    <div
      className="position-fixed justify-content-center align-items-center"
      style={{
        top: 0,
        left: 0,
        right: 0,
        height: "100vh",
        zIndex: 1100,
        backgroundColor: "rgba(15, 10, 25, 0.1)",
        transition: "all 0.2s ease",
        opacity: navigation.state === "loading" || loading === true ? 1 : 0,
      }}
      ref={loadingRef}
    >
      <div className="nk-loading">
        <div className="spinner-border text-blue" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    </div>
  );
}

Component.DisplayName = "DashboardRoot";
