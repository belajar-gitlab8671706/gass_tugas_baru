import './style-homeDashboard.css'
import { Button } from "@/components";
import {
    AiOutlineShoppingCart,
    AiOutlineClockCircle,
    AiOutlineTeam,
    AiOutlineReload,
    AiTwotoneFilePdf,
    AiOutlineMore,
    AiOutlineUser,
    AiOutlineCheckCircle,
} from "react-icons/ai";
import { FaShippingFast } from "react-icons/fa";
import { GiPin } from "react-icons/gi";
import { Chart as CartsJs, BarElement, CategoryScale, LinearScale, Tooltip, Legend } from 'chart.js';
import { Bar } from 'react-chartjs-2';
import ProgressBar from 'react-bootstrap/ProgressBar';

export function Component() {

    const now = 60;

    const barChartData = {
        labels: ['January', 'February', 'March', 'April', 'May'],
        datasets: [
            {
                label: 'Monthly Sales',
                backgroundColor: 'rgba(75,192,192,0.2)',
                borderColor: 'rgba(75,192,192,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(75,192,192,0.4)',
                hoverBorderColor: 'rgba(75,192,192,1)',
                data: [65, 59, 80, 81, 56],
            },
        ],
    };

    CartsJs.register(BarElement, CategoryScale, LinearScale, Tooltip, Legend);

    const barChartOptions = {
        scales: {
            y: {
                beginAtZero: true,
            },
        },
    };

    return (
        <div>
            {/* Dashboard */}
            <div className="d-flex justify-content-between">
                <div>
                    <p className="h5 text-primary"> Dashboard</p>
                    <p className="text-muted pararafDasbord">Periode: <span className="text-dark">Hari ini</span></p>
                </div>
                <div className="d-flex">
                    <div className='contenInput'>
                        <label>
                            <input type="date" id="customDateInput" class="inputDate" />
                            <span class="dateIcon">&#x1F4C5;</span>
                        </label>
                    </div>
                    <button className="btn-file"><AiTwotoneFilePdf className='h4' /></button>
                    <div className='btn-reload'>
                        <Button className='btn-primary fw-bold'><AiOutlineReload className='h5 text-white' /></Button>
                    </div>
                </div>
            </div>
            <div className='mt-3'>
                <div className='row'>
                    <div className='.col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6'>
                        <div className='py-3 px-3 mt-2 border bg-white'>
                            <p className='text-center text-danger h6'>Penjualan</p>
                            <p className='h4 text-center'>0</p>
                            <div className='contenIconKranjangPenjualan'>
                                <AiOutlineShoppingCart className='h3 iconKranjang' />
                            </div>
                            <div className='row mt-3'>
                                <div className='col-6'>
                                    <p className='pargarfIconKranjang'><AiOutlineCheckCircle className='text-danger' /> 0</p>
                                    <p className='text-center text-muted'>Paid</p>
                                </div>
                                <div className='col-6'>
                                    <p className='pargarfIconKranjang'><AiOutlineCheckCircle className='text-danger' /> 0</p>
                                    <p className='text-center text-muted'>Verify</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='.col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6'>
                        <div className='py-3 px-3 mt-2 border bg-white'>
                            <p className='text-center text-primary h6'>Customers</p>
                            <p className='h4 text-center'>0</p>
                            <div className='contenIconKranjangPenjualan'>
                                <AiOutlineTeam className='h3 iconKranjang' />
                            </div>
                            <div className='row mt-3'>
                                <div className='col-6'>
                                    <p className='pargarfIconKranjang'><GiPin className='text-primary' /> 0</p>
                                    <p className='text-center text-muted'>Total</p>
                                </div>
                                <div className='col-6'>
                                    <p className='pargarfIconKranjang'><GiPin className='text-primary' /> 0</p>
                                    <p className='text-center text-muted'>Baru</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='.col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6'>
                        <div className='py-3 px-3 mt-2 border bg-white'>
                            <p className='text-center text-success h6'>Pengguna</p>
                            <p className='h4 text-center'>0</p>
                            <div className='contenIconKranjangPenjualan'>
                                <AiOutlineUser className='h3 iconKranjang' />
                            </div>
                            <div className='row mt-3'>
                                <div className='col-6'>
                                    <p className='pargarfIconKranjang'><AiOutlineUser className='text-success' /> 0</p>
                                    <p className='text-center text-muted'>Total</p>
                                </div>
                                <div className='col-6'>
                                    <p className='pargarfIconKranjang'><AiOutlineClockCircle className='text-success' /> 0</p>
                                    <p className='text-center text-muted'>Verify</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='.col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6'>
                        <div className='py-3 px-3 mt-2 border bg-white'>
                            <p className='text-center text-warning h6'>Return</p>
                            <p className='h4 text-center'>0</p>
                            <div className='contenIconKranjangPenjualan'>
                                <AiOutlineShoppingCart className='h3 iconKranjang' />
                            </div>
                            <div className='row mt-3'>
                                <div className='col-6'>
                                    <p className='pargarfIconKranjang'><FaShippingFast className='text-warning' /> 0</p>
                                    <p className='text-center text-muted'>Terkirim</p>
                                </div>
                                <div className='col-6'>
                                    <p className='pargarfIconKranjang'><AiOutlineClockCircle className='text-warning' /> 0</p>
                                    <p className='text-center text-muted'>Panding</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* selesai Dashborard */}

                {/* Stattistik componen */}
                <div className='mt-5'>
                    <p className='h4'>Statistik Penjualan</p>
                    <Bar data={barChartData} options={barChartOptions} />
                </div>

                <div className='row mt-5'>
                    <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 my-3'>
                        <div className='bg-white px-3 py-3'>
                            <div className='d-flex justify-content-between'>
                                <div>
                                    <p className='h5'>Rank Marketing</p>
                                    <p>Total Rp 0</p>
                                </div>
                                <div>
                                    <Button className="px-0"><AiOutlineReload className='h3' /></Button>
                                    <Button className="px-0"><AiOutlineMore className='h3' /></Button>
                                </div>
                            </div>
                            <div className='d-flex justify-content-center pt-3 border-bottom align-items-center'>
                                <AiOutlineUser className='pe-2 h3' />
                                <div className='w-100'>
                                    <div className='d-flex justify-content-between'>
                                        <p className='textMarketing'>Yono Cahyono</p>
                                        <p>Rp. 10.000</p>
                                    </div>
                                    <ProgressBar now={now} label={`${now}%`} className='w-100' />;
                                </div>
                            </div>
                            <div className='d-flex justify-content-center pt-3 border-bottom align-items-center'>
                                <AiOutlineUser className='pe-2 h3' />
                                <div className='w-100'>
                                    <div className='d-flex justify-content-between'>
                                        <p className='textMarketing'>Yono Cahyono</p>
                                        <p>Rp. 10.000</p>
                                    </div>
                                    <ProgressBar now={now} label={`${now}%`} className='w-100' />;
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 my-3'>
                        <div className='bg-white px-3 py-3'>
                            <div className='d-flex justify-content-between'>
                                <div>
                                    <p className='h5'>Rank Customers</p>
                                    <p>Total Rp 0</p>
                                </div>
                                <div>
                                    <Button className="px-0"><AiOutlineReload className='h3' /></Button>
                                    <Button className="px-0"><AiOutlineMore className='h3' /></Button>
                                </div>
                            </div>
                            <div className='d-flex justify-content-center pt-3 border-bottom align-items-center'>
                                <AiOutlineTeam className='pe-2 h3' />
                                <div className='w-100'>
                                    <div className='d-flex justify-content-between'>
                                        <p className='textMarketing'>Yono Cahyono</p>
                                        <p>Rp. 10.000</p>
                                    </div>
                                    <ProgressBar now={now} label={`${now}%`} className='w-100' />;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='row'>
                    <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 my-3'>
                        <div className='bg-white px-3 py-3'>
                            <div className='d-flex justify-content-between'>
                                <div>
                                    <p className='h5'>Rank Ekspedisi</p>
                                    <p>Total 0</p>
                                </div>
                                <div>
                                    <Button className="px-0"><AiOutlineReload className='h3' /></Button>
                                    <Button className="px-0"><AiOutlineMore className='h3' /></Button>
                                </div>
                            </div>

                            <div className='d-flex justify-content-center pt-3 border-bottom align-items-center'>
                                <FaShippingFast className='pe-2 h3' />
                                <div className='w-100'>
                                    <p className='textExpedisi'>JNE</p>
                                    <ProgressBar now={now} label={`${now}%`} className='w-100' />;
                                </div>
                            </div>

                            <div className='d-flex justify-content-center pt-3 border-bottom align-items-center'>
                                <FaShippingFast className='pe-2 h3' />
                                <div className='w-100'>
                                    <p className='textExpedisi'>JNE</p>
                                    <ProgressBar now={now} label={`${now}%`} className='w-100' />;
                                </div>
                            </div>

                            <div className='d-flex justify-content-center pt-3 border-bottom align-items-center'>
                                <FaShippingFast className='pe-2 h3' />
                                <div className='w-100'>
                                    <p className='textExpedisi'>JNE</p>
                                    <ProgressBar now={now} label={`${now}%`} className='w-100' />;
                                </div>
                            </div>

                            <div className='d-flex justify-content-center pt-3 border-bottom align-items-center'>
                                <FaShippingFast className='pe-2 h3' />
                                <div className='w-100'>
                                    <p className='textExpedisi'>JNE</p>
                                    <ProgressBar now={now} label={`${now}%`} className='w-100' />;
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 my-3'>
                        <div className='bg-white px-3 py-3'>
                            <div className='d-flex justify-content-between'>
                                <div>
                                    <p className='h5'>Rank Destination</p>
                                    <p>Total Rp 0</p>
                                </div>
                                <div>
                                    <Button className="px-0"><AiOutlineReload className='h3' /></Button>
                                    <Button className="px-0"><AiOutlineMore className='h3' /></Button>
                                </div>
                            </div>
                            <div className='py-5'>
                                <p className='text-center'>Data tidak ditemukan</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}