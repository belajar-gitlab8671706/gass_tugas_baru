import './style-pesanan.css'
import { Button, Dropdown } from "@/components";
import { DropdownButton, Form, InputGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
    AiOutlineMedicineBox,
    AiFillFilter,
    AiOutlineSolution,
    AiTwotoneFileZip,
    AiOutlineIdcard,
    AiOutlineCreditCard,
    AiOutlineSetting,
    AiOutlineUser,
    AiOutlineMobile,
    AiOutlineArrowLeft,
    AiOutlineArrowRight,
    AiOutlineSwitcher,
    AiOutlineCloseCircle
} from "react-icons/ai";
import { IoSearchOutline, IoPencil } from "react-icons/io5";
import { IoIosPrint, IoLogoWhatsapp } from "react-icons/io";
import { FaList } from "react-icons/fa";
import { FaShippingFast } from "react-icons/fa";

import { useCounterStore } from '@/store';
import { useState } from 'react';
import FilterCustomerPesananCategory from './komponen/filterCustomerPesananCategory'


export function Component() {
    const {
        toggleBackgroundActive,
        toggleBackgroundExportDefault,
        toggleBackgroundExportImazi,
        toggleBackgroundPesananDistributor,
        toggleBackgroundUpdatePesananDistributor
    } = useCounterStore()
    const [toggleFilterCategory, setToggleFilterCategory] = useState(false)
    const [categoryStyle, setCategoryStyle] = useState({})

    const hendlebtnCategory = () => {
        if (!toggleFilterCategory) {
            setCategoryStyle({ zIndex: 99999991 })
        } else {
            setCategoryStyle({})
        }
        setToggleFilterCategory(!toggleFilterCategory)
        toggleBackgroundActive()
    }

    return (
        <div>
            <p><Link to='/' className="h6 text-primary">Dashboard </Link> /  Pesanan Distributor</p>

            {/* Button Group */}
            <div className="contenInputButton">
                <div className='d-flex'>
                    <div class="dropdown">
                        <button
                            className="btn dropdown-toggle btn-primary btnFile me-3"
                            type="button"
                            data-bs-toggle="dropdown"
                            aria-expanded="false"
                            onClick={hendlebtnCategory}
                            style={categoryStyle}
                        >
                            {
                                toggleFilterCategory ? < AiOutlineCloseCircle className='iconFile' /> : <AiFillFilter className='iconFile' />
                            }
                        </button>
                        {
                            toggleFilterCategory && (
                                <Dropdown style={categoryStyle} className="py-2 contenFilterCategory">
                                    <div className='d-flex w-100 justify-content-between dropdownList'>
                                        <FilterCustomerPesananCategory />
                                    </div>
                                </Dropdown>
                            )
                        }
                    </div>

                    <InputGroup className="mb-3 inputSearch">
                        <Form.Control
                            placeholder="Recipient's username"
                            aria-label="Recipient's username"
                            aria-describedby="basic-addon2"
                        />
                        <Button variant="outline-secondary" className='btn-primary btnFile' id="button-addon2">
                            <IoSearchOutline className='iconFile' />
                        </Button>
                    </InputGroup>
                </div>
                <div className='d-flex contenButton'>
                    <DropdownButton className='btnDropdown' title='Export to Excel'>
                        <Dropdown className="px-2 py-2">
                            <div className='d-flex justify-content-between dropdownList'>
                                <Link onClick={toggleBackgroundExportDefault}>Format Default</Link>
                                <AiOutlineSolution />
                            </div>
                        </Dropdown>
                        <Dropdown className="px-2 py-2">
                            <div className='d-flex justify-content-between dropdownList'>
                                <Link onClick={toggleBackgroundExportImazi}>Format to Imezi</Link>
                                <AiOutlineSolution />
                            </div>
                        </Dropdown>
                    </DropdownButton>
                    <Button onClick={toggleBackgroundPesananDistributor} className="btn-primary btnFile"><AiOutlineMedicineBox className='iconFile' /></Button>
                </div>
            </div>
            {/* Selesai button Group */}

            {/* Penjualan List */}
            <div className='row mt-3 rounded'>
                <div className='col-3 bg-white py-2 border-end'>
                    <div className='d-flex justify-content-between'>
                        <Link className='d-flex'><AiTwotoneFileZip className='me-1' /> <p>RICING</p></Link>
                        <Link><FaList /></Link>
                    </div>
                </div>
                <div className='col-2 bg-white py-2 border-end'>
                    <p className='text-primary'><AiOutlineIdcard className='h6 me-1 d-none d-lg-inline' /> CUSTOMER </p>
                </div>
                <div className='col-2 bg-white py-2 border-end'>
                    <p className='text-primary'><FaShippingFast className='h6 me-1 d-none d-lg-inline' />ONGKIR</p>
                </div>
                <div className='col-4 bg-white py-2 border-end'>
                    <p className='text-primary'><AiOutlineCreditCard className='h6 me-1 d-none d-lg-inline' />PEMBAYARAN</p>
                </div>
                <div className='col-1 bg-white py-2'>
                    <AiOutlineSetting className='h5 d-block mx-auto' />
                </div>
            </div>
            <div className='row border mt-3 rounded'>
                <div className='col-12'>
                    <div className='d-flex justify-content-between py-3 align-items-center'>
                        <div className='d-flex justify-content-center align-items-center'>
                            <p className='bg-danger text-white px-2 textPending me-2'>PENDING</p>
                            <span> <p className='text-dark fw-bold'>#2401116P80V0INV</p></span>
                            <Link className='ms-2'><AiOutlineSwitcher className='text-warning h6' /></Link>
                            <Link className='ms-2'><IoLogoWhatsapp className='text-success h6' /></Link>
                        </div>
                        <div className='d-flex align-items-center'>
                            <span><p className='me-3'><AiOutlineUser className='me-1' /> Baus</p></span>
                            <p><AiOutlineCreditCard className='me-1' /> PAYMENT</p>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-3 bg-white py-2 border-end d-flex align-items-center'>
                            <ul >
                                <li>- x1 bajukoko</li>
                                <li>- x1 Chiki</li>
                            </ul>
                        </div>
                        <div className='col-2 bg-white py-2 border-end d-flex align-items-center'>
                            <p>
                                <span className='fw-bold'>Distributor</span><br />
                                <AiOutlineMobile className='my-0' />
                                08781233223
                            </p>
                        </div>
                        <div className='col-2 bg-white py-2 border-end d-flex align-items-center'>
                            <p>
                                <span className='fw-bold'>Rp0</span><br />
                                Total Ongkir
                            </p>
                        </div>
                        <div className='col-4 bg-white py-2 border-end'>
                            <div className='d-flex justify-content-between border-bottom'>
                                <div>
                                    <p className='my-0'>Sub Total:</p>
                                    <p className='my-0'>Diskon:</p>
                                    <p className='my-0'>Total:</p>
                                </div>
                                <div>
                                    <p className='my-0 text-end text-dark'>Rp110.000</p>
                                    <p className='my-0 text-end text-dark'>-Rp0:</p>
                                    <p className='my-0 text-end text-dark'>Rp110.000</p>
                                </div>
                            </div>
                            <p>Pembayaran: <span className='text-dark'>Transfer</span></p>
                        </div>
                        <div className='col-1 bg-white py-2 d-flex align-items-center'>
                            <div className='mx-auto'>
                                <Button className='btn-primary btnPrin d-block'><IoIosPrint className='iconFile top-0' /></Button>
                                <Button onClick={toggleBackgroundUpdatePesananDistributor} className='btn-warning mt-2 btnPrin d-block border'><IoPencil className='iconTitikHVertikal' /></Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='d-flex justify-content-between align-items-center mt-4'>
                <span><p>Total 4 - Hal 1 dari 1</p></span>
                <div>
                    <Button className='btn-light btnFile border'><AiOutlineArrowLeft className='iconFile top-0' /></Button>
                    <Button className='btn-light ms-2 btnFile border'><AiOutlineArrowRight className='iconFile top-0' /></Button>
                </div>
            </div>
        </div >
    )
}