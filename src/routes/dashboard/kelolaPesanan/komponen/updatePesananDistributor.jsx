import './style-filterCategory.css'
import { Link } from 'react-router-dom'
import { IoMdClose } from "react-icons/io";
import { AiFillTag } from "react-icons/ai";
import { FaCloudUploadAlt } from "react-icons/fa";
import { useCounterStore } from '@/store'
import { LiaClipboardListSolid } from "react-icons/lia";
import { Form } from 'react-bootstrap';
import { MdDeleteForever } from "react-icons/md";
import { Input } from '@/components';
import { useState } from 'react';

const UpdatePesananDistributor = () => {
    const { toggleBackgroundUpdatePesananDistributor } = useCounterStore()
    const [btnBuktiBAyar, setBtnBuktiBayar] = useState(false)

    return (
        <div className="bg-white contenProductDistributor pt-3 rounded position-relative mx-1 position-relative">
            <div className='border-bottom px-3 pb-3'>
                <Link to='/pesanan-distributor' onClick={toggleBackgroundUpdatePesananDistributor} className='position-absolute end-0 me-3'>
                    <IoMdClose className='h4' />
                </Link>
                <p className='text-center fw-bold mb-0'>Update Pesanan Distributor</p>
            </div>
            <div className='py-4 px-3 d-flex border-bottom'>
                <Form.Select disabled className="selectUpdate">
                    <option value="option1">Nothing Selected </option>
                </Form.Select>
            </div>
            <div className='pt-4 px-3'>
                <div className='d-flex justify-content-between'>
                    <p className='fw-bold text-dark'><AiFillTag /> Product</p>
                    <div className='d-flex'>
                        <p className='me-1'>DISKON (Rp):</p>
                        <input className=' mx-0 text-end containerDiskon border-0' placeholder='0' />
                    </div>
                </div>
            </div>
            <div className='contenDetelProduct border-bottom'>
                <div className='d-flex '>
                    <div className='contenID d-flex align-items-center justify-content-center border-end border-secondary'>
                        <p className='text-center'>ID</p>
                    </div>
                    <div className='productID  d-flex align-items-center border-end border-secondary'>
                        <p className='ms-2'>Produk</p>
                    </div>
                    <div className='d-flex align-items-center justify-content-center qtyID border-end border-secondary'>
                        <p className='text-center'>Qty</p>
                    </div>
                    <div className='d-flex align-items-center justify-content-center border-end border-secondary qtyID'>
                        <p className='text-center'>Harga Penerima</p>
                    </div>
                    <div className='d-flex align-items-center justify-content-center border-end border-secondary alamatID'>
                        <p className='text-center'>Alamat</p>
                    </div>
                    <div className='d-flex align-items-center justify-content-center border-end border-secondary alamatID'>
                        <p className='text-center'>Kurir</p>
                    </div>
                    <div className='d-flex align-items-center justify-content-center border-end border-secondary qtyID'>
                        <p className='text-center'>Ongkir</p>
                    </div>
                    <div className='d-flex align-items-center justify-content-center border-end border-secondary qtyID'>
                        <p className='text-center'>Sub Total</p>
                    </div>
                    <div className='d-flex align-items-center justify-content-center qtyID'>
                        <p className='text-center'>Total Bayar</p>
                    </div>
                    <div className='contenID d-flex align-items-center btnSampah justify-content-center border-end border-secondary px-3 border-start'></div>
                </div>
                <div className='d-flex'>
                    <div className='d-flex align-items-center justify-content-center dataID border'>
                        <p className='text-center'>ID</p>
                    </div>
                    <div className='d-flex align-items-center dataProduct border'>
                        <input className='w-100 border-0' />
                    </div>
                    <div className='d-flex align-items-center justify-content-center dataQty border'>
                        <input className='w-100 border-0' />
                    </div>
                    <div className='d-flex align-items-center justify-content-center dataQty border'>
                        <input className='w-100 border-0' />
                    </div>
                    <div className='d-flex align-items-center justify-content-center dataAlamat border'>
                        <input className='w-100 border-0' />
                    </div>
                    <div className='d-flex align-items-center justify-content-center dataAlamat border'>
                        <input className='w-100 border-0' />
                    </div>
                    <div className='d-flex align-items-center justify-content-center dataQty border'>
                        <input className='w-100 border-0' />
                    </div>
                    <div className='d-flex align-items-center justify-content-center dataQty border'>
                        <p>110.001</p>
                    </div>
                    <div className='d-flex align-items-center justify-content-center dataQty border'>
                        <p>110.001</p>
                    </div>
                    <div className=' d-flex align-items-center btnSampah justify-content-center border'>
                        <button className='border-0 bg-white w-100'><MdDeleteForever /></button>
                    </div>
                </div>
            </div>
            <div>
                <div className='px-3 pt-3 d-flex justify-content-between border-bottom'>
                    <p className='fw-bold text-black'><LiaClipboardListSolid /> TAGIHAN</p>
                    <div className='d-flex'>
                        <p>JATUH TEMPO</p>
                        <input type='date' className='inputDateComponenDistributor ms-1 me-2 border-0' />
                        <p className='ps-2 border-start my-0'>NOMOR INVOICE: <span className='fw-bold text-dark'>002/02/06/2022/D-INV</span></p>
                    </div>
                </div>
                <biv className='d-flex px-3 py-3 justify-content-between'>
                    <button onClick={() => setBtnBuktiBayar(!btnBuktiBAyar)} className='border-0 bg-primary text-white rounded'>Bukti Bayar</button>
                    <div className='d-flex'>
                        <div class="form-check me-2">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                            <label class="form-check-label" for="flexCheckDefault">
                                Pembayaran Tenor
                            </label>
                        </div>
                        <span className='px-2 border-end border-start'>Terhutang <p className='fw-bold d-inline text-dark'>Rp0</p></span>
                        <span className='ms-2'>Total Tagihan <p className='fw-bold d-inline text-dark'>Rp0</p></span>
                    </div>
                </biv>
            </div>
            <div className='d-flex'>
                <div className='col-1 border-end colorTabelBayar'>
                    <p className='text-center'>No</p>
                </div>
                <div className='col-3 border-end colorTabelBayar'>
                    <p className='text-center'>Nominal</p>
                </div>
                <div className='col-2 border-end colorTabelBayar'>
                    <p className='text-center'>	Upload</p>
                </div>
                <div className='col-2 border-end colorTabelBayar'>
                    <p className='text-center'>Bukti Bayar</p>
                </div>
                <div className='col-4 colorTabelBayar'>
                    <p className='text-center'>Status</p>
                </div>
            </div>
            <div className='d-flex border-bottom'>
                <div className='col-1 border-end '>
                    <p className='text-center'>No</p>
                </div>
                <div className='col-3 border-end '>
                    <p className='text-center'>Nominal</p>
                </div>
                <div className='col-2 border-end '>
                    <p className='text-center'>	Upload</p>
                </div>
                <div className='col-2 border-end'>
                    <p className='text-center'>Bukti Bayar</p>
                </div>
                <div className='col-4 '>
                    <p className='text-center'>Status</p>
                </div>
            </div>
            <div className='px-3 py-3 d-flex justify-content-end colorTabelBayar'>
                <button className='border-0 px-4 py-1 rounded bg-primary text-white'>UPDATE</button>
            </div>
            {
                btnBuktiBAyar && (
                    <div className='buktiBayarUpdatePesananDistributor'>
                        <div className='mt-5 mx-auto contenBayarPesananDistributor bg-white border rounded'>
                            <div className='py-3 px-3 d-flex justify-content-between'>
                                <p className='fe-bold h6'>Bukti Bayar</p>
                                <IoMdClose className='h4' onClick={() => setBtnBuktiBayar(!btnBuktiBAyar)} />
                            </div>
                            <div className='px-3'>
                                <label className='w-100'>
                                    NOMINAL
                                    <Input />
                                </label>
                                <label className='w-100 mt-3'>
                                    BUKTI BAYAR
                                    <Input type="file" placeholder='pilih file di sini' />
                                </label>
                                <button className='border-0 text-white text-center w-100 rounded bg-primary my-3 py-1'>UPLOAD <FaCloudUploadAlt className='h6 text-white' /></button>
                            </div>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default UpdatePesananDistributor