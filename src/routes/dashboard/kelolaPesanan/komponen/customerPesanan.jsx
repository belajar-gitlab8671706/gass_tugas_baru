import { Link } from "react-router-dom";
import { IoMdClose } from "react-icons/io";
import { AiOutlineLeft, AiOutlineUserAdd, AiOutlineUser, AiOutlineRightCircle } from "react-icons/ai";
import { CiSearch } from "react-icons/ci";
import { useCounterStore } from '@/store';
import { useState } from "react";
import TambahCustomer from "../../customer/tambahCustomer";

const CustomerPesanan = () => {
    const { toggelPemesananCustomer, toggleBackgroundActiveProduct, toggleKranjangPesanan, toggleBackgroundTambahCustomer, backgroundTambahCustomer } = useCounterStore()

    const btnProduc = () => {
        toggelPemesananCustomer()
        toggleBackgroundActiveProduct()
    }

    const btnKranjang = () => {
        toggelPemesananCustomer()
        toggleKranjangPesanan()
    }

    return (
        <div className="bg-white contenProduct position-relative rounded">
            <div className='d-flex justify-content-between border-bottom px-3 py-3'>
                <Link to='/pesanan' onClick={() => btnProduc()} >
                    <AiOutlineLeft />   Product
                </Link>
                <p className='text-center fw-bold mb-0'>Customer</p>
                <Link to='/pesanan' >
                    <IoMdClose className='h4' onClick={toggelPemesananCustomer} />
                </Link>
            </div>
            <div className="d-flex justify-content-between px-3 py-3">
                <div className="d-flex w-50">
                    <input type="text" className="form-control" placeholder="Cari..." aria-label="Recipient's username" aria-describedby="button-addon2" />
                    <button className="btn btn-primary ms-2 btnFile" type="button"><CiSearch className='iconFile' /></button>
                </div>
                <button onClick={toggleBackgroundTambahCustomer} class="btn btn-warning ms-2 btnFile" type="button"><AiOutlineUserAdd className='iconFile' /></button>
            </div>
            <div className="row px-3 cartUserCustomer">
                <div className="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 my-3">
                    <div className="border containerUserCustomer bg-light py-2 px-3 rounded d-flex align-items-center">
                        <div className="contenIconCustomerUser bg-secondary me-2">
                            <AiOutlineUser className="h3 text-white" />
                        </div>
                        <div>
                            <p className="fw-bold my-0">A. Mambaus Sholihin</p>
                            <p>08636363636</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="px-3 border-top py-3 d-flex justify-content-end bg-light">
                <button onClick={() => btnKranjang()} className="bg-primary py-1 text-white text-center border-0 px-2 rounded">Keranjang <AiOutlineRightCircle /></button>
            </div>
            {
                backgroundTambahCustomer && (
                    <div className="buktiBayarUpdatePesananDistributor d-flex justify-content-center">
                        <div className="bg-white">
                            <TambahCustomer />
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default CustomerPesanan