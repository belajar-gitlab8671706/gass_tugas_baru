import './style-filterCategory.css'
import { Button, Input } from "@/components"
import { CiSearch, CiImageOn } from "react-icons/ci";
import { IoMdClose, IoIosChatbubbles } from "react-icons/io";
import {
    AiOutlineEllipsis,
    AiOutlineCheckCircle,
    AiFillPlusCircle,
    AiOutlineDislike,
    AiOutlineEyeInvisible,
    AiOutlineLike,
    AiOutlineSetting,
    AiOutlineRightCircle
} from "react-icons/ai";
import { LuImagePlus } from "react-icons/lu";
import { RiBook3Fill } from "react-icons/ri";
import { Link } from 'react-router-dom';

// state
import { useCounterStore } from '../../../../store'
import { useState } from 'react';

const Product = () => {
    const { toggleBackgroundActiveProduct, toggelPemesananCustomer } = useCounterStore()
    const [toggleDropdownColection, setToggleDropdownColection] = useState(false)
    const [toggleSeting, setToggleSeting] = useState(false)

    const btnCustomer = () => {
        toggleBackgroundActiveProduct()
        toggelPemesananCustomer()
    }

    return (
        <div className="bg-white contenProduct position-relative">
            <div className='border-bottom px-3 py-3'>
                <Link to='/pesanan' onClick={toggleBackgroundActiveProduct} className='position-absolute end-0 me-3'>
                    <IoMdClose className='h4' />
                </Link>

                <p className='text-center fw-bold mb-0'>Produk</p>
            </div>
            <div className='d-flex px-3 py-3'>
                <Input className='me-3' />
                <Button className='btn-primary position-relative btnSearch'>
                    <CiSearch className='position-absolute h1 text-white top-0 end-0' />
                </Button>
            </div>
            <div className='row my-3 px-3'>
                <div className='.col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 mb-2 '>
                    <div className='border w-100 px-2 px-2 containerProduct'>
                        <div className='position-relative'>
                            <div className='position-absolute contenIconImage bg-light rounded-circle end-0 mt-2'>
                                <CiImageOn className='text-primary h5' />
                            </div>
                            <div onClick={() => {
                                setToggleDropdownColection(!toggleDropdownColection)
                                setToggleSeting(false)
                            }}
                                className='position-absolute contenIconImage2 bg-light rounded-circle end-0 mt-2'>
                                <RiBook3Fill className='text-primary h5' />
                            </div>
                            {
                                toggleDropdownColection && (
                                    <div className='position-absolute bg-white mt-2'>
                                        <div className='border colectionsProduct pb-2 pt-2'>
                                            <p className='text-center'>Saved to <Link className='text-primary fw-bold'>Colections</Link></p>
                                            <div className='d-flex align-items-center pb-1 border-bottom w-100 px-2'>
                                                <div className='bg-light btnFile rounded '>
                                                    <AiOutlineCheckCircle className='text-primary iconFile top-0' />
                                                </div>
                                                <p className='ms-2'>Saved Images</p>
                                            </div>
                                            <div className='py-2 px-2 d-flex justify-content-center'>
                                                <Link className='text-center'><AiFillPlusCircle className='h5' /> Create a new Colection</Link>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }
                            <div onClick={() => {
                                setToggleSeting(!toggleSeting)
                                setToggleDropdownColection(false)
                            }}
                                className='position-absolute contenIconImage bg-light rounded-circle top-50 end-0 mt-2'>
                                <AiOutlineEllipsis className='text-primary h5' />
                            </div>
                            {
                                toggleSeting && (
                                    <div className='position-absolute bg-white mt-2 contenSettingProduct'>
                                        <div className='border py-3 px-2'>
                                            <Link className='text-primary d-block'><LuImagePlus /> Edit image</Link>
                                            <Link className='text-primary d-block my-2'><AiOutlineEyeInvisible /> Hide for this site</Link>
                                            <Link className='text-primary d-block'><AiOutlineEyeInvisible /> Hide always</Link>
                                            <Link className='text-primary d-block my-2'>< AiOutlineSetting /> Setings</Link>
                                            <div className='d-flex justify-content-between align-items-center'>
                                                <span><IoIosChatbubbles /> Do you like the hover menu?</span>
                                                <div className='d-flex'>
                                                    <AiOutlineLike className='me-1' />
                                                    <AiOutlineDislike />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }
                            <img src="https://th.bing.com/th/id/OIP.B1tShhs5oZGyxng58jyENQHaHa?rs=1&pid=ImgDetMain" />
                        </div>
                        <div className='mt-2 mb-2'>
                            <h6>Baju koko</h6>
                            <p className='text-danger text-end'>Rp 100.000</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='d-flex justify-content-between align-items-center bg-light px-3 py-3'>
                <span><p className='h6 text-dark d-inline'>0</p> Produk dipilih</span>
                <Button onClick={() => btnCustomer()} className='btnCustomer'>Pilih Customer <AiOutlineRightCircle className='ms-1' /></Button>
            </div>
        </div>
    )
}

export default Product