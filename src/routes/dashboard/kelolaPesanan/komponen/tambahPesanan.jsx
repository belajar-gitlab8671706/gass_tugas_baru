import { AiOutlineDown } from "react-icons/ai";
import { useCounterStore } from '@/store'
import { Input } from "@/components";

const TambahPesanan = () => {
    const { toggleBtnCheckout } = useCounterStore()

    return (
        <div className="w-100 h-100 backgroundChekoutPesanan">
            <div className="bg-white mx-auto containerCheckoutPesanan mt-5">
                <div className="d-flex align-items-center justify-content-between bg-light py-3 px-3">
                    <span><p className="fw-bold text-dark">Product</p></span>
                    <AiOutlineDown onClick={toggleBtnCheckout} className="h4" />
                </div>
                <div className="px-3 py-2 border-bottom">
                    <Input placeholder="Cari..." />
                </div>
                <div className="mx-3 py-2">
                    <label className="align-items-center d-flex">
                        <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
                        <img className="imageProduct border mx-2 rounded" alt="image Product" src="https://tshirtbar.id/wp-content/uploads/2021/08/OVERSIZE-TSHIRT-SALEM-323x500.png" />
                        <p>Baju Kokoh <br /> Rp.170.000</p>
                    </label>
                </div>
                <div className="border-top px-3 py-2 d-flex justify-content-end">
                    <button className="bg-primary py-1 rounded border-0 text-white px-3">SIMPAN</button>
                </div>
            </div>
        </div>
    )
}

export default TambahPesanan