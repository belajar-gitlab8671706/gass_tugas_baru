import { Button, Input, Select } from '@/components'
import './style-filterCategory.css'


const FilterCategory = () => {
    return (
        <div className="bg-white conten">
            <p className='mx-4 mt-2 mb-0'>Filter Kategori</p>
            <hr className='text-primary' />
            <div className='mx-4'>
                <label className='w-100'>
                    CUSTOMER
                    <Select
                        placeholder="PT. Ripit"
                    >

                    </Select>
                </label>
                <label className='w-100 my-3'>
                    USER
                    <Select
                        placeholder="All.."
                    >

                    </Select>
                </label>
                <label className='w-100'>
                    STATUS
                    <Select
                        placeholder="All.."
                    >

                    </Select>
                </label>
                <label className='my-3'>
                    TANGGAL
                    <div className='d-flex'>
                        <Input />
                        <Input />
                    </div>
                </label>
                <button className='w-100 text-center btn-primary py-1 mb-3 rounded border-0 text-white'>SIMPAN</button>
            </div>
        </div >
    )
}

export default FilterCategory