import { IoMdClose, } from "react-icons/io";
import { AiOutlineUser, AiOutlineCheckCircle, AiOutlineSwitcher, AiOutlineUp, AiOutlineDown } from "react-icons/ai";
import { FaWhatsappSquare } from "react-icons/fa";
import { Link } from "react-router-dom";
import { useCounterStore } from '@/store'
import { useState } from "react";

const StatusOrder = () => {
    const { toggleBackgroundStatusOrder } = useCounterStore()
    const [btnDetailInfo, setBtnDetailInfo] = useState(false)

    return (
        <div className="bg-white containerStatusOrder position-relative rounded">
            <div className='border px-3 py-3'>
                <Link to='/pesanan' onClick={toggleBackgroundStatusOrder} className='position-absolute end-0 me-3'>
                    <IoMdClose className='h4' />
                </Link>
                <p className='text-center fw-bold mb-0'>Produk</p>
            </div>
            <div className="d-flex bg-light align-items-center border-bottom-1 py-3 px-4">
                <div className="contenStatusOrderName bg-secondary">
                    <AiOutlineUser className="h3 text-white position-absolute" />
                </div>
                <div className="ms-2">
                    <p className="fw-bold my-0">yono</p>
                    <p>08781233223</p>
                </div>
            </div>
            <div className="contenComponenBerhasil">
                <div className="border-bottom  border-2 mx-4">
                    <div className="containerBerhasil my-3 py-2 rounded">
                        <AiOutlineCheckCircle className="h1 mx-auto d-block text-primary" />
                        <p className="text-center textBerhasil text-primary">Berhasil</p>
                    </div>
                    <div className="border rounded py-2 px-3 d-flex justify-content-between align-items-center">
                        <div>
                            <p className="my-0">INVOICE</p>
                            <p className="fw-bold my-0">#230206E00M5XINV</p>
                        </div>
                        <div className="d-flex">
                            <FaWhatsappSquare className="text-success h3" />
                            <AiOutlineSwitcher className="text-warning ms-1 h3" />
                        </div>
                    </div>
                    <div className="my-3 border rounded py-2 px-3 d-flex justify-content-between align-items-center">
                        <div>
                            <p className="my-0">INVOICE</p>
                            <p className="fw-bold my-0 text-danger">Rp350.000</p>
                        </div>
                        <Link to='/pesanan' onClick={() => setBtnDetailInfo(!btnDetailInfo)} className="text-warning">Detail  <AiOutlineUp className="h6 text-warning" /></Link>
                    </div>
                </div>
                <div className="border rounded mt-3 mb-4 py-2 px-3 mx-4">
                    <h6 className="text-dark mb-2 ">Metode Pembayaran</h6>
                    <p className="fw-bold my-0">Transfer Bank MANDIRI</p>
                    <p className="my-0">produsen - 321321</p>
                </div>
            </div>
            {
                btnDetailInfo && (
                    <div className="position-absolute top-0 contenDetalPembayaran bg-light">
                        <div className="border-bottom px-3 py-3">
                            <Link to='/pesanan' onClick={() => setBtnDetailInfo(!btnDetailInfo)} className='position-absolute end-0 me-3'>
                                < AiOutlineDown className='h4' />
                            </Link>
                            <p className='fw-bold mb-0 text-dark'>Detail Pembayaran</p>
                        </div>
                        <div className="contenMetodePembayaranDetail">
                            <div className="d-flex justify-content-between align-items-center py-3 px-3 bg-white">
                                <p>Metode Pembayaran</p>
                                <p className="fw-bold text-dark">Transfer Bank MANDIRI</p>
                            </div>
                            <div className="py-3 px-3 my-2 bg-white">
                                <div className="d-flex justify-content-between align-items-center">
                                    <div>
                                        <p>Total Harga</p>
                                        <p>Total Ongkir</p>
                                    </div>
                                    <div>
                                        <p className="text-end">Rp340.000</p>
                                        <p className="text-end">Rp10.000</p>
                                    </div>
                                </div>
                                <div className="d-flex justify-content-between align-items-center mt-2 border-bottom">
                                    <p><span className="fw-bold">Potongan</span> <br />Promo</p>
                                    <p className="text-end">(Rp0)</p>
                                </div>
                                <div className="d-flex justify-content-between align-items-center mt-2">
                                    <p className="fw-bold text-dark">Total Bayar</p>
                                    <p className="text-end text-danger fw-bold">Rp350.000</p>
                                </div>
                            </div>
                            <div className="pt-3 pb-5 px-3 bg-white">
                                <p className="fw-bold">Barang</p>
                                <div className="d-flex justify-content-between align-items-center mt-2 border-bottom">
                                    <p><span className="fw-bold">Chiki</span> <br />1x Rp170.000</p>
                                    <p className="text-end">Rp170.000</p>
                                </div>
                                <div className="d-flex justify-content-between align-items-center mt-2 border-bottom">
                                    <p><span className="fw-bold">bajukoko</span> <br />1x Rp170.000</p>
                                    <p className="text-end">Rp170.000</p>
                                </div>
                                <div className="mt-2">
                                    <p className="text-dark fw-bold my-0">Kurir</p>
                                    <p>JNE (CTC)</p>
                                    <p className="text-dark fw-bold mb-0">Alamat Pengiriman</p>
                                    <p>jalan sukun rt 08, Kec. cipayung, Kota. Jakarta Timur, DKI Jakarta,12312</p>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default StatusOrder