import { Input } from "@/components";
import { Link, useLocation } from "react-router-dom";
import { IoMdClose } from "react-icons/io";
import { Form } from "react-bootstrap";

// state
import { useCounterStore } from '../../../../store'
const ExportFormatDefault = ({ formatText }) => {
    const { toggleBackgroundExportDefault, toggleBackgroundExportImazi } = useCounterStore()
    const location = useLocation()

    const isDistributorPage = location.pathname.includes('/pesanan-distributor')
    const toValue = isDistributorPage ? '/pesanan-distributor' : '/pesanan';

    const btnCloss = () => {
        if (formatText === 'Format Default') {
            toggleBackgroundExportDefault()
        } else {
            toggleBackgroundExportImazi()
        }
    }

    return (
        <div className="containerformatDefault bg-white rounded mx-1">
            <div className="py-3 px-3 d-flex justify-content-between">
                <div>
                    <h6 className="fw-bold my-0">EXPORT</h6>
                    <p>{formatText}</p>
                </div>
                <Link to={toValue} onClick={btnCloss}><IoMdClose className="h4" /></Link>
            </div>
            <div className="pb-3 px-3">
                <label className="w-100">
                    TANGGAL:
                    <div className="d-flex">
                        <Input />
                        <Input />
                    </div>
                </label>
                <label className="w-100 my-4">
                    STATUS:
                    <Form.Select aria-label="Default select example w-100">
                        <option >Pilih Status</option>
                        <option value="1">Menggunakan Pembayaran</option>
                        <option value="2">Menggunakan Verifikasi</option>
                        <option value="3">Order Berhasil</option>
                    </Form.Select>
                </label>
                <button className="w-100 btn-primary py-1 border-0 rounded fw-bold text-white">EXPORT</button>
            </div>
        </div>
    )
}

export default ExportFormatDefault