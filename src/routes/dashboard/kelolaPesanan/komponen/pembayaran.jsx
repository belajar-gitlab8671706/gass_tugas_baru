import { Link } from "react-router-dom";
import { AiOutlineLeft, AiOutlineUser, AiOutlineSwitcher, AiOutlineDown, AiOutlineCreditCard, AiOutlineExclamationCircle } from "react-icons/ai";
import { IoMdClose } from "react-icons/io";
import { FaWhatsapp } from "react-icons/fa";
import { useCounterStore } from '@/store';
import { Input, Select } from "@/components";

const Pembayaran = () => {
    const { toggleCheckoutPesanan, toggleBtnPembayaran } = useCounterStore()

    const btnCheckout = () => {
        toggleCheckoutPesanan()
        toggleBtnPembayaran()
    }

    return (
        <div className='bg-white contenProduct position-relative rounded'>
            <div className='d-flex justify-content-between border-bottom px-3 py-3'>
                <Link to='/pesanan' onClick={btnCheckout}>
                    <AiOutlineLeft /> Checkout
                </Link>
                <p className='text-center fw-bold mb-0'>Pembayaran</p>
                <Link to='/pesanan' onClick={toggleBtnPembayaran} >
                    <IoMdClose className='h4' />
                </Link>
            </div>
            <div className="d-flex justify-content-between bg-light align-items-center border-bottom">
                <div className="py-2 px-3 d-flex align-items-center">
                    <div className="contenIconCustomerUser bg-secondary me-2">
                        <AiOutlineUser className="h3 text-white" />
                    </div>
                    <div>
                        <p className="fw-bold my-0">A. Mambaus Sholihin</p>
                        <p>08636363636</p>
                    </div>
                </div>
            </div>
            <div className='contenChekoutScroll'>
                <div className="row justify-content-center px-3 pb-2">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mt-2">
                        <div className="px-2 py-3 border rounded d-flex justify-content-between">
                            <div>
                                <p className="textPembayaranInvoice">INVOICE</p>
                                <p className="my-0 text-dark">#24030604L05EINV</p>
                            </div>
                            <div className="d-flex">
                                <FaWhatsapp className="text-success h5 me-1" />
                                <AiOutlineSwitcher className="text-warning h4" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mt-2">
                        <div className="px-2 py-3 border rounded d-flex justify-content-between">
                            <div>
                                <p className="textPembayaranInvoice">TOTAL BAYAR</p>
                                <p className="my-0 text-danger fw-bold">Rp1.359.721</p>
                            </div>
                            <button className="text-warning border-0 bg-white">Detail <AiOutlineDown /></button>
                        </div>
                    </div>
                </div>
                <div className="d-flex align-items-center py-2 mx-3 border-top border-bottom">
                    <div className="w-50">
                        <AiOutlineCreditCard className="h5" />
                        <h6 className="my-0 d-inline">Metode Pembayaran</h6>
                    </div>
                    <div className='d-flex align-items-center py-2 px-3 ms-2 w-100'>
                        <img className='imageBank' src='https://cdn0-production-images-kly.akamaized.net/ClOSWKZI_eb6xIDHlG8tIQqaf2Y=/800x450/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/4255282/original/094712800_1670569941-mandiri.jpg' alt='image bank' />
                        <div className='ms-2'>
                            <p className='fw-bold my-0'>Tranfer MANDIRI</p>
                            <p className='my-0'>produsen - 321321</p>
                        </div>
                    </div>
                </div>
                <div className="d-flex align-items-center py-2 mx-3 border-top border-bottom">
                    <div className="w-50">
                        <AiOutlineExclamationCircle className="h5" />
                        <h6 className="my-0 d-inline">Konfirmasi Pembayaran</h6>
                    </div>
                    <div className="w-100">
                        <input className="border w-100 text-center rounded" type="image" src='https://t4.ftcdn.net/jpg/01/07/57/91/360_F_107579101_QVlTG43Fwg9Q6ggwF436MPIBTVpaKKtb.jpg' alt="Submit" height="150" />
                        <label className="w-100 mt-1">
                            Transfer dari Bank
                            <Select>

                            </Select>
                        </label>
                        <label className="w-100 my-2">
                            Nama Pemilik Rekening
                            <Input />
                        </label>
                        <label className="w-100">
                            Tanggal Transfer
                            <Input type='date' />
                        </label>
                        <label className="w-100 mt-2 mb-4">
                            <div className="d-flex justify-content-between">
                                <p className="my-0">Nominal Bayar</p>
                                <p className="text-primary my-0">Rp1.359.721</p>
                            </div>
                            <Input />
                        </label>
                    </div>
                </div>
            </div>
            <div className='px-3 py-3 d-flex justify-content-end bg-light'>
                <button className='bg-danger text-white py-2 px-3 border-0 rounded'>BAYAR SEKARANG</button>
            </div>
        </div>
    )
}

export default Pembayaran