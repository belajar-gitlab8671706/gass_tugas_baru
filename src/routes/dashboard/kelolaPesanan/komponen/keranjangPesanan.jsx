import { Link } from "react-router-dom";
import { IoMdClose } from "react-icons/io";
import { AiOutlineLeft, AiOutlineUser, AiOutlineRightCircle, AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import { MdDelete } from "react-icons/md";
import { BsBagPlus } from "react-icons/bs";
import { useCounterStore } from '@/store';
import TambahPesanan from "./tambahPesanan";

const KeranjangPesanan = () => {
    const { toggelPemesananCustomer, toggleKranjangPesanan, btnCheckout, toggleBtnCheckout, toggleCheckoutPesanan, } = useCounterStore()

    const btnCustomer = () => {
        toggelPemesananCustomer()
        toggleKranjangPesanan()
    }

    const btnCheckoutPesanan = () => {
        toggleKranjangPesanan()
        toggleCheckoutPesanan()
    }

    return (
        <div className="bg-white contenProduct position-relative rounded">
            <div className='d-flex justify-content-between border-bottom px-3 py-3'>
                <Link to='/pesanan' onClick={() => btnCustomer()} >
                    <AiOutlineLeft />  Customer
                </Link>
                <p className='text-center fw-bold mb-0'>Keranjang</p>
                <Link to='/pesanan' >
                    <IoMdClose className='h4' onClick={toggleKranjangPesanan} />
                </Link>
            </div>
            <div className="d-flex justify-content-between bg-light align-items-center border-bottom">
                <div className="py-2 px-3 d-flex align-items-center">
                    <div className="contenIconCustomerUser bg-secondary me-2">
                        <AiOutlineUser className="h3 text-white" />
                    </div>
                    <div>
                        <p className="fw-bold my-0">A. Mambaus Sholihin</p>
                        <p>08636363636</p>
                    </div>
                </div>
                <button onClick={toggleBtnCheckout} className="btnFile me-3 bg-warning border-0 rounded text-white"><BsBagPlus className="iconFile top-0" /></button>
            </div>
            <div className="py-4 px-4">
                <div className="d-flex align-items-center justify-content-between">
                    <div className="w-50">
                        <img className="imageProduct border py-1 px-1 rounded" alt="image Product" src="https://tshirtbar.id/wp-content/uploads/2021/08/OVERSIZE-TSHIRT-SALEM-323x500.png" />
                        <div className="ms-2">
                            <p className="my-0">Chiki</p>
                            <span><p className="fw-bold my-0 d-inline">Rp170.000</p> (320 gram)</span>
                        </div>
                    </div>
                    <div className="d-flex">
                        <button className="bg-white btnQtyPesanan border-1 border-ligth"><MdDelete className="iconFile text-dark top-0 end-0" /></button>
                        <button className="bg-white btnQtyPesanan border-1 border-primary ms-2"><AiOutlineMinus className="iconFile text-primary top-0 end-0" /></button>
                        <input type="text" className="form-control inputQtyPesanan mx-2 text-center" placeholder="0" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" />
                        <button className="bg-white btnQtyPesanan border-1 border-primary"><AiOutlinePlus className="iconFile text-primary top-0 end-0" /></button>
                    </div>
                </div>
            </div>
            <div className="px-3 border-top py-3 d-flex justify-content-end bg-light">
                <button onClick={() => btnCheckoutPesanan()} className="bg-primary py-1 text-white text-center border-0 px-2 rounded">Chekout <AiOutlineRightCircle /></button>
            </div>
            {
                btnCheckout && (
                    <div className="w-100 h-100">
                        <TambahPesanan />
                    </div>
                )
            }
        </div>
    )
}

export default KeranjangPesanan