import { Select, Input } from "@/components"
import { Form } from "react-bootstrap"

const FilterCustomerPesananCategory = () => {
    return (
        <div className="bg-white conten">
            <div className="py-2 px-3 border-bottom">
                <p>Filter Kategori</p>
            </div>
            <div className="mx-4">
                <label className='w-100'>
                    CUSTOMER
                    <Select
                        placeholder="All.."
                    >

                    </Select>
                </label>
                <label className='w-100 my-3'>
                    STATUS
                    <Form.Select aria-label="Default select example w-100">
                        <option >All...</option>
                        <option value="1">Lunas</option>
                        <option value="2">Verify</option>
                        <option value="3">Pending</option>
                        <option value="3">Cancel</option>
                    </Form.Select>
                </label>
                <label className='w-100 mb-3'>
                    TANGGAL
                    <div className='d-flex'>
                        <Input />
                        <Input />
                    </div>
                </label>
                <button className="btn-primary w-100 border-0 text-white py-1 mb-3 rounded">SIMPAN</button>
            </div>
        </div>
    )
}

export default FilterCustomerPesananCategory