import { useCounterStore } from '@/store';
import { Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AiOutlineLeft, AiOutlineUser, AiOutlineCreditCard, AiOutlineCheck, AiOutlineDown, AiOutlineRight } from "react-icons/ai";
import { IoMdClose } from "react-icons/io";
import { IoLocation } from "react-icons/io5";
import { FaShippingFast, FaClipboardList } from "react-icons/fa";
import { CiDiscount1 } from "react-icons/ci";
import { CgNotes } from "react-icons/cg";
import { CiBoxList } from "react-icons/ci";
import LocationPengiriman from './componen-checkout/locationPengiriman';
import AlamatPengiriman from './componen-checkout/alamatPengiriman';
import TambahAlamat from './componen-checkout/tambahAlamat';
import UbahAlamat from './componen-checkout/ubahALamat';
import MetodePembayaran from './componen-checkout/metodePembayaran';
import Promo from './componen-checkout/promo';

const ChekoutPesanan = () => {
    const {
        toggleCheckoutPesanan,
        toggleKranjangPesanan,
        componenCheckoutlocation,
        toggleComponenCheckoutLocation,
        alamatPengiriman,
        toggleAlamatPengiriman,
        tambahAlamat,
        ubahAlamat,
        metodePembayaran,
        toggleMetodePembayaran,
        btnPromo,
        toggleBtnPromo,
        toggleBtnPembayaran
    } = useCounterStore()

    const btnPesanan = () => {
        toggleCheckoutPesanan()
        toggleKranjangPesanan()

    }

    const btnPembayaran = () => {
        toggleBtnPembayaran()
        toggleCheckoutPesanan()
    }

    return (
        <div className='bg-white contenProduct position-relative rounded'>
            <div className='d-flex justify-content-between border-bottom px-3 py-3'>
                <Link to='/pesanan' onClick={() => btnPesanan()} >
                    <AiOutlineLeft />  Keranjang
                </Link>
                <p className='text-center fw-bold mb-0'>Checkout</p>
                <Link to='/pesanan' >
                    <IoMdClose className='h4' onClick={toggleCheckoutPesanan} />
                </Link>
            </div>
            <div className="d-flex justify-content-between bg-light align-items-center border-bottom">
                <div className="py-2 px-3 d-flex align-items-center">
                    <div className="contenIconCustomerUser bg-secondary me-2">
                        <AiOutlineUser className="h3 text-white" />
                    </div>
                    <div>
                        <p className="fw-bold my-0">A. Mambaus Sholihin</p>
                        <p>08636363636</p>
                    </div>
                </div>
            </div>
            <div className='contenChekoutScroll'>
                <div className='px-3 py-2'>
                    <div>
                        <div className='d-md-flex align-items-center'>
                            <span className='w-50'><IoLocation className='h4 text-dark' /> <p className='fw-bold d-inline text-dark'>Tujuan Pengiriman</p></span>
                            <div className='ps-md-3 mt-sm-2 border-bottom border-top w-100'>
                                <p>
                                    <span className='fw-bold text-dark'>Yono</span> (Rumah)<br />
                                    <span className='text-dark'>0887877878</span> jalan sukun rt 08<br />
                                    Kota. Jakarta Timur - Kec. cipayung<br />
                                    DKI Jakarta (12312)
                                </p>
                            </div>
                        </div>
                        <div className='py-2 d-flex justify-content-end border-5 border-bottom'>
                            <button onClick={toggleAlamatPengiriman} className='border-1 border-light rounded py-1 px-2'>Pilih Alamat Lain</button>
                        </div>
                    </div>
                </div>
                <div className='px-3'>
                    <div className='d-md-flex align-items-center border-5 border-bottom pb-2'>
                        <span className='w-50'><IoLocation className='h4 text-dark' /> <p className='fw-bold d-inline text-dark'>Pengiriman Dari</p></span>
                        <div className='mt-sm-2 w-100'>
                            <button onClick={toggleComponenCheckoutLocation} className='text-dark w-100 border-1 border-primary bg-white rounded position-relative'>
                                <p className='mb-0 fw-bold'>Gudang Ripit</p>
                                <p className='my-0'>Kota. Jakarta Timur - DKI Jakartar</p>
                                <AiOutlineCheck className='position-absolute end-0 top-0 h4 me-2 text-dark' />
                            </button>
                        </div>
                    </div>
                </div>
                <div className='px-3'>
                    <div className='d-md-flex align-items-center border-5 border-bottom pb-2 mt-1'>
                        <span className='w-50'><FaShippingFast className='h4 text-dark' /> <p className='fw-bold d-inline text-dark'>Kurir</p></span>
                        <div className='mt-sm-2 w-100'>
                            <Dropdown className='w-100'>
                                <Dropdown.Toggle className='bg-white text-dark position-relative dropdownKurir w-100' >
                                    <p className='my-0 position-absolute end-50 text-center'>Pilih Kurir</p>
                                    <AiOutlineDown className='position-absolute end-0 top-0 h4 me-2 text-dark' />
                                </Dropdown.Toggle>

                                <Dropdown.Menu className='w-100'>
                                    <Dropdown.Item className='border-bottom border-1 position-relative'>TIKI <AiOutlineRight className='position-absolute end-0 top-0 h6 me-3' /></Dropdown.Item>
                                    <Dropdown.Item className='border-bottom border-1 position-relative'>POS <AiOutlineRight className='position-absolute end-0 top-0 h6 me-3' /></Dropdown.Item>
                                    <Dropdown.Item className='border-bottom border-1 position-relative'>JNT <AiOutlineRight className='position-absolute end-0 top-0 h6 me-3' /></Dropdown.Item>
                                    <Dropdown.Item className='border-bottom border-1 position-relative'>SAP <AiOutlineRight className='position-absolute end-0 top-0 h6 me-3' /></Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </div>
                </div>
                <div className='px-3'>
                    <div className='d-md-flex align-items-center border-5 border-bottom pb-2 mt-1'>
                        <span className='w-50'><AiOutlineCreditCard className='h4 text-dark' /> <p className='fw-bold d-inline text-dark'>Pembayaran</p></span>
                        <div className='mt-sm-2 w-100'>
                            <button onClick={toggleMetodePembayaran} className='text-dark w-100 py-2 border-1 border-primary bg-white rounded position-relative'>
                                <p className='mb-0 fw-bold'>Pilih Pembayaran</p>
                                <AiOutlineDown className='position-absolute end-0 top-0 h4 me-2 text-dark' />
                            </button>
                        </div>
                    </div>
                </div>
                <div className='px-3'>
                    <div className='d-md-flex align-items-center border-5 border-bottom pb-2 mt-1'>
                        <span className='w-50'><FaClipboardList className='h4 text-dark' /> <p className='fw-bold d-inline text-dark'>Barang</p></span>
                        <div className='mt-sm-2 w-100'>
                            <div className='d-flex align-items-center py-1 border-bottom border-top border-1'>
                                <img className="imageProduct me-2 rounded" alt="image Product" src="https://tshirtbar.id/wp-content/uploads/2021/08/OVERSIZE-TSHIRT-SALEM-323x500.png" />
                                <p
                                >bajukoko <br />
                                    <span className='fw-bold text-dark'>Rp170.000</span> x1 (300 Gram)
                                </p>
                            </div>
                            <div className='d-flex align-items-center py-1 border-bottom border-top border-1'>
                                <img className="imageProduct me-2 rounded" alt="image Product" src="https://tshirtbar.id/wp-content/uploads/2021/08/OVERSIZE-TSHIRT-SALEM-323x500.png" />
                                <p
                                >bajukoko <br />
                                    <span className='fw-bold text-dark'>Rp170.000</span> x1 (300 Gram)
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='px-3'>
                    <div className='d-md-flex align-items-center border-5 border-bottom pb-2 mt-1'>
                        <span className='w-50'><CgNotes className='h4 text-dark' /> <p className='fw-bold d-inline text-dark'>Catatan</p></span>
                        <div className='mt-sm-2 w-100'>
                            <textarea className="form-control" placeholder='Tambahkan Catatan...' rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div className='px-3'>
                    <div className='d-md-flex align-items-center border-5 border-bottom pb-2 mt-1'>
                        <span className='w-50'><CiDiscount1 className='h4 text-dark' /> <p className='fw-bold d-inline text-dark'>Promo</p></span>
                        <div className='mt-sm-2 w-100'>
                            <button onClick={toggleBtnPromo} className='text-dark w-100 py-2 border-1 border-primary bg-white rounded position-relative'>
                                <p className='mb-0 fw-bold'>Cari Promo disini</p>
                                {/* <p>Kota. Jakarta Timur - DKI Jakartar</p> */}
                                <AiOutlineDown className='position-absolute end-0 top-0 h4 me-2 text-dark' />
                            </button>
                        </div>
                    </div>
                </div>
                <div className='px-3'>
                    <div className='d-md-flex align-items-center border-5 border-bottom pb-2 mt-1'>
                        <span className='w-50'><CiBoxList className='h4 text-dark' /> <p className='fw-bold d-inline text-dark'>Rincian</p></span>
                        <div className='mt-sm-2 w-100'>
                            <div className='d-flex justify-content-between'>
                                <div>
                                    <p className='my-0'>Total Harga:</p>
                                    <p className='my-0'>Total Ongkir:</p>
                                    <p className='my-0'>Angka Unik:</p>
                                    <p className='my-0'><span className='text-dark fw-bold'>Potongan</span> Promo:</p>
                                </div>
                                <div>
                                    <p className='my-0 text-end'>Rp1.190.000</p>
                                    <p className='my-0 text-end'>Rp0</p>
                                    <p className='my-0 text-end'>(Rp279)</p>
                                    <p className='my-0 text-end'>(Rp0)</p>
                                </div>
                            </div>
                            <div className='d-flex justify-content-between border-top border-1 mt-1 pt-1'>
                                <div>
                                    <p className='my-0 fw-bold text-dark'>Total Tagihan</p>
                                </div>
                                <div>
                                    <p className='my-0 text-end fw-bold text-dark'>Rp1.190.000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='px-3 py-3 d-flex justify-content-end bg-light'>
                <button onClick={() => btnPembayaran()} className='bg-primary text-white py-1 px-2 border-0 rounded'>PEMBAYARAN</button>
            </div>
            {
                componenCheckoutlocation && (
                    <div className='w-100 h-100 d-flex justify-content-center backgroundChekoutPesanan'>
                        <LocationPengiriman />
                    </div>
                )
            }
            {
                alamatPengiriman && (
                    <div className='w-100 h-100 d-flex justify-content-center backgroundChekoutPesanan'>
                        <AlamatPengiriman />
                    </div>
                )
            }
            {
                tambahAlamat && (
                    <div className='w-100 h-100 d-flex justify-content-center backgroundChekoutPesanan'>
                        <TambahAlamat />
                    </div>
                )
            }
            {
                ubahAlamat && (
                    <div className='w-100 h-100 d-flex justify-content-center backgroundChekoutPesanan'>
                        <UbahAlamat />
                    </div>
                )
            }
            {
                metodePembayaran && (
                    <div className='w-100 h-100 d-flex justify-content-center backgroundChekoutPesanan'>
                        <MetodePembayaran />
                    </div>
                )
            }
            {
                btnPromo && (
                    <div className='w-100 h-100 d-flex justify-content-center backgroundChekoutPesanan'>
                        <Promo />
                    </div>
                )
            }
        </div>
    )
}

export default ChekoutPesanan