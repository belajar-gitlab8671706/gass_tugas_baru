import './style-componen-checkout.css';
import { AiOutlineDown, AiOutlinePlus } from "react-icons/ai";
import { useCounterStore } from '@/store';

const AlamatPengiriman = () => {
    const { toggleAlamatPengiriman, toggleTambahAlamat, toggleUbahAlamat } = useCounterStore()

    const btnTambahAlamat = () => {
        toggleAlamatPengiriman()
        toggleTambahAlamat()
    }

    const btnUbahAlamat = () => {
        toggleUbahAlamat()
        toggleAlamatPengiriman()
    }

    return (
        <div className="bg-white mt-5 componenLocationPengiriman">
            <div className="d-flex justify-content-between align-items-center bg-light py-3 px-3">
                <p className='my-0'>Alamat Pengiriman</p>
                <AiOutlineDown className="h4 text-dark" onClick={toggleAlamatPengiriman} />
            </div>
            <div className='py-2 px-3'>
                <button onClick={() => btnTambahAlamat()} className='fw-bold border w-100 py-2 rounded'>< AiOutlinePlus className='h5' /> Tambah Alamat Baru</button>
            </div>
            <div className='mx-3 border border-primary py-3 px-3 rounded'>
                <p className='d-inline me-2 fw-bold text-dark'>Yono</p><span className='backgroundUtamaAlamatPesanan px-1'>Utama</span>
                <p className='my-0'>0887877878</p>
                <p>
                    jalan sukun rt 08
                    Cipayung - Kota. Jakarta Timur
                    DKI Jakarta (12312)
                </p>
                <button onClick={() => btnUbahAlamat()} className='py-1 px-2 border-primary border bg-white text-primary rounded'>Ubah ALamat</button>
            </div>
        </div>
    )
}

export default AlamatPengiriman