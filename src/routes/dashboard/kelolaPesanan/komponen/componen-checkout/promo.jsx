import './style-componen-checkout.css';
import { AiOutlineDown, } from "react-icons/ai";
import { useCounterStore } from '@/store';
import { CiDiscount1 } from "react-icons/ci";

const Promo = () => {
    const { toggleAlamatPengiriman, toggleBtnPromo } = useCounterStore()

    return (
        <div className="bg-white mt-5 componenLocationPengiriman">
            <div className="d-flex justify-content-between align-items-center bg-light py-3 px-3">
                <p className='my-0'>Promo</p>
                <AiOutlineDown className="h4 text-dark" onClick={toggleBtnPromo} />
            </div>
            <div>
                <CiDiscount1 className='mx-auto d-block mt-5 h1' />
                <p className='text-center'>Promo saat ini tidak tersedia</p>
            </div>
        </div>
    )
}

export default Promo