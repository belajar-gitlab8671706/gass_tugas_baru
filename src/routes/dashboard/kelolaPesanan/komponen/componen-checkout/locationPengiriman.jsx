import './style-componen-checkout.css';
import { AiOutlineDown, AiOutlineRight } from "react-icons/ai";
import { useCounterStore } from '@/store';
import { IoLocation } from "react-icons/io5";

const LocationPengiriman = () => {
    const { toggleComponenCheckoutLocation } = useCounterStore()

    return (
        <div className="bg-white mt-5 componenLocationPengiriman">
            <div className="d-flex justify-content-between align-items-center bg-light py-3 px-3">
                <p className='my-0'>Lokasi Pengiriman</p>
                <AiOutlineDown className="h4 text-dark" onClick={toggleComponenCheckoutLocation} />
            </div>
            <div className='d-flex align-items-center py-2 px-3 border-bottom position-relative'>
                <IoLocation className='h5' />
                <div className='ms-5'>
                    <p className='fw-bold my-0'>Gudang Ripit</p>
                    <p className='my-0'>Kota. Jakarta Timur - DKI Jakarta</p>
                </div>
                <AiOutlineRight className='position-absolute end-0 me-3 h4' />
            </div>
        </div>
    )
}

export default LocationPengiriman