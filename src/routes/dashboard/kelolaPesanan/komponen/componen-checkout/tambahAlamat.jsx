import './style-componen-checkout.css';
import { useCounterStore } from '@/store';
import { IoLocation } from "react-icons/io5";
import { IoMdClose } from "react-icons/io";
import { Input } from '@/components';

const TambahAlamat = () => {
    const { toggleTambahAlamat, toggleAlamatPengiriman } = useCounterStore()

    const closeBtnTambahAlamat = () => {
        toggleTambahAlamat()
        toggleAlamatPengiriman()
    }

    return (
        <div className="bg-white componenLocationPengiriman">
            <div className="d-flex justify-content-between align-items-center border-bottom py-3 px-3">
                <p className='my-0 fw-bold text-dark h6'> <IoLocation className='h5' /> Tambah Alamat</p>
                <IoMdClose className="h4 text-dark" onClick={() => closeBtnTambahAlamat()} />
            </div>
            <div className='py-2 px-3'>
                <label className='w-100'>
                    Label Alamat
                    <Input />
                </label>
                <p className='my-0'>Contoh: Alamat Rumah, Alamat Kantor, dll..</p>
                <div className='d-md-flex align-items-center mb-2'>
                    <label className='w-100 me-2 mt-1'>
                        Nama Penerima
                        <Input />
                    </label>
                    <label className='w-100 mt-1'>
                        Nomor Telepon
                        <Input />
                    </label>
                </div>
                <div className='d-flex align-items-center'>
                    <label className='w-100 me-2'>
                        Kota atau Kecamatan
                        <Input />
                    </label>
                    <label>
                        Kode Pos
                        <Input />
                    </label>
                </div>
                <label className='w-100 my-2'>
                    Alamat
                    <textarea className="form-control" rows="3"></textarea>
                </label>
                <input className="form-check-input me-2" type="checkbox" value="" id="flexCheckDefault" />
                <label className="form-check-label" for="flexCheckDefault">
                    Jadikan Alamat Utama
                </label>
            </div>
            <div className='border-top py-2 px-3 bg-white d-flex justify-content-end'>
                <button className='py-1 px-2 border border-danger bg-white rounded text-danger me-2'>SAMPAH</button>
                <button className='py-1 px-2 border-0 rounded bg-primary text-white'>SIMPAN</button>
            </div>
        </div>
    )
}

export default TambahAlamat