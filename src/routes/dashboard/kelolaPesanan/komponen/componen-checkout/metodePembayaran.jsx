import './style-componen-checkout.css';
import { AiOutlineDown, AiOutlineRight } from "react-icons/ai";
import { useCounterStore } from '@/store';
import { IoLocation } from "react-icons/io5";

const MetodePembayaran = () => {
    const { toggleMetodePembayaran } = useCounterStore()

    return (
        <div className="bg-white mt-5 componenLocationPengiriman">
            <div className="d-flex justify-content-between align-items-center bg-light py-3 px-3">
                <p className='my-0'>Metode Pembayaran</p>
                <AiOutlineDown className="h4 text-dark" onClick={toggleMetodePembayaran} />
            </div>
            <div className='d-flex align-items-center py-2 px-3 border-bottom position-relative'>
                <img className='imageBank' src='https://cdn0-production-images-kly.akamaized.net/ClOSWKZI_eb6xIDHlG8tIQqaf2Y=/800x450/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/4255282/original/094712800_1670569941-mandiri.jpg' alt='image bank' />
                <div className='ms-5'>
                    <p className='fw-bold my-0'>Tranfer MANDIRI</p>
                    <p className='my-0'>produsen - 321321</p>
                </div>
                <AiOutlineRight className='position-absolute end-0 me-3 h4' />
            </div>
        </div>
    )
}

export default MetodePembayaran