import { Button } from "@/components";
import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <div
      className="d-flex align-items-center justify-content-center flex-column gap-4"
      style={{ height: "100vh" }}
    >
      <img
        src="/images/404.svg"
        alt="Error 404"
        style={{ width: "60%", maxWidth: "420px" }}
      />
      <Button as={Link} to="/" bg="primary">
        Kembali ke Halaman Utama
      </Button>
    </div>
  );
}
