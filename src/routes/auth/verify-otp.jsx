import { useLocation } from "react-router-dom";
import { useMutation } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { useForm } from "react-hook-form";
import { extractRouteParams } from "@/libs/utils";

import { Input, Alert, Button } from "@/components";

export function Component() {
  const location = useLocation();
  const { code } = extractRouteParams(location.search);

  const verifyOtp = useMutation(
    (data) =>
      axios
        .post("", { act: "user_verif_otp_register", code, ...data })
        .then((res) => res.data),
    {
      onSuccess: (data) => {
        localStorage.setItem("token", data.token);
        window.location.href = "/";
      },
    }
  );

  const { register, handleSubmit } = useForm();

  return (
    <div
      className="card shadow border-primary"
      style={{ borderTopWidth: "10px", borderRadius: "1.25rem" }}
    >
      <div className="card-inner card-inner-lg">
        <div className="nk-block-head">
          <div className="nk-block-head-content">
            <h4 className="nk-block-title">Verify Otp</h4>
            <div className="nk-block-des"></div>
          </div>
        </div>
        <form onSubmit={handleSubmit(verifyOtp.mutate)}>
          {verifyOtp.isError && (
            <Alert
              visible={verifyOtp.isError}
              label="Error!"
              state="danger"
              onClose={verifyOtp.reset}
            >
              <div
                dangerouslySetInnerHTML={{
                  __html: verifyOtp.error?.message?.replace("<br>", ""),
                }}
              ></div>
            </Alert>
          )}
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-inbox fs-3"></em>
              </div>
              <Input
                type="number"
                size="lg"
                id="otp"
                placeholder="Code OTP"
                name="otp"
                className="ps-5"
                {...register("otp", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <Button
              className="btn-block"
              size="lg"
              bg="primary"
              loading={
                verifyOtp.status === "loading" || verifyOtp.status === "success"
              }
              disabled={
                verifyOtp.status === "loading" || verifyOtp.status === "success"
              }
            >
              VERIFY
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
}
