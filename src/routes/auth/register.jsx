import "./register.css";

import { Link, useNavigate, useSearchParams } from "react-router-dom";
import { Input, Button, Alert, TelInput } from "@/components";

import { useMutation } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { useEffect } from "react";

import { GoogleReCaptcha } from "react-google-recaptcha-v3";
import { useCallback } from "react";
import phone from "@/json/phone.json";

export function Component() {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  const register = useMutation(
    (data) =>
      axios
        .post("", {
          act: "user_register",
          ...data,
          ...(searchParams.get("civ") ? { civ: searchParams.get("civ") } : {}),
        })
        .then((res) => res.data),
    {
      onSuccess: (data) => {
        navigate(`/auth/verify-otp?code=${data.verif_code}`);
      },
      onError: (error) => {
        if (error.data.status === "exist" && !error.data.verified) {
          navigate(`/auth/verify-otp?code=${error.data.verif_code}`);
        } else {
          toast.error(() => (
            <div
              dangerouslySetInnerHTML={{
                __html: error.message?.replace("<br>", ""),
              }}
            ></div>
          ));
        }
      },
    }
  );

  const {
    register: form,
    handleSubmit,
    control,
    trigger,
    watch,
    setValue,
    resetField,
  } = useForm({
    defaultValues: {
      country: "ID",
    },
  });

  useEffect(() => {
    setTimeout(() => {
      $(".select2-selection__rendered").addClass("ps-5");
      $(".select2-selection__rendered").addClass("form-control-lg");
    }, 10);

    if (!watch("phone")?.startsWith("62")) {
      resetField("city");
    }
  }, [watch("phone")]);

  const handleCaptcha = useCallback((token) => {
    setValue("captcha", token);
  }, []);

  const onSubmit = (data) => {
    if (!data.captcha) {
      return toast.error("Please verify captcha");
    }

    if (!data.phone.startsWith("62")) {
      Object.keys(phone).forEach((item) => {
        if (phone[item]) {
          if (
            data.phone
              .replace(/\D/g, "")
              .startsWith(phone[item].replace(/\D/g, ""))
          ) {
            data.country = item;
          }
        }
      });
    }

    setTimeout(() => {
      register.mutate(data);
    }, 100);
  };

  return (
    <div
      className="card shadow border-primary"
      style={{ borderTopWidth: "10px", borderRadius: "1.25rem" }}
    >
      <div className="card-inner">
        <div className="nk-block-head">
          <div className="nk-block-head-content">
            <h4 className="nk-block-title text-center">Registration</h4>
            <div className="nk-block-des text-center">
              <p>Pastikan anda menggunakan No. Whatsapp & Email yang aktif </p>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Alert
            visible={
              register.isError &&
              (register.error.data.status !== "exist" ||
                register.error.data.verified)
            }
            label="Error!"
            state="danger"
            onClose={register.reset}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: register.error?.message?.replace("<br>", ""),
              }}
            ></div>
          </Alert>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-user-circle fs-3"></em>
              </div>
              <Input
                type="text"
                size="lg"
                id="name"
                placeholder="Full Name"
                name="name"
                className="ps-5"
                {...form("name", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-mail fs-3"></em>
              </div>
              <Input
                type="email"
                size="lg"
                id="email"
                placeholder="Email"
                name="email"
                className="ps-5"
                {...form("email", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-lock-alt fs-3"></em>
              </div>
              <Input
                type="password"
                size="lg"
                id="password"
                name="password"
                placeholder="Password"
                className="ps-5"
                {...form("password", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-user-circle fs-3"></em>
              </div>
              <Input
                type="text"
                size="lg"
                id="username"
                placeholder="Username"
                name="username"
                className="ps-5"
                {...form("username", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-user-circle fs-3"></em>
              </div>
              <Input
                type="text"
                size="lg"
                id="compani_id"
                placeholder="Compani ID"
                name="compani_id"
                className="ps-5"
                {...form("compani_id", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-user-circle fs-3"></em>
              </div>
              <Input
                type="text"
                size="lg"
                id="level"
                placeholder="Level"
                name="level"
                className="ps-5"
                {...form("level", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-user-circle fs-3"></em>
              </div>
              <Input
                type="text"
                size="lg"
                id="sub_level"
                placeholder="Sub Level"
                name="sub_level"
                className="ps-5"
                {...form("sub_level", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-user-circle fs-3"></em>
              </div>
              <Input
                type="text"
                size="lg"
                id="gender"
                placeholder="Gender"
                name="gender"
                className="ps-5"
                {...form("gender", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem", zIndex: 9999999999 }}
              >
                <em className="icon ni ni-whatsapp fs-3"></em>
              </div>
              <TelInput
                control={control}
                trigger={trigger}
                id="phone"
                placeholder="No. Whatsapp"
                inputClass="form-control-lg"
                name="phone"
                country="id"
                enableSearch={true}
                enableAreaCodes={true}
                countryCodeEditable={false}
                required
                autoComplete="off"
              />
            </div>
          </div>

          <div className="divider my-5"></div>

          <div className="form-group">
            <GoogleReCaptcha onVerify={handleCaptcha} />
          </div>

          <div className="form-group mt-5">
            <Button
              type="submit"
              className="btn-block"
              size="lg"
              bg="primary"
              loading={
                register.status === "loading" || register.status === "success"
              }
              disabled={
                register.status === "loading" || register.status === "success"
              }
            >
              REGISTER
            </Button>
          </div>
        </form>
        <div className="form-note-s2 text-center pt-4">
          {" "}
          Already have an account?{" "}
          <Link
            to={`/auth/login${searchParams.get("civ") ? `?civ=${searchParams.get("civ")}` : ""
              }`}
          >
            <strong>Sign In</strong>
          </Link>
        </div>
      </div>
    </div>
  );
}
