import { useNavigate, Link, useSearchParams } from "react-router-dom";
import { useMutation } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { useForm } from "react-hook-form";

import { Input, Button, Alert, TelInput } from "@/components";

export function Component() {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const forgot = useMutation(
    (data) =>
      axios.post("", { act: "user_forgot", ...data }).then((res) => res.data),
    {
      onSuccess: (data) => {
        navigate(`/auth/forgot-password/otp?code=${data.verif_code}`);
      },
    }
  );

  const { register, handleSubmit, control, trigger } = useForm();

  return (
    <div
      className="card shadow border-primary"
      style={{ borderTopWidth: "10px", borderRadius: "1.25rem" }}
    >
      <div className="card-inner card-inner-lg">
        <div className="nk-block-head">
          <div className="nk-block-head-content">
            <h4 className="nk-block-title">Forgot Password</h4>
            <div className="nk-block-des">
              <p>Gunakan No. Whatsapp saat registrasi</p>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(forgot.mutate)}>
          <Alert
            visible={forgot.isError}
            label="Error!"
            state="danger"
            onClose={forgot.reset}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: forgot.error?.message?.replace("<br>", ""),
              }}
            ></div>
          </Alert>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem", zIndex: 9999999999 }}
              >
                <em className="icon ni ni-whatsapp fs-3"></em>
              </div>
              <TelInput
                control={control}
                trigger={trigger}
                id="phone"
                placeholder="No. Whatsapp"
                inputClass="form-control-lg"
                name="phone"
                country="id"
                enableSearch={true}
                enableAreaCodes={true}
                countryCodeEditable={false}
                required
                autoComplete="off"
              />
            </div>
          </div>
          <div className="form-group">
            <Button
              className="btn-block"
              size="lg"
              bg="primary"
              loading={
                forgot.status === "loading" || forgot.status === "success"
              }
              disabled={
                forgot.status === "loading" || forgot.status === "success"
              }
            >
              RESET PASSWORD
            </Button>
          </div>
          <div className="form-note-s2 text-center pt-4">
            <Link
              to={`/auth/login${
                searchParams.get("civ") ? `?civ=${searchParams.get("civ")}` : ""
              }`}
              className="d-flex align-items-center gap-1 justify-content-center"
            >
              <i className="ni ni-chevron-left fs-4"></i>
              Back to Login
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}
