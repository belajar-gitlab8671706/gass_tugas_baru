import { useNavigate, useLocation } from "react-router-dom";
import { useMutation } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { extractRouteParams } from "@/libs/utils";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

import { Input, Alert, Button } from "@/components";

export function Component() {
  const navigate = useNavigate();
  const location = useLocation();
  const { id_otp, code_otp } = extractRouteParams(location.search);

  const { register, handleSubmit } = useForm();
  const changePwd = useMutation(
    (data) =>
      axios
        .post("", { act: "user_change_pass", id_otp, code_otp, ...data })
        .then((res) => res.data),
    {
      onSuccess: () => {
        toast.success("Password changed successfully!");
        navigate(`/auth/login`);
      },
    }
  );

  return (
    <div
      className="card shadow border-primary"
      style={{ borderTopWidth: "10px", borderRadius: "1.25rem" }}
    >
      <div className="card-inner card-inner-lg">
        <div className="nk-block-head">
          <div className="nk-block-head-content">
            <h4 className="nk-block-title">Forgot Password</h4>
            <div className="nk-block-des">
              <p>Masukkan Password baru.</p>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(changePwd.mutate)}>
          <Alert
            visible={changePwd.isError}
            label="Error!"
            state="danger"
            onClose={changePwd.reset}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: changePwd.error?.message?.replace("<br>", ""),
              }}
            ></div>
          </Alert>
          <div className="form-group">
            <div className="form-label-group">
              <label className="form-label" htmlFor="new_password">
                New Password
              </label>
            </div>
            <div className="form-control-wrap">
              <Input
                type="password"
                size="lg"
                id="new_password"
                placeholder="Enter your new password"
                name="new_password"
                autoComplete="off"
                {...register("new_password", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <Input
                type="password"
                size="lg"
                id="confirm_new_password"
                placeholder="Enter your new password confirmation"
                name="confirm_new_password"
                autoComplete="off"
                {...register("confirm_new_password", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <Button
              className="btn-block"
              size="lg"
              bg="primary"
              loading={
                changePwd.status === "loading" || changePwd.status === "success"
              }
              disabled={
                changePwd.status === "loading" || changePwd.status === "success"
              }
            >
              CHANGE PASSWORD
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
}
