import { useLocation, useNavigate } from "react-router-dom";
import { useMutation } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { useForm } from "react-hook-form";
import { extractRouteParams } from "@/libs/utils";
import { Link } from "react-router-dom";

import { Input, Button, Alert } from "@/components";

export function Component() {
  const navigate = useNavigate();
  const location = useLocation();
  const { code } = extractRouteParams(location.search);

  const verifyOtp = useMutation(
    (data) =>
      axios
        .post("", { act: "user_verif_otp_forgot", code, ...data })
        .then((res) => res.data),
    {
      onSuccess: (data) => {
        navigate(
          `/auth/forgot-password/change?id_otp=${data.id_otp}&code_otp=${data.code_otp}`
        );
      },
    }
  );

  const { register, handleSubmit } = useForm();

  return (
    <div
      className="card shadow border-primary"
      style={{ borderTopWidth: "10px", borderRadius: "1.25rem" }}
    >
      <div className="card-inner card-inner-lg">
        <div className="nk-block-head">
          <div className="nk-block-head-content">
            <h4 className="nk-block-title">Forgot Password</h4>
            <div className="nk-block-des">
              <p>Verifikasi kode OTP.</p>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(verifyOtp.mutate)}>
          <Alert
            visible={verifyOtp.isError}
            label="Error!"
            state="danger"
            onClose={verifyOtp.reset}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: verifyOtp.error?.message?.replace("<br>", ""),
              }}
            ></div>
          </Alert>
          <div className="form-group">
            <div className="form-label-group">
              <label className="form-label" htmlFor="otp">
                OTP
              </label>
              <Link
                className="link link-primary link-sm"
                to="/auth/forgot-password"
              >
                Nomor Handphone Salah?
              </Link>
            </div>
            <div className="form-control-wrap">
              <Input
                type="number"
                size="lg"
                id="otp"
                placeholder="Enter your OTP code"
                name="otp"
                autoComplete="off"
                {...register("otp", {
                  required: true,
                })}
              />
            </div>
          </div>
          <div className="form-group">
            <Button
              className="btn-block"
              size="lg"
              bg="primary"
              loading={
                verifyOtp.status === "loading" || verifyOtp.status === "success"
              }
              disabled={
                verifyOtp.status === "loading" || verifyOtp.status === "success"
              }
            >
              VERIFY
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
}
