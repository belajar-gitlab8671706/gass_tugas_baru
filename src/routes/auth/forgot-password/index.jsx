import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

export function Redirect() {
  const navigate = useNavigate();
  useEffect(() => {
    navigate("/auth/forgot-password/phone");
  }, [navigate]);

  return <></>;
}
