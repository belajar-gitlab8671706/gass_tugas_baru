import { useRef, useEffect } from "react";

import { Link, Outlet, useNavigation, useNavigate } from "react-router-dom";

export function Component() {
  return (
    <div className="nk-app-root">
      <div className="nk-main">
        <div className="nk-wrap nk-wrap-nosidebar">
          <div className="nk-content">
            <div className="nk-block nk-block-middle nk-auth-body wide-xs">
              <div className="brand-logo pb-4 text-center">
                <Link to="/" className="logo-link">
                  <img
                    className="logo-light logo-img logo-img-lg"
                    src="/images/GASS LOGO.png"
                    alt="logo"
                  ></img>
                  <img
                    className="logo-dark logo-img logo-img-lg"
                    src="/images/GASS LOGO.png"
                    alt="logo-dark"
                  ></img>
                </Link>
              </div>
              <Outlet />
            </div>
          </div>
        </div>
      </div>

      <LoadingLayer />
    </div>
  );
}

export function Redirect() {
  const navigate = useNavigate();
  useEffect(() => {
    navigate("/auth/login");
  }, [navigate]);

  return <></>;
}

function LoadingLayer() {
  const navigation = useNavigation();
  const loadingRef = useRef(null);

  useEffect(() => {
    if (navigation.state === "loading") {
      loadingRef.current.style.display = "flex";
    } else {
      setTimeout(() => {
        if (loadingRef.current) loadingRef.current.style.display = "none";
      }, 200);
    }
  }, [navigation]);

  return (
    <div
      className="position-fixed justify-content-center align-items-center"
      style={{
        top: 0,
        left: 0,
        right: 0,
        height: "100vh",
        backgroundColor: "rgba(15, 10, 25, 0.1)",
        transition: "all 0.2s ease",
        opacity: navigation.state === "loading" ? 1 : 0,
      }}
      ref={loadingRef}
    >
      <div className="nk-loading">
        <div className="spinner-border text-blue" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    </div>
  );
}

Component.DisplayName = "AuthRoot";
