import { Link, useNavigate, useSearchParams } from "react-router-dom";
import { Input, Button, Alert, TelInput } from "@/components";

import { useMutation, useQueryClient } from "@tanstack/react-query";
import axios from "@/libs/axios";
import { useForm, Controller } from "react-hook-form";
import { parsePhone } from "@/libs/utils";
import Cookies from "js-cookie";

export function Component() {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const queryClient = useQueryClient();

  const { mutate: acceptInvite } = useMutation(
    (code) =>
      axios
        .post("", { act: "project_accept_invite", code })
        .then((res) => res.data.data),
    {
      onSuccess: (data) => {
        navigate(`/project/setting/${data.project_key}`);
      },
    }
  );

  const login = useMutation(
    (data) =>
      axios
        .post("", { act: "user_login", ...data, phone: parsePhone(data.phone) })
        .then((res) => res.data),
    {
      onSuccess: (data) => {
        localStorage.setItem("auth-token", data.token);
        queryClient.invalidateQueries(["auth", "user"]);
        Cookies.set("gass-token", data.token, {
          secure: true,
          domain: "gass.co.id",
          // domain: "localhost",
          sameSite: "strict",
        });

        if (searchParams.get("civ")) acceptInvite(searchParams.get("civ"));
        else {
          const params = extractRouteParams(window.location.search);
          if (params.from) {
            window.location.href = `${params.from}${params.callbackUrl || ""}`;
          } else {
            navigate(localStorage.getItem("auth-redirect"));
            localStorage.removeItem("auth-redirect");
          }
        }
      },
    }
  );

  const { register, handleSubmit, control, trigger, watch } = useForm();
  return (
    <div
      className="card shadow border-primary"
      style={{ borderTopWidth: "10px", borderRadius: "1.25rem" }}
    >
      <div className="card-inner py-5">
        <div className="nk-block-head">
          <div className="nk-block-head-content">
            <h4 className="nk-block-title">Sign In to Dashboard GASS</h4>
            <div className="nk-block-des">
              <p>Gunakan No. Whatsapp saat registrasi</p>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(login.mutate)}>
          <Alert
            visible={login.isError}
            label="Error!"
            state="danger"
            onClose={login.reset}
          >
            Gagal Login
          </Alert>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem", zIndex: 9999999999 }}
              >
                <em className="icon ni ni-whatsapp fs-3"></em>
              </div>
              <TelInput
                control={control}
                trigger={trigger}
                id="phone"
                placeholder="No. Whatsapp"
                inputClass="form-control-lg"
                name="phone"
                country="id"
                enableSearch={true}
                enableAreaCodes={true}
                countryCodeEditable={false}
                required
                autoComplete="off"
              />
            </div>
          </div>
          <div className="form-group">
            <div className="form-control-wrap">
              <div
                className="form-icon form-icon-left ms-1"
                style={{ top: "0.2rem" }}
              >
                <em className="icon ni ni-lock-alt fs-3"></em>
              </div>
              <Input
                type="password"
                size="lg"
                id="password"
                name="password"
                placeholder="Password"
                className="ps-5"
                {...register("password", {
                  required: true,
                })}
              />
            </div>
            <div className="text-end mt-2">
              <Link
                className="link link-primary link-sm"
                to="/auth/forgot-password"
              >
                Forgot Password?
              </Link>
            </div>
          </div>
          <div className="form-group">
            <Button
              type="submit"
              className="btn-block"
              size="lg"
              bg="primary"
              loading={login.status === "loading" || login.status === "success"}
              disabled={
                login.status === "loading" || login.status === "success"
              }
            >
              SIGN IN
            </Button>
          </div>
        </form>
        <div className="form-note-s2 text-center pt-4">
          {" "}
          New on our platform?{" "}
          <Link to="/auth/register">Create an account</Link>
        </div>
      </div>
    </div>
  );
}
