import { toast } from "react-toastify";

export function extractRouteParams(search, asFormData = false) {
  if (asFormData) {
    const formData = new FormData();
    for (const [key, value] of Object.entries(params)) {
      formData.append(key, value);
    }
    return formData;
  } else {
    const params = {};
    const searchParams = new URLSearchParams(search);
    for (const [key, value] of searchParams) {
      if (key.endsWith("[]")) {
        const keyName = key.replace("[]", "");
        if (params[keyName]) {
          params[keyName].push(value);
        } else {
          params[keyName] = [value];
        }
      } else {
        params[key] = value;
      }
    }

    return params;
  }
}

export function currency(value, options = {}) {
  return Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    ...options,
  }).format(value || 0);
}

export function formatNumber(value, options = {}) {
  return Intl.NumberFormat("id-ID", {
    maximumFractionDigits: 2,
    ...options,
  }).format(value || 0);
}

export function avatarName(name) {
  return name
    ?.replace(/[^a-zA-Z ]/g, "")
    .split(" ")
    .slice(0, 2)
    .map((n) => n.slice(0, 1).toUpperCase())
    .join("");
}

export function copyText(text, cb) {
  function listener(e) {
    e.clipboardData.setData("text/html", text);
    e.clipboardData.setData("text/plain", text);
    e.preventDefault();
  }
  document.addEventListener("copy", listener);
  document.execCommand("copy");
  document.removeEventListener("copy", listener);

  if (cb) cb();
}

export function getNotificationPermission() {
  if (Notification.permission === "granted") {
    return true;
  } else if (Notification.permission !== "denied") {
    Notification.requestPermission().then((permission) => {
      if (permission === "granted") {
        return true;
      }
    });
  }
  return false;
}

export function callNotif(title, message, callback) {
  if (getNotificationPermission()) {
    const notification = new Notification(title, {
      body: message,
      icon: "/favicon.ico",
    });
    notification.onclick = function (ev) {
      ev.preventDefault();
      window.focus();
      notification.close();
      if (callback) callback();
    };
    new Audio("https://app.ezchat.co.id/ezchat.mp3").play();
  }
}

export function toLink(text, isme) {
  var Rexp =
    /((http|https|ftp):\/\/[\w?=&.\/-;,#~%-]+(?![\w\s?&.\/;,#~%"=-]*>))/g;
  if (isme) {
    return text.replace(
      Rexp,
      "<a href='$1' target='_blank' style='color: #bed2ff;'>$1</a>"
    );
  } else {
    return text.replace(Rexp, "<a href='$1' target='_blank'>$1</a>");
  }
}

export function scrollToBottom(tg) {
  const $divElement = $(tg);
  $divElement.scrollTop($divElement.prop("scrollHeight"));
}

export function buildChatCaption(psn, type, isme = false) {
  try {
    var caption = "";
    if (type == "recent") {
      if (psn.type == "jpeg" || psn.type == "png" || psn.type == "webp") {
        caption = '<em class="icon ni ni-img"></em><span>Image</span>';
      } else if (psn.type == "ogg") {
        caption = '<em class="icon ni ni-mic"></em><span>Audio</span>';
      } else if (psn.type == "mp4") {
        caption = '<em class="icon ni ni-video"></em><span>Video</span>';
      } else if (psn.type == "text") {
        caption = psn.caption;
      } else {
        caption =
          '<em class="icon ni ni-clip-h"></em><span>' +
          psn.filename +
          "</span>";
      }
    } else {
      if (psn.caption != "") {
        psn.caption = psn.caption.replace(new RegExp("\r?\n", "g"), "<br />");
        psn.caption = toLink(psn.caption, isme);
      }
      if (
        psn.type == "jpeg" ||
        psn.type == "png" ||
        psn.type == "webp" ||
        psn.type == "jpg"
      ) {
        caption =
          '<div class="gallery card"><a class="gallery-image popup-image" href="' +
          psn.url +
          '"><img class="w-100 rounded-top" alt="" style="max-width: 320px" src="' +
          psn.url +
          '"></a><div class="gallery-body card-inner align-center justify-between flex-wrap g-2"><div class="user-card"><div class="user-info"><span class="sub-text d-block">' +
          psn.caption +
          "</span></div></div></div></div>";
      } else if (psn.type == "text") {
        caption = psn.caption;
      } else if (psn.type == "mp4") {
        caption =
          '<div class="card"><div class="kanban-item"><div class="kanban-item-meta"><ul class="kanban-item-meta-list"><li><a class="download" href="' +
          psn.url +
          '" target="_blank"><iframe src="' +
          psn.url +
          '" allowfullscreen></iframe></a></li></ul></div></div></div>' +
          psn.caption;
      } else if (psn.type == "mp4") {
        caption =
          '<div class="card"><div class="kanban-item"><div class="kanban-item-meta"><ul class="kanban-item-meta-list"><li><a class="download" href="' +
          psn.url +
          '" target="_blank"><iframe src="' +
          psn.url +
          '" allowfullscreen></iframe></a></li></ul></div></div></div>' +
          psn.caption;
      } else if (psn.type == "ogg") {
        caption =
          '<div class="card"><div class="kanban-item"><div class="kanban-item-meta"><ul class="kanban-item-meta-list"><li><Audio controls="" src="' +
          psn.url +
          '"></Audio></li></ul></div></div></div>' +
          psn.caption;
      } else {
        caption =
          '<div class="card"><div class="kanban-item"><div class="kanban-item-meta"><ul class="kanban-item-meta-list"><li><a class="download" href="' +
          psn.url +
          '" target="_blank"><em class="icon ni ni-clip-h"></em><span>' +
          psn.filename +
          "</span></a></li></ul></div></div></div>" +
          psn.caption;
      }
    }
    return caption;
  } catch (error) {
    toast.error(error.message);
    return "";
  }
}

export function reverseArray(arr) {
  var newArray = [];
  for (var i = arr.length - 1; i >= 0; i--) {
    newArray.push(arr[i]);
  }
  return newArray;
}

export function formDataToObject(formData) {
  const object = {};
  formData.forEach((value, key) => {
    object[key] = value;
  });
  return object;
}

export function getFileExt(path) {
  return path.split(".").pop();
}

export function formatDate(dateStr, withTime = false) {
  if (!dateStr) return "-";
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mei",
    "Jun",
    "Jul",
    "Agu",
    "Sep",
    "Okt",
    "Nov",
    "Des",
  ];

  const date = new Date(dateStr);
  return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()} ${
    withTime ? dateStr.split(" ")[1] : ""
  }`;
}

export function isMobile() {
  const toMatch = [
    /Android/i,
    /webOS/i,
    /iPhone/i,
    /iPad/i,
    /iPod/i,
    /BlackBerry/i,
    /Windows Phone/i,
  ];

  return toMatch.some((toMatchItem) => {
    return navigator.userAgent.match(toMatchItem);
  });
}

export function parsePhone(phone) {
  if (phone.startsWith("0")) {
    return `62${phone.substr(1)}`;
  } else if (phone.startsWith("8")) {
    return `62${phone}`;
  } else {
    return phone;
  }
}

export function encrypt(data) {
  return btoa(`${JSON.stringify(data)}|${import.meta.env.VITE_DOMAIN}`);
}

export function decrypt(data) {
  return JSON.parse(atob(data).split("|")[0]);
}

export function findDeep(data, id) {
  return data.some(function (e) {
    if (e.id == id) return true;
    else if (e.items) return findDeep(e.items, id);
  });
}

export function accessItemByIndex(arr, indexArray) {
  // indexArray = "[0][items][2][items][2][items][1]" (example)
  indexArray = indexArray.slice(1);
  indexArray = indexArray.slice(0, -1);
  indexArray = indexArray.split("][");

  console.log(arr, indexArray);

  let result = arr;
  for (const index of indexArray) {
    result = result[index];
    console.log(result);
  }
  return result;
}

export function randomChar(length = 10) {
  const chars =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let result = "";
  for (let i = 0; i < length; i++) {
    result += chars[Math.floor(Math.random() * chars.length)];
  }
  return result + Date.now();
}
