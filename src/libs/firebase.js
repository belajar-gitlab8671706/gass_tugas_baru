import { initializeApp } from "firebase/app";
import { onMessage, getMessaging, getToken } from "firebase/messaging";
import { getAnalytics } from "firebase/analytics";
import { callNotif } from "./utils";
import { create } from "zustand";

export const useFcmStore = create((set) => ({
  fcm: null,
  setFcm: (fcm) => set({ fcm }),
}));

const firebaseConfig = {
  apiKey: "AIzaSyB-grqVDDlpGnDourK_lC5PgZq4bdh6JSg",
  authDomain: "gass-24518.firebaseapp.com",
  projectId: "gass-24518",
  storageBucket: "gass-24518.appspot.com",
  messagingSenderId: "482574677515",
  appId: "1:482574677515:web:1ad7ff6373a5ec2a09390e",
  measurementId: "G-QC8SPRLNJT",
};
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

let messaging;
try {
  messaging = getMessaging(app);
} catch (error) {
  console.error(error.message);
}

if (messaging) {
  onMessage(messaging, function (payload) {
    const notificationOption = {
      body: payload.notification.body,
      icon: payload.notification.icon,
      data: {
        url: payload.data.url,
      },
    };

    callNotif(payload.notification.title, notificationOption);
  });

  getToken(messaging)
    .then(function (token) {
      useFcmStore.setState({ fcm: token });
    })
    .catch(function (error) {
      console.error(error);
    });
}
