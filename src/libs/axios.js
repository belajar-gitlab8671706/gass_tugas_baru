// import Axios from "axios";
// import { useFcmStore } from "./firebase";

// const axios = Axios.create({
//   baseURL: `${import.meta.env.VITE_API_URL}/api.html`,
//   transformRequest: [
//     (data) => {
//       if (data instanceof FormData) {
//         data.append("token", localStorage.getItem("auth-token"));
//         data.append("fcm", useFcmStore.getState().fcm);
//         return data;
//       } else {
//         return {
//           ...data,
//           token: localStorage.getItem("auth-token"),
//           fcm: useFcmStore.getState().fcm,
//         };
//       }
//     },
//     ...Axios.defaults.transformRequest,
//   ],
// });

// axios.interceptors.request.use((config) => {
//   config.headers["Content-Type"] = "application/x-www-form-urlencoded";
//   if (config.data instanceof FormData) {
//     config.headers["Content-Type"] = "multipart/form-data";
//   }
//   return config;
// });

// axios.interceptors.response.use((response) => {
//   if (typeof response.data == "string" && response.data.includes("error")) {
//     return Promise.reject({
//       error: "Error",
//       message: "Error",
//     });
//   }

//   if (response.data == null) {
//     return Promise.reject({
//       error: "Error",
//       message: "Error",
//     });
//   }

//   if (response.data.code == "0") {
//     return Promise.reject({
//       error: "Error",
//       message: response.data.result?.msg || response.data.msg,
//       data: response.data,
//     });
//   }

//   return response;
// });

// export default axios;

// axios.js
import Axios from "axios";
import { useFcmStore } from "./firebase";

const axios = Axios.create({
  baseURL: "https://ripit.gass.co.id/api/daftar",  // Updated base URL
  transformRequest: [
    (data) => {
      if (data instanceof FormData) {
        data.append("token", localStorage.getItem("auth-token"));
        data.append("fcm", useFcmStore.getState().fcm);
        return data;
      } else {
        return {
          ...data,
          token: localStorage.getItem("auth-token"),
          fcm: useFcmStore.getState().fcm,
        };
      }
    },
    ...Axios.defaults.transformRequest,
  ],
});

axios.interceptors.request.use((config) => {
  config.headers["Content-Type"] = "application/x-www-form-urlencoded";
  if (config.data instanceof FormData) {
    config.headers["Content-Type"] = "multipart/form-data";
  }
  return config;
});

axios.interceptors.response.use((response) => {
  if (typeof response.data == "string" && response.data.includes("error")) {
    return Promise.reject({
      error: "Error",
      message: "Error",
    });
  }

  if (response.data == null) {
    return Promise.reject({
      error: "Error",
      message: "Error",
    });
  }

  if (response.data.code === "0") {
    return Promise.reject({
      error: "Error",
      message: response.data.result?.msg || response.data.msg,
      data: response.data,
    });
  }

  return response;
});

export default axios;
